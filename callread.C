void callread(char* path="psu/7-61/OF*.root",Int_t setn=61,char* Opath=".")
{
  TString str=gSystem->ExpandPathName("$SETFMSENV");
  if(str!="SETFMSENV"){printf("source SetFMSEnv first");exit();};
  gROOT->Reset();
  TString FMSTXT =gSystem->ExpandPathName("${FMSTXT}");
  FMSTXT=FMSTXT+"/";
  TString FMSGAIN =gSystem->ExpandPathName("${FMSGAIN}");
  TString FMSCORR =gSystem->ExpandPathName("${FMSCORR}");
  TString QTMAP = gSystem->ExpandPathName("${QTMAP}");
  TString QTMAP2PP = gSystem->ExpandPathName("${QTMAP2PP}");
  gSystem->Load("${FMSSRC}/Fpdchan.so");
  TString FMSTXT=gSystem->ExpandPathName("${FMSTXT}");
  cout<<"FMSTXT=" <<FMSTXT <<"\n";
  
  FilesSet*  p_files=new FilesSet(  (char*) FMSTXT,
				    "fpdped.txt",
				    (char*) FMSGAIN, 
				    (char*) FMSCORR,
				    "fill.txt",
				    "Fake",
				    "spinpat",
				    "geom.txt",
				    (char*) QTMAP,
				    (char*) QTMAP2PP);

  p_files->Print();

  TString Output_root="Output.root";
  TString TOutputName=gSystem->Getenv("OutputName");
  if(TOutputName!="")
    {Output_root=TOutputName;};
  TString OutRoot=TString(Opath)+"/"+Output_root;
  printf("Output.root=%s\n",OutRoot);
  cout<<"path="<<path<<"\n";
  cout<<"Opath="<<Opath<<"\n";
  TString tmplist=Opath;
  tmplist=tmplist+"/"+Output_root+"_tmplist.txt";
  TString command="rm -f ";
  command=command+tmplist+";ls -1 ";
  command=command+path+">"+tmplist;
  cout<<"command="<<command<<"\n";
  gSystem->Exec(command);

  cout<<"OutRoot="<<OutRoot<<"\n";
  cout<<"tmplist="<<tmplist<<"\n";
  
AnalTools* at=new AnalTools();
// at->IdentifyLED=false; //dont interpret LED identification
 Int_t HiTow=(0x03);
 Int_t JetPatch=((0x07)<<8);
 Int_t Fpd=(0x01)<<14;
 Int_t clbit=((0x03F)<<2);
 at->TrigBitsSelect=HiTow+JetPatch+clbit;
 at->TrigBitsSelect=HiTow;
 at->TrigBitsSelect=JetPatch;
 at->TrigBitsSelect=0;
 at->UseRunDepCor=false;
 at->OpenRunDepMgr("");
 at->RunDepBaseOverride=13095009;
 at->RunDepBaseOverride=13058029;
 at->OutFileName=OutRoot;
 at->Enable_ReRec=false;
 at->Max_Number_Clust_Out=0; // if 0, writes out all cone clusters


 Float_t a1[3]={1.0708,.1677,-.239};
 Float_t b1[3]={.536,.850,2.383};
 // float a1[3]={.8,.3,-.1};
 //float b1[3]={.8,.2,7.6};

 FitTower::Setwe_ab(a1[0],a1[1],a1[2],b1[0],b1[1],b1[2]);
 FitTower::Setwe_ErrFactors(.01,.03,1.,1.,at->Energy_study);
 FitTower::SetBlockFit(false);
 FitTower::Setwe_EDepCor(false);
 FitTower::SetSubclu2(false);
 FitTower::SetDoGlobal(false);
//at->OutputToSingle=true;
 at->NumberEventsToProcess=100000;
 at->NumberEventsToProcess=0;
 Float_t EphotMin=3.;
 TString TEphotMin=gSystem->Getenv("EphotMin");
 if(TEphotMin!="")
   {
     sscanf(TEphotMin.Data(),"%f",&EphotMin);
   };
 for(int j=0;j<4;j++)at->Eminpout[j]=EphotMin;
 at->UseCorrectMass=true; // mass opening angle correction (FpdRoot/CorrectMass.cxx)

 TString useCorrectMass=gSystem->Getenv("UseCorrectMass");
 printf("useCorrectMass env=%s\n",useCorrectMass.Data());
 if(useCorrectMass=="false")at->UseCorrectMass=false;
 at->UseCorrectMassDefaults=true;
 if(at->UseCorrectMass)printf("UseCorrectMass=true\n");
 at->readq92((const char*) tmplist,p_files);
 // at->readq1((const char*) tmplist,setn,p_files);
//at->readqM((const char*) tmplist,setn,p_files);

}
