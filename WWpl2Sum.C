void WWpl2Sum( char*  picks)
{
  gStyle->SetOptFit();
  char* CnPhiBins="4";

  int nPhiBins=4;
  TGraphErrors* valperphi[3][4];

  TString TrName[3]={"phi_vs_pt11000LgW","phi_vs_pt12000LgW","phi_vs_pt14000LgW"};
  sscanf(CnPhiBins,"%d",&nPhiBins);
  TString bins[4]={"1","2","3","4"};

  TCanvas* c1=new TCanvas("c1","c1");

  c1->Print("c1Sum.ps(");
  for(int nn=0;nn<3;nn++)
    {
      for(int jph=0;jph<nPhiBins;jph++)
	{
	  valperphi[nn][jph]=new TGraphErrors();
	  TString grname=TrName[nn]+"_"+bins[jph];
	  cout<<grname<<"\n";
	  valperphi[nn][jph]->SetTitle(grname);

	}
      
      TString Picks=picks;
      TObjArray* Ind=Picks.Tokenize(" ");
      int NInd=Ind->GetEntries();
      
      cout<<"Picks are: "<<picks<<"\n";
      TString runlist =gSystem->Getenv("Wruns");
      TObjArray* names=runlist.Tokenize(" ");
      TIter next(names);
      TString Name;
      TObjString* NM;
      int count=0;
      int passcnt=0;
      while(NM= (TObjString*) next())
	{
	  
	  bool pass=false;
	  count++;
	  TIter nxt(Ind);
	  TObjString* scnt;
	  while(scnt=(TObjString*) nxt())
	    {
	      TString sdat=scnt->GetString();
	      int newind=-1;
	      sscanf(sdat.Data(),"%d",&newind);
	      if(newind==count)pass=true;
	      
	    };
	  if(pass)
	    {
	      passcnt++;
	      c1->Clear();
	      c1->Divide(4,1);
	      Name=NM->GetString();
	      TString Fname="";
	      cout<<"Names are: "<<Name<<"\n";
	      Fname=TString("W")+Name+"/otr.root";
	      cout<<Name<<" "<< Fname <<"\n";
	      TFile* f=new TFile(Fname,"update");
	      //	  f->ls();
	      TString dname=TrName[nn]+Name+"_phi"+CnPhiBins;
	      
	      cout<<dname<<"\n";
	      TDirectory* dd=(TDirectory*) (f->Get(dname));
	      dd->ls();
	      TH1D* h[16];
	      
	      Color_t colors[16]={ kOrange+10,kOrange+1,kOrange-4,kOrange-9,kYellow+1,kSpring+9,kSpring+3,kGreen+1,kGreen+3,kTeal+8,kCyan,kCyan+3,kAzure,kBlue-10,kBlue,kViolet};
	      for(int j=0;j<nPhiBins;j++)
		{
		  char chnum[100];
		  sprintf(chnum,"_%d_",j);
		  cout<<dname+chnum<<"\n";
		  h[j]=(TH1D*) dd->Get(dname+chnum);
		  if(h[j])
		    {
		      h[j]->SetLineColor(colors[j]);
		      cout<<h[j]->GetName()<<"\n";
		    };
		  
		};
	      TGraphErrors* gr0=(TGraphErrors*) dd->Get("gr0");
	      TGraphErrors* gr1=(TGraphErrors*) dd->Get("gr1");
	      c1->cd(1);
	      if(gr0)
		{
		  gr0->Fit("pol0");
		  gr0->SetTitle(dname+"gr0");
		  float ave=gr0->GetFunction("pol0")->GetParameter(0);
		  gr0->GetYaxis()->SetRangeUser(0,2*ave);
		  gr0->Draw("A*");
		  for(int jph=0;jph<nPhiBins;jph++)
		    {
		      if(jph<gr0->GetN())
			{
			  int ntmp=valperphi[nn][jph]->GetN();
			  valperphi[nn][jph]->SetPoint(ntmp,passcnt,gr0->GetY()[jph]);
			  valperphi[nn][jph]->SetPointError(ntmp,0 ,gr0->GetErrorY(jph));
			}
		    };
		};
	      c1->cd(2);
	      if(gr1)
		{
		  gr1->Fit("pol0");
		  gr1->SetTitle(dname+"gr1");
		  gr1->GetYaxis()->SetRangeUser(-4.,0.);
		  gr1->Draw("A*");
		};
	      c1->cd(3);
	      if(h[0])h[0]->Draw();
	      c1->GetPad(3)->SetLogy();
	      
	      for(int kk=1;kk<nPhiBins;kk++)if(h[kk])h[kk]->Draw("same");
	      c1->Update();
	      
	      c1->Print("c1Sum.ps");
	      if(f)delete f;
	    }
	  
	};
    };
  cout<<"now draw summaries\n";
  TF1 *low;
  TF1 *hi;
  for(int nn =0;nn<3;nn++)
    {
      c1->Clear();
      for(int jph=0;jph<4;jph++)
	{
	  if(jph<gr0->GetN())
	    {
	      printf("nn=%d jph=%d \n",nn,jph);
	      valperphi[nn][jph]->Fit("pol0","","",0,4.5);
	      low=new TF1(*(valperphi[nn][jph]->GetFunction("pol0")));
	      valperphi[nn][jph]->GetXaxis()->SetTitle("Run Index (1-4 Day 82) (5-8)Day 142");

	      float flow=low->GetParameter(0);
	      float ratio=0;
	      valperphi[nn][jph]->Fit("pol0","","",4.5,9);
	      hi=new TF1(*(valperphi[nn][jph]->GetFunction("pol0")));
	      if(flow>0)ratio=hi->GetParameter(0)/flow;
	      printf("ratio=%f\n",ratio);
	      TString titl=valperphi[nn][jph]->GetTitle();
	      cout<<titl<<"\n";
	      char ttl[1000];
	      sprintf(ttl,"%s_ratio=%f4.2",titl.Data(),ratio);
	      valperphi[nn][jph]->SetTitle(ttl);
	      valperphi[nn][jph]->Draw("A*");
	      if(low) low->Draw("same");
	      if(hi)hi->Draw("same");
	    }
	  c1->Print("c1Sum.ps");
	}
    }
  c1->Clear();
  c1->Print("c1Sum.ps)");

  gSystem->Exec("ps2pdf c1Sum.ps ; rm c1Sum.ps");
}
