#ifndef SETFPD_
#define SETFPD_
#include "CalibStr.h"

 class SETFPD 
 {
 public:
   SETFPD(CalibStr*);
   CalibStr* fpdcorr;
   CalibStr* maxadc;
   void SetRange(int Iew,int nstb,int row0,int col0,float low, float high);
   void SetValue(int Iew,int nstb,int row0,int col0,float val);
   void PreSet();
   void checkCases();
   void CheckAll();
   bool sset[4][34][34];
   void aGTb(int Iewa,int nstba,int row0a,int col0a,int Iewb,int nstbb,int row0b,int col0b);// set a to  value above b
   void aLTb(int Iewa,int nstba,int row0a,int col0a,int Iewb,int nstbb,int row0b,int col0b);// set a to  value below b
 };


#endif
