#include "CorrectMass.h"
using namespace std;
ClassImp(CorrectMass)

CorrectMass::CorrectMass(float oldmass, float e, float z, bool defaults,float hm0,float hm1,float hm2)
{
  if(defaults)
    {
      Cinit(oldmass,e,z);
    }
  else {Cinit(oldmass,e,z,hm0,hm1,hm2);};
}
CorrectMass::CorrectMass(float oldmass, float e, float z,float hm0,float hm1,float hm2)
{
  Cinit(oldmass, e, z,hm0,hm1,hm2);
};
void CorrectMass::Cinit(float oldmass, float e, float z,float hm0,float hm1,float hm2)
{
  hiEMass[0]=hm0;
  hiEMass[1]=hm1;
  hiEMass[2]=hm2;
  pairEta=100.;
  pairPhi=0.;
  OldMass=oldmass;
  e1=(1+z)/2*e;
  e2=(1-z)/2*e;

  ang=1;
  angfactor=1.;
  largeCell=true;
  if(e1*e1>0)ang=OldMass/sqrt(e1*e2); 
};
float CorrectMass::FixedforM(float m)
{
  float mtmp=m-(m-.135) *exp(-(m-.2)/.1);
  if(m<.2){m=.135;}
    else
      m=mtmp;
  float normf=1.5;
  if(!largeCell)normf=.7;

  if(e1*e2>0)ang=m/sqrt(e1*e2);
  gcorang=1+normf*exp(hiEMass[0]+hiEMass[1]*ang*angfactor+hiEMass[2]*ang*ang*angfactor*angfactor);
  //  printf("e1=%f e2=%f ang=%f angfactor=%f gcorang=%f\n",e1,e2,ang,angfactor,gcorang);
  if(gcorang<=0)return 0;


  return OldMass/gcorang;
}
float CorrectMass::FixSimple()
{
  float normf=1.9;
  if(!largeCell)//small cells
    {
      angfactor=5.8/3.81*1.3;//small cells
      normf=.5;
    }
  else  //large cells
    {
      normf=3.5;
      angfactor=1.3;
    }
  gcorang=1+normf*exp(hiEMass[0]+hiEMass[1]*ang*angfactor+hiEMass[2]*ang*ang*angfactor*angfactor);
  if(gcorang<=0)return 0;

  return OldMass/gcorang;
}



void CorrectMass::UpdateEtaPhi(float eta, float phi)
{
  pairEta=eta;
  pairPhi=phi;
  largeCell=false;
  angfactor=5.8/3.81*1.3;//small cells
  
  if(fabs(2*exp(-eta)*cos(phi))>12*3.81/730.){largeCell=true;angfactor=1.3;}//large cells
  if(fabs(2*exp(-eta)*sin(phi))>12*3.81/730.){largeCell=true;angfactor=1.3;}//large cells

}
