#ifndef GCFMS_
#define GCFMS_
#include <TFile.h>
#include <TString.h>
#include <TGraph.h>
#include "Cell.h"
#include "CalibStr.h"
#include "SETFPD.h"


class GCFMS : public TObject
{
public:
  GCFMS(Cell* c=0);
  ~GCFMS();
  void GraphVersions(int nstblo,int nstbhi);
  void AppendChange(int EW, int nstb, int row0, int col0);
  Float_t Cor;
  int ver;
  Int_t NROWS[4];
  TFile* Fpd;
  CalibStr* FpdCorr;
  CalibStr* FpdGain;
  TString GainSource;
  TString CorrSource;
  TGraph* fpgr[34][17][4];
  TGraph* cvar[34][17][4];
  private:
  ClassDef(GCFMS,1);
};

#endif
