#ifndef PostYiqun_
#define PostYiqun_
#include "Yiqun.h"
#include "TSystem.h"
#include "TMath.h"
#include "TString.h"
class PostYiqun : public TObject 
{
 public:
  PostYiqun(TString formname,Yiqun* y);
  PostYiqun(Yiqun* y,bool islarge=true,TString ReCorrect="",float g0=0,float eps=0,float eps2=0,float e0=12,float e00=25);
  void PostYiqunL(Yiqun* y=0,float g0=1.,float eps=0.025,float eps2=0.001,float e0=12.,float e00=25.);
  void PostYiqunS(Yiqun* y=0,float g0=1.,float eps=0.025,float eps2=0.001,float e0=12.,float e00=25.);
  bool AllowReCorrect;
  Yiqun* Y;
  //non-linear photon correction 3 lines slopes=eps,0,eps2
  //between corner energies e0 and e00
  float E0L,E00L;
  float E0S,E00S;
  float EpsL,Eps2L;
  float EpsS,Eps2S;
  float G0L;//overall gain factor
  float G0S;//overall gain factor
  //
  float correct(float E1);
  bool ReadEnvironment;
  float* SummaPar;
  float summaparL[6];
  float summaparS[6];
  float lastscaleL;
  float lastscaleS;
  float lastscale;
  float g;
 private:
  ClassDef(PostYiqun,2);
};
#endif

