#ifndef ProjectGCor_
#include "RunDepMgr.h"

class ProjectGCor : public TObject
{
 public:
  ProjectGCor(TString gcorpath1,TString RunDepPath,
	      TString gcorpath2,Int_t PathBase1,Int_t PathBase2);
  ~ProjectGCor();
  TString RundepPath;
  RunDepMgr* Mgr;
  RunDepMgr* Mgr1;
  RunDepMgr* Mgr2;
  TString CalibPath1;
  TString CalibPath2;
  CalibStr* Cal1;
  CalibStr* Cal2;
  Int_t PAthBase1;
  Int_t PAthBase2;
  void CleanCalib(CalibStr* ccl);
  void SetupMgr (int one_two, CalibStr* cal,Int_t runNumber, Int_t baseNumber);
  void PrintMgr (RunDepMgr* mgr,int printopt=0,int set12=0);
  
  CalibStr* ProjectTo(Int_t RunNumber,Int_t OldRunNumber);
  void SetPrint(bool tf=true){kPrint=tf;};
  bool checkMgr(int one_two);
  void HistCompare12();
  CalibStr* ProjectMgr1();
  void ScaleCalibStr(CalibStr* cal,int Nstb,float scalefactor);

  TH2F* ratio1[4];
  TH2F* ratio2[4];
  TH2F* ratio3[4];
  TH1F* rat1[4];
  TH1F* rat2[4];
  TH1F* rat3[4];

  TGraphErrors* rat1vsEta[4];
  TGraphErrors* rat3vsEta[4];
  void  SetLedCorrection(float lcf){LedCorrectionFraction=lcf;};
 private:
  CalibStr* Cal1Out;
  bool kPrint;
  Float_t LedCorrectionFraction; //this is the fraction of led change to be aplied to new corrections
  ClassDef(ProjectGCor,1);
};
#endif

