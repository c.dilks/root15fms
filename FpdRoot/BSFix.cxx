#include "BSFix.h"
#include "TMath.h"
ClassImp(BSFix)
BSFix::BSFix(Yiqun* y,Double_t dayNum_/*=60*/, Double_t fudgeFactor_/*=1*/,Geom* pgeom)
{
  /*
    if pointer to Geom is non-zero, then this will now also 
    fill the clu2 clusters user in calibration. 
   */
  p_geom=pgeom;
  pTimeEtaFuncL=0;
  pTimeEtaFuncS=0;
  pSlopeFuncL=0;
  pSlopeFuncS=0;
  if(y==0)return;
  Y=y;

  if( CreateBSFuncs() )
  {
    std::cerr<<"ERROR in BSFix constructor when creating functions"<<std::endl;
    return;
  }

  if(Y->NSTB>0 && Y->NSTB<5)
  {
    for( int n=0;n<Y->NPh;n++)
    {
      if(!Y->photons[n].corrected)
      {
	Double_t energy=(Double_t) Y->photons[n].energy;
	TVector3 labCoor = Y->ph_coord_lab(n);
	Double_t eta= labCoor.Eta();
//	Bool_t isLargeCell = (Y->NSTB<2) ? true : false;
	Bool_t isLargeCell = (Y->NSTB<=2) ? true : false;
	energy=GetETrue(isLargeCell, eta, dayNum_, energy,fudgeFactor_);
	Y->photons[n].energy=(float) energy;
	Y->photons[n].corrected=true;
      }
    }
    // added by SH Mar 7 2018 to modify alternative clusters
    TObjArray* clu2=Y->Clu2; 
    TIter nex0(clu2);
    HitCluster* hcl=0;
    while(hcl=(HitCluster*) nex0())
      {
	if(!(hcl->IsEDepUpdated && p_geom))
	  {
	    Double_t energy=(Double_t) hcl->energy;
	    TVector3 locCoor(hcl->x0,hcl->y0,0);
	    TVector3 labCoor =p_geom->GlobalXYZ(2,Y->NSTB,locCoor);
	    Double_t eta= labCoor.Eta();
	    //	Bool_t isLargeCell = (Y->NSTB<2) ? true : false;
	    Bool_t isLargeCell = (Y->NSTB<=2) ? true : false;
	    energy=GetETrue(isLargeCell, eta, dayNum_, energy,fudgeFactor_);	
	    hcl->energy=energy;
	    hcl->IsEDepUpdated=true;
	  };
      };
    //end Mar 7 2018 change
  }
}

BSFix::~BSFix()
{
  if(pTimeEtaFuncL) delete pTimeEtaFuncL;
  if(pTimeEtaFuncS) delete pTimeEtaFuncS;
  if(pSlopeFuncL) delete pSlopeFuncL;
  if(pSlopeFuncS) delete pSlopeFuncS;
}

Int_t BSFix::CreateBSFuncs()
{
  pTimeEtaFuncL=new TF1("pTimeEtaFuncL","expo"); //Adjust time for relative radiation at different etas
  pTimeEtaFuncS=new TF1("pTimeEtaFuncS","expo");
  pSlopeFuncL=new TF1("pSlopeFuncL","[0]-expo(1)"); //Calculate factor to multiply day 0 slopes by 
  pSlopeFuncS=new TF1("pSlopeFuncS","[0]-expo(1)"); //Calculate factor to multiply day 0 slopes by 
  
  //BS NOTE: add in slope func for small cells here, and fix values

  Double_t parTimeEtaL[2]={-7.366003, 2.37613};
  Double_t parTimeEtaS[2]={-7.43847 , 1.90730};

  Double_t parSlopeL[3]={4.45576,1.24338,-2.77894E-3};
  
  Double_t parSlopeErrL[3]={0.57074,0.015987,6.77183E-4};


  Double_t parSlopeS[3]={3.44334,0.896651,-2.17923E-3};
  Double_t parSlopeErrS[3]={0.203544,7.55224E-2,3.97603E-4};

  pTimeEtaFuncL->SetParameters(parTimeEtaL); //Origionally wanted fix parameters but would have to do each parameter in a loop. Functionaly same since we never touch the functions
  pTimeEtaFuncS->SetParameters(parTimeEtaS);


  pSlopeFuncL->SetParameters(parSlopeL);
  pSlopeFuncL->SetParErrors(parSlopeErrL);


  pSlopeFuncS->SetParameters(parSlopeS);
  pSlopeFuncS->SetParErrors(parSlopeErrS);

  return 0;
}

Double_t BSFix::GetETrue(Bool_t isLarge_/*=true*/, Float_t eta_/*=3.1*/ , Float_t days_/*=52*/, Float_t energy_/*=10.0*/, Float_t fudgeFactor_/*=1.*/ )
{

  //Starting values for low, mid, high slope
  Double_t startSlopeL[3]={0.0146288, 0.00822771, 0.002269};
  Double_t startSlopeS[3]={0.0197536, 0.0048173, 0.0019957};
  
  Double_t eTrue, crossLowMid, crossMidHigh, normPoint, timeEtaFactor, slopeFactor, *currentSlopes=0;
  TF1* whichFuncEta=0, *whichFuncSlope=0;

  if(isLarge_) //Large cell
  {
    if( pTimeEtaFuncL==0 || pSlopeFuncL==0)
    {
      std::cerr<<"ERROR: In BSFix::GetBSDivisor. Funciton Returned Null pointer. Returning origional energy"<<std::endl;
      return energy_;
    }

    whichFuncEta   = pTimeEtaFuncL;
    whichFuncSlope = pSlopeFuncL;
    currentSlopes  = startSlopeL;
    crossLowMid  = 8.60402; //Low-Mid Crossover Point
    crossMidHigh = 20.0762; //Mid-High Crossover Point
    normPoint=12.5; //Nomralization point
  }//end isLarge_ = true loop
  else //isLarge_ = false, Small cells
  {
    if( pTimeEtaFuncS==0 || pSlopeFuncS==0)
    {
      std::cerr<<"ERROR: In BSFix::GetETrue. Funciton Returned Null pointer. Returning origional energy"<<std::endl;
      return energy_;
    }

    whichFuncEta   = pTimeEtaFuncS;
    whichFuncSlope = pSlopeFuncS;
    currentSlopes  = startSlopeS;
    crossLowMid  = 10.1867; //Low-Mid Crossover Point //BS NOTE. THESE POINTS ARE MUCH MORE VOLITILE TO THE CHANGEING GEOMETRY/FITS
    crossMidHigh = 34.1256; //Mid-High Crossover Point //BS NOTE. THESE POINTS ARE MUCH MORE VOLITILE TO THE CHANGEING GEOMETRY/FITS
    normPoint=20; //Nomralization point

  }//end isLarge_ = false loop

  timeEtaFactor = whichFuncEta->Eval(eta_); //Relative factor for different etas
  timeEtaFactor *= fudgeFactor_; //Additional fudge factor to account for my def of day being off
  slopeFactor = whichFuncSlope->Eval(days_ * timeEtaFactor);//What to multiply all slopes by to get current daily values



  //ADJUSTING SLOPES
  for(Int_t iSlope=0; iSlope<3; iSlope++) currentSlopes[iSlope]*=slopeFactor;

  //BS CHANGE OCT 20. Correcting error in crossover points due to true energy vs observed enrgy
  Double_t actualCrossLowMid= crossLowMid*(1.-(normPoint-crossLowMid)*currentSlopes[1]);
  Double_t actualCrossMidHigh= crossMidHigh*(1.-(normPoint-crossMidHigh)*currentSlopes[1]);
  //Adjusting changeover points in if energy loop

  //BS NOTE. Currently assumes that the normalization point is between the two crossover points. Think on generalizations
  Double_t dummy; //Exists just to simplified code
  if( energy_ < actualCrossLowMid) //Low Energy Domain
  {
    dummy=normPoint*currentSlopes[1]-1.+ crossLowMid*(currentSlopes[0]-currentSlopes[1]); 
    eTrue=(dummy+TMath::Sqrt(TMath::Power(dummy,2) + 4 * energy_*currentSlopes[0]))/(2.*currentSlopes[0]);
  }
  else if ( energy_ >= actualCrossLowMid && energy_ <= actualCrossMidHigh) //Middle energy
  {
    dummy=normPoint*currentSlopes[1]-1.;
    eTrue=(dummy+TMath::Sqrt(TMath::Power(dummy,2) + 4 * energy_*currentSlopes[1]))/(2.*currentSlopes[1]);
  }
  else //High energy regime
  {
    dummy=normPoint*currentSlopes[1]-1.+ crossMidHigh*(currentSlopes[2]-currentSlopes[1]); 
    eTrue=(dummy+TMath::Sqrt(TMath::Power(dummy,2) + 4 * energy_*currentSlopes[2]))/(2.*currentSlopes[2]);
  }

  if(eTrue<=0)
  {
    std::cerr<<"ERROR: In BSFix::GetETrue. eTrue ="<<eTrue<<" is <= 0. Setting eTrue=eInitial"<<std::endl;
    eTrue=energy_;
  }
  return eTrue;
}
