#ifndef CorrectMass_
#define CorrectMass_
#include "TObject.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>

class CorrectMass : public TObject
{
public:
  CorrectMass(float oldmass, float e, float z, bool defaults,float hm0=.277,float hm1=-148.,float hm2=-5079.);
  CorrectMass(float oldmass, float e,float z,float hm0=.277,float hm1=-148.,float hm2=-5079.);
  void Cinit(float oldmass, float e,float z,float hm0=.277,float hm1=-148.,float hm2=-5079.);
  float hiEMass[3];
  float pairEta;
  float pairPhi;
  float FixedforM(float m=0.135);
  float FixSimple();
  float OldMass;
  float e1;
  float e2;
  float ang;
  float angfactor; //angfactor nominally set to 5.8/3.81 in small cells indicating effect of smaller cells.
  float gcorang;
  void UpdateEtaPhi(float eta,float phi);
  bool largeCell;
 private:
  ClassDef(CorrectMass,2);
};
#endif
