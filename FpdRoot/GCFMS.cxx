#include "GCFMS.h"
using namespace std;
ClassImp(GCFMS)

GCFMS::GCFMS(Cell* c)
{ 

  // ***********Fpd
  FpdGain=0;
  FpdCorr=0;
  NROWS[0]=NROWS[1]=34;NROWS[2]=NROWS[3]=24;
  Fpd=new TFile("${FMSROOT}/CodeSmall/FpdFile.root","Read");
  FpdGain=(CalibStr*) Fpd->Get("FpdGain");
  GainSource="FpdFile";
  if(FpdGain==0)
    {
      cout<<"FpdGain not found in file\n";
      if(c)
	{
	  FpdGain=c->fpdgain;
	  GainSource=c->GetName();
	};
      if(FpdGain==0)
	{
	  FpdGain=new CalibStr(90100100,"${FMSTXT}/${FMSGAIN}");
	  GainSource="FMSTXT/FMSGAIN";
	};
    }
  Cor=1;
  ver=1;
  char strver[100];
  sprintf(strver,"%d",ver);
  CorrSource="FpdFile";
  while(CalibStr* FpdCnew=(CalibStr*) Fpd->Get(TString("FpdCorrV")+strver))
    {		
      FpdCorr=FpdCnew;
      FpdCorr->filename="FpdCorr.txt";
      FpdCorr->OutName="FpdCorr.txt";
      ver++;
      sprintf(strver,"%d",ver);
    };  
  Fpd->Close();
  TString ChangeFpd =gSystem->ExpandPathName("${FMSROOT}/CodeSmall/ChangeFpd.txt");
  cout<<"look for "<<ChangeFpd<<" \n";
  FILE* fp=fopen((const char*) ChangeFpd,"r");
  char line[200];
  if(fp)
    {
      cout<<"Reading "<<ChangeFpd<<"\n";
      while(!feof(fp))
	{
	  if(fgets(line,100,fp))
	    {
	      Int_t ew,nst,ro,co;
	      Float_t val;
	      sscanf(line,"SetValue(%d,%d,%d,%d,%f)",&ew,&nst,&ro,&co,&val);
	      FpdCorr->SetValue(ew,nst,ro,co,val);
	      };
	};
      CorrSource="FpdFile+ChangFpd";
      fclose(fp);
    }
  else
    {
      cout<<"ChangeFpd.txt not found\n";
    };
    CorrSource="FpdFile+ChangFpd+SetFPD";
    SETFPD setfpd(FpdCorr);
}
GCFMS::~GCFMS()
{
  if(Fpd)delete Fpd;
}
void GCFMS::GraphVersions(int nstblo,int nstbhi)
{
  if(Fpd){Fpd->Close();delete Fpd;};
  Fpd=new TFile("${FMSROOT}/CodeSmall/FpdFile.root","Read");
  CalibStr* FpdCorr0=0;
  CalibStr* FpdCnew=0;
  int vers=1;  
  char strver[100];
  for( int ns=nstblo-1;ns<nstbhi;ns++)
    {
      for(int ro=0;ro<NROWS[ns];ro++)
	{
	  for(int co=0;co<NROWS[ns]/2;co++)
	    {
	      fpgr[ro][co][ns]=0;
	      cvar[ro][co][ns]=0;
	    }
	};
    };
  sprintf(strver,"%d",vers);
  while(FpdCnew=(CalibStr*) Fpd->Get(TString("FpdCorrV")+strver))
    {		
      printf("inter \n");
      FpdCorr0=FpdCnew;
      FpdCorr0->filename="FpdCorr.txt";
      FpdCorr0->OutName="FpdCorr.txt";
      for( int ns=nstblo-1;ns<nstbhi;ns++)
	{
	  for(int ro=0;ro<NROWS[ns];ro++)
	    {
	      for(int co=0;co<NROWS[ns]/2;co++)
		{
		  if(fpgr[ro][co][ns]==0)fpgr[ro][co][ns]=new TGraph();
		  int npt=fpgr[ro][co][ns]->GetN();
		  fpgr[ro][co][ns]->SetPoint(npt,vers,FpdCorr0->GetValue(2,ns+1,ro,co));
		  
		  char str[100];
		  sprintf(str,"row_%d_col%d_d%d",ro,co,ns);
		  fpgr[ro][co][ns]->SetTitle(str);
		};
	    };
	};
      vers++;
      
      sprintf(strver,"%d",vers);
    };
  

}
void GCFMS::AppendChange(int EW, int nstb, int row0, int col0)
{
  char setvalchar[100];
  float val=FpdCorr->GetValue(2,nstb,row0,col0);
  sprintf(setvalchar,"SetValue(2,%d,%d,%d,%f)\n",nstb,row0,col0,val);
  TString ChangeFpd =gSystem->ExpandPathName("${FMSROOT}/CodeSmall/ChangeFpd.txt");
  FILE* fp=fopen((const char*) ChangeFpd,"a");
  if(fp)
    {
      fputs(setvalchar,fp);
      fclose(fp);
      printf("wrote: %s \n",setvalchar);
    }
  else printf("Error opening ChangeFpd.txt\n");
};
