#include "PostYiqun.h"
ClassImp(PostYiqun)

void PostYiqun::PostYiqunL(Yiqun* y,float g0,float eps,float eps2,float e0,float e00)
{
  if(y==0)return;
  Y=y;
  if(g0<0)ReadEnvironment=true;
  G0L=fabs(g0);
  EpsL=eps;
  Eps2L=eps2;
  E0L=e0;
  E00L=e00;
  
if(Y->NSTB>0 && Y->NSTB<3)
    {
        for( int n=0;n<Y->NPh;n++)
	{
	  if((!Y->photons[n].corrected) || AllowReCorrect )
	    {
	      float energy=Y->photons[n].energy;
	      energy=correct(energy);
	      Y->photons[n].energy=energy;
	      Y->photons[n].corrected=true;
	    }
	}
      TObjArray* clu2=Y->Clu2; 
      TIter nex0(clu2);
      HitCluster* hcl=0;
      while(hcl=(HitCluster*) nex0())
	{
	  if(!(hcl->IsEDepUpdated) || AllowReCorrect )
	    {
	      float energy=hcl->energy;
	      energy=correct(energy);
	      hcl->energy=energy;
	      hcl->IsEDepUpdated=true;
	    };
	};
    };
};

void PostYiqun::PostYiqunS(Yiqun* y,float g0,float eps,float eps2,float e0,float e00)
{
  if(y==0)return;
  Y=y;
  if(g0<0)ReadEnvironment=true;
  G0S=fabs(g0);
  EpsS=eps;
  Eps2S=eps2;
  E0S=e0;
  E00S=e00;
  
  if(Y->NSTB>2 && Y->NSTB<5)
    {
      for( int n=0;n<Y->NPh;n++)
	{
	  if(!Y->photons[n].corrected || AllowReCorrect )
	    {
	      float energy=Y->photons[n].energy;
	      energy=correct(energy);
	      Y->photons[n].energy=energy;
	      Y->photons[n].corrected=true;
	    }
	}
      TObjArray* clu2=Y->Clu2; 
      TIter nex0(clu2);
      HitCluster* hcl=0;
      while(hcl=(HitCluster*) nex0())
	{
	  if(!(hcl->IsEDepUpdated) || AllowReCorrect )
	    {
	      float energy=hcl->energy;
	      energy=correct(energy);
	      hcl->energy=energy;
	      hcl->IsEDepUpdated=true;
	    };
	};
      
    };
};

PostYiqun::PostYiqun(TString formname,Yiqun* y)
{
  /*
    for formname = "summaparL" or "summaparS"  (other strings do not correct)
                This is a brute force early version of Brandons correction
   */
  AllowReCorrect=false;
  ReadEnvironment=false;
  SummaPar=0;
  //Brandons day 1
  float spL[6]={0.847242,0.0146288,0.00822771,0.002269,7.79738,18.587};
  float spS[6]={0.810838,0.015133,0.00409459,0.00185059,13,35.778};
  lastscaleL=1.;
  lastscaleS=1.;
  lastscale=lastscaleS;
  if(y->NSTB<3)lastscale=lastscaleL;
  for(int j=0;j<6;j++)
    {
      summaparL[j]=spL[j];
      summaparS[j]=spS[j];
    };
  if(formname=="summaparL")
    {
      SummaPar=summaparL;
    }
  if(formname=="summaparS")
    {
      SummaPar=summaparS;
    }
  //return if neither summaparS or summaparL
  if(SummaPar==0)return;

  Y=y;
  float energy;
  for( int n=0;n<Y->NPh;n++)
    {
      if(!Y->photons[n].corrected || AllowReCorrect )
	{
	  energy=Y->photons[n].energy;
	  energy=correct(energy);
	  Y->photons[n].energy=energy;
	  Y->photons[n].corrected=true;
	}
    }

  TObjArray* clu2=Y->Clu2; 
  TIter nex0(clu2);
  HitCluster* hcl=0;
  while(hcl=(HitCluster*) nex0())
    {
      if(!(hcl->IsEDepUpdated) || AllowReCorrect )
	{
	  energy=hcl->energy;
	  energy=correct(energy);
	  hcl->energy=energy;
	  hcl->IsEDepUpdated=true;
	};
    };
}

float PostYiqun::correct(float E1)
{
  g=1.;
  
  if(SummaPar)
    {
      float gacum[3]={0,0,0};
      gacum[0]=SummaPar[0]+SummaPar[1]*(TMath::Min(SummaPar[4],E1));
      if(E1>SummaPar[4])gacum[1]=SummaPar[2]*(TMath::Min(SummaPar[5],E1)-SummaPar[4]);
      if(E1>SummaPar[5])gacum[2]=SummaPar[3]*(E1-SummaPar[5]);
      float gg=gacum[0]+gacum[1]+gacum[2];
      if(gg<.2)gg=.2;
      //      printf("E1=%f gg=%f -> E1/gg=%f\n",E1,gg,E1/gg);
      return E1/gg/lastscale;
    };
  if(ReadEnvironment)
    {
      TString EdepSlopes=  gSystem->Getenv("EdepSlopes");
      if(EdepSlopes != "")
	{
	  sscanf(EdepSlopes.Data(),"%f %f %f %f %f %f ",&G0L,&EpsL,&Eps2L,&G0S,&EpsS,&Eps2S);
	}
    };
  if(Y->NSTB<3)
    {
      if(E1<E0L)
	{
	  g=(1+(E1-E0L)*EpsL);
	};
      if(E1>E00L) 
	{
	  g=(1+(E1-E00L)*Eps2L);
	}
      
      g=g*G0L;
      if(g<.3)g=.3;
      return E1/g/lastscale;
    }
  else
    {
      if(E1<E0S)
	{
	  g=(1+(E1-E0S)*EpsS);
	};
      if(E1>E00S) 
	{
	  g=(1+(E1-E00S)*Eps2S);
	}
      
      g=g*G0S;
      if(g<.3)g=.3;

      return E1/g/lastscale;
    };
}
PostYiqun::PostYiqun(Yiqun* y,bool islarge,TString ReCorrect,float g0,float eps,float eps2,float e0,float e00)
{
  /*
    operates on large (islarge==true) or small cell energies.

    This is the (perhaps with recorrecting) a 3 line correction between <e0, e0-e00,>e00
    with slopes eps, 0 , eps2

    if g0==0 use default parameters 
    otherwise use parameters indicated
    do not read environment values for parameters
    
   */
  if(ReCorrect=="ReCorrect")AllowReCorrect=true;
  ReadEnvironment=false;
  lastscaleL=1;
  lastscaleS=1;
  lastscale=1;
  SummaPar=0;
  if(islarge)
    {
      if(g0==0){
	PostYiqunL(y);
      }
      else {
	PostYiqunL(y,g0,eps,eps2,e0,e00);
      }

    }
  else
    {
      if(g0==0){
	PostYiqunS(y);
      }
      else {
	PostYiqunS(y,g0,eps,eps2,e0,e00);
      }
    };
};


