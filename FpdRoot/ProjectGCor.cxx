#include "ProjectGCor.h"
ClassImp(ProjectGCor)
ProjectGCor:: ProjectGCor(TString gcorpath1,TString RunDepPath,
			  TString gcorpath2,Int_t PathBase1, Int_t PathBase2)
{ 
  
  RundepPath=RunDepPath;
  CalibPath1=gcorpath1;
  CalibPath2=gcorpath2;
  kPrint=false;
  Mgr1=0;
  Mgr2=0;
  SetLedCorrection(1.);
  char* path=(char*) RunDepPath.Data();
  Cal1=new CalibStr(12000000,gcorpath1);
  printf("After Cal1 .......................\n");
  Cal1Out=new CalibStr(12000000,gcorpath1);
  printf("After Cal1Out .......................\n");
  printf("name=%s\n",gcorpath2.Data());
  Cal2=new CalibStr(12000000,gcorpath2);
  printf("After Cal2 .......................\n");
  PAthBase1=PathBase1;
  PAthBase2=PathBase2;
  for(int j=0;j<4;j++)
    {
      ratio1[j]=0;
      ratio2[j]=0;
      ratio3[j]=0;
      rat1[j]=0;
      rat2[j]=0;
      rat3[j]=0;
      rat1vsEta[j]=0;
      rat3vsEta[j]=0;
    };
  Int_t nrows[4]={34,34,24,24};
  Int_t ncols[4]={17,17,12,12};
  for(int j=0;j<4;j++)
    {
      printf("j=%d\n",j);
      char titl[100];
      sprintf(titl,"ratio12_%d",j);
      rat1vsEta[j]=new TGraphErrors();
      rat1vsEta[j]->SetTitle(titl);
      sprintf(titl,"cal_ratio_3_%d",j);
      rat3vsEta[j]=new TGraphErrors();
      rat3vsEta[j]->SetTitle(titl);

      ratio1[j]=new TH2F(titl,titl,ncols[j],0,ncols[j],nrows[j],0,nrows[j]);
      sprintf(titl,"rat1_%d",j);
      rat1[j]=new TH1F(titl,titl,200,.25,2.25);
      sprintf(titl,"ratioproj_%d",j);
      ratio2[j]=new TH2F(titl,titl,ncols[j],0,ncols[j],nrows[j],0,nrows[j]);
      sprintf(titl,"rat2_p_%d",j);
      rat2[j]=new TH1F(titl,titl,200,.25,2.25);

      sprintf(titl,"ratiog_%d",j);
      ratio3[j]=new TH2F(titl,titl,ncols[j],0,ncols[j],nrows[j],0,nrows[j]);
      sprintf(titl,"rat3_p_%d",j);
      rat3[j]=new TH1F(titl,titl,200,.25,2.25);

    }; //
  printf("constructor done\n");
};
void ProjectGCor::CleanCalib(CalibStr* ccl)
{

  for(int j=0;j<4;j++)
    {
      int nrows=ccl->tm(2,j+1)->GetNrows();
      int ncols=ccl->tm(2,j+1)->GetNcols();
      for(int nr=0;nr<nrows;nr++)
	{
	  for(int nc=0;nc<ncols;nc++)
	    {
	      if(!(Mgr->Rdep->Legal(2,j+1,nr,nc)))
		{
		  ccl->SetValue(2,j+1,nr,nc,0);
		  printf("(j+1=%d nr=%d nc=%d )=zero\n",j+1,nr,nc);
		}
	      else
		{
		 float  vval=ccl->GetValue(2,j+1,nr,nc);
		  if(fabs(vval)>20)vval=20;
		  ccl->SetValue(2,j+1,nr,nc,vval);
		  printf("(j+1=%d nr=%d nc=%d )=%f\n",j+1,nr,nc,ccl->GetValue(2,j+1,nr,nc));

		};
	    }
	}
    }
}
ProjectGCor::~ProjectGCor()
{
  if(Mgr2)delete Mgr2;
  if(Mgr1)delete Mgr1;
  for(int j=0;j<4;j++)
    {
      if(ratio1[j])delete ratio1[j];
      if(ratio2[j])delete ratio2[j];
      if(ratio3[j])delete ratio3[j];
      if(rat1[j])delete rat1[j];
      if(rat2[j])delete rat2[j];
      if(rat3[j])delete rat3[j];
      if(rat1vsEta[j])delete rat1vsEta[j];
      if(rat3vsEta[j])delete rat3vsEta[j];
    }
};
CalibStr* ProjectGCor::ProjectMgr1()
{
  if(!Mgr1)return 0;
  if(!Mgr2)return 0;

  CalibStr* Calnew=new CalibStr(12000000,CalibPath1);
  TString StripName=CalibPath1;
  int nparts= StripName.Tokenize("/")->GetEntries();
  if(nparts>1)
    {
      TObjString* tos=(TObjString*) (StripName.Tokenize("/")->At(nparts-1));
      StripName= tos->GetString();
    }
  printf("StripName=%s \n",StripName.Data());
  Calnew->filename=StripName;
  Calnew->OutName=TString("WW")+StripName;
  TIter nexT(Calnew->M_array);
  while(TMatrix* t=(TMatrix*) nexT())(*t)=0;
  TIter next(Mgr1->Rdep->array);
  CellTDep* c=0;
  CellTDep* c2=0;
  printf("t zeroed\n");
  while(c=(CellTDep*) next())
    {
      int nstb=c->GetNstb();
      int r0=c->GetRow0();
      int c0=c->GetCol0();
      float newcal=0.;
      if(Mgr->Rdep->Legal(2,nstb,r0,c0))
	{
	  bool allgood=true;
	  newcal=1.01;
	  c2=Mgr2->Rdep->Cdep(nstb,r0,c0);	  
	  float oldcal=Cal1->GetValue(2,nstb,r0,c0);
	  printf("nstb=%d r0=%d c0=%d oldcal=%f \n",nstb,r0,c0,oldcal);
	  newcal=oldcal;
	  if(!(c2->GetGraph()))allgood=false;
	  if(!(c->GetGraph()))allgood=false;
	  if(allgood)
	    {
	      TF1* fn1=0;
	      TF1* fn2=0;
	      if(!(fn2=c->GetGraph()->GetFunction("pol0")))allgood=false;
	      if(!(fn1=c2->GetGraph()->GetFunction("pol0")))allgood=false;
	      if(allgood)
		{
		  if(fn1->GetNDF()<3) allgood==false;
		  if(allgood)
		    {
		      if(fn1->GetChisquare()/fn1->GetNDF()>10)allgood==false;
		    }
		}
	      if(allgood)
		{
		  if(fn2->GetNDF()<3) allgood==false;
		  if(allgood)
		    {
		      if((fn2->GetChisquare()/fn2->GetNDF())>10)allgood==false;
		    };
		}
	    };
	  float thisledADC2=c2->GetthisLedADC();
	  float thisledADC1=c->GetthisLedADC();
	  float nomledADC1=c->GetNominalLedADC();
	  float nomledADC2=c2->GetNominalLedADC();
	  
	  if(nomledADC2>1 && nomledADC1>1 && allgood)
	    {
	      //projection assumes newbain is oldgain scaled by ratio of base 
	      //led ratio for old and new (using only LedCorrectionFraction 
	      //of change
	      float fcor=nomledADC1/nomledADC2;
	      fcor=fcor-1.;
	      fcor=fcor*LedCorrectionFraction;
	      fcor=fcor+1.;
	      if(.1<nomledADC1/nomledADC2<10)newcal=oldcal*fcor;
	    }
	  
	  Calnew->SetValue(2,nstb,r0,c0,newcal);
	  printf("  ...nstb=%d r0=%d c0=%d nomledADC1=%f nomledADC2=%f newcal=%f \n",nstb,r0,c0,nomledADC1,nomledADC2,newcal);
	}
    }
  CleanCalib(Calnew);
  Calnew->UpdateFile(7); 
  
  return Calnew;
}
void ProjectGCor::ScaleCalibStr(CalibStr* cal,int Nstb,float scalefactor)
{
  if(!cal)return;
  if(!(Nstb>0&&Nstb<5))return;
  if(!Mgr1)return;
  Int_t nrows[4]={34,34,24,24};
  Int_t ncols[4]={17,17,12,12};
  for(int nr0=0;nr0<nrows[Nstb-1];nr0++)
    {
      for(int nc0=0;nc0<ncols[Nstb-1];nc0++)
	{
	  if(Mgr1->Rdep->Legal(2,Nstb,nr0,nc0))
	    {
	      float val=cal->GetValue(2,Nstb,nr0,nc0);
	      val=val*scalefactor;
	      cal->SetValue(2,Nstb,nr0,nc0,val);
	    }
	};
    }
}
void ProjectGCor::SetupMgr (int one_two, CalibStr* cal, Int_t runNumber, Int_t baseNumber)
{
  char* path=(char*) RundepPath.Data();
  Mgr=0;
  if(one_two==1)
    {
      Mgr1=new RunDepMgr(path);
      Mgr=Mgr1;
    }
  if(one_two==2)
    {
      Mgr2=new RunDepMgr(path);
      Mgr=Mgr2;
    }
  if(!Mgr)
    {printf("Error in call to SetMgr \n ");return;};
  Mgr->SetRdep(runNumber);
  Mgr->SetBase(baseNumber);

}
void ProjectGCor::PrintMgr( RunDepMgr* mgr, int printopt,int set12)
{
  /*
    printopt 0 minimal
    if(printopt&(0x1) print mvsday
    if(printopt&(0x2) print CellTDep inv
   */
  CalibStr* cal=0;
  int Pathbase=0;
  Long_t rdepN=0;
  Long_t  baseN=0;
  if(mgr->Rdep)
    {
      rdepN=mgr->Rdep->RunNumber;
      baseN=mgr->Rdep->BaseRunNumber;
      if(mgr->Base)
	{
	  if(mgr->Base->RunNumber==baseN)
	    {printf("Base In Mgr matches Rdep value\n");}
	  else
	    {printf("Mismatch Base In Mgr with Rdep value\n");};
	}
    }

  if(set12==1)
    {
      cal=Cal1;
      Pathbase=PAthBase1;
    }
  if(set12==2)
    {
      cal=Cal2;
      Pathbase=PAthBase2;
    }
  if(mgr->mvsday)
    {
      printf("mvsday array found with %d entries\n",mgr->mvsday->GetEntries());
      if(printopt&(0x1))
	{
	  TIter next(mgr->mvsday);
	  TGraphErrors* gr;
	  while(gr=(TGraphErrors*) next())
	    {
	      int npoints=gr->GetN();
	      printf("mass  %d days for day %f to day %f \n",npoints,
		     gr->GetX()[0],gr->GetX()[npoints-1]);
	    }
	}
    }
  if(!mgr->Rdep)
    {
      printf("Rdep not defined\n");
      return;
    };
  printf("Cells for rdep=%d base=%d \n",(int) mgr->Rdep->RunNumber,(int) mgr->Base->RunNumber);
  int ncells=mgr->Rdep->array->GetEntries();
  printf("array of CellTDep with %d entries\n",ncells);
  int loopcnt=0;
  int nbr=mgr->Rdep->RunNumber;
  
  if(ncells>0 && printopt&(0x2))
    {
      TIter next(mgr->Rdep->array);
      CellTDep* c;
      while(c=(CellTDep*) next())
	{
	  loopcnt++;
	  int ngrpnts=0;
	  int nstb,row0,col0;
	  float cthis,cnom;
	  TGraphErrors* gr=c->GetGraph();
	  if(gr)ngrpnts=gr->GetN();
	  printf("%d) %d nstb=%d row0=%d col=%d \n (base(nominal led)=%f this(led)=%f Gr Pnts =%d\n",loopcnt,nbr,
		 nstb=c->GetNstb(),row0=c->GetRow0(),col0=c->GetCol0(),
		 cnom=c->GetNominalLedADC(),cthis=c->GetthisLedADC(),
		 ngrpnts);
	  if(cal)
	    {
	      float gcor=cal->GetValue(2,nstb,row0,col0);
	      float ledgcor=gcor;
	      if(cthis>0)ledgcor=ledgcor*cnom/cthis;
	      printf("              raw gcor=%f  gcor+led=%f \n",gcor,ledgcor);
	    }
	};
    };
  
}


bool ProjectGCor::checkMgr(int one_two)
{
  RunDepMgr* mgr=0;
  CalibStr* cal=0;
  if(one_two==1)
    {
      mgr=Mgr1;
      cal=Cal1;
    };
  if(one_two==2)
    {
      mgr=Mgr2;
      cal=Cal2;
    };
  if(! (cal&&mgr))return false;
  if(!mgr->Rdep)return false;
  if(!mgr->Base)return false;
  if(mgr->Base->RunNumber != mgr->Rdep->BaseRunNumber)return false;
  if(mgr->Rdep->array->GetEntries()<1000)return false;
  if(mgr->Rdep->Location[1][5][5]<1)return false;
  return true;
};

void ProjectGCor::HistCompare12()
{
  float rowlims[2];
  rowlims[0]=-100;
  rowlims[1]=100;

  TString RowRange=gSystem->Getenv("RowRange");
  if(RowRange!="") sscanf(RowRange.Data(),"%f %f",&(rowlims[0]),&(rowlims[1]));
  printf("rowlims=%f %f \n",rowlims[0],rowlims[1]);
  if((!(Mgr1&&Mgr2)))
    {
      printf("HistCompare:: failed with missing Mgr\n");
      return;
    } ;
  if(!checkMgr(1))
    {
      printf("Mgr1 check failed\n");
    };
  if(!checkMgr(2))
    {
      printf("Mgr2 check failed\n");
    };
  
  TIter next(Mgr1->Rdep->array);
  CellTDep* c1;
  CellTDep* c2;
  while(c1= (CellTDep*) next())
    {
      int nstb=c1->GetNstb();
      int r0=c1->GetRow0();
      int c0=c1->GetCol0();
      if(Mgr->Rdep->Legal(2,nstb,r0,c0) &&r0>rowlims[0]-1&&r0<rowlims[1]+1 )
	{
	  float Y=0.;
	  float dx,dy;
	  if(nstb<3)
	    {
	      dx=(c0+.5)*5.81;
	      dy=(r0-17+.5)*5.81;
	    }
	  else
	    {
	      dx=(c0+.5)*3.81;
	      dy=(r0-12+.5)*3.81;
	    };
	  Y=-log(sqrt(dx*dx+dy*dy)/731./2.);
	  
	  
	  c2=Mgr2->Rdep->Cdep(nstb,r0,c0);
	  float led2=c2->GetthisLedADC();
	  float led1=c1->GetthisLedADC();
	  float ledratio=0;
	  if(led1)ledratio=led2/led1;
	  int bin=ratio1[nstb-1]->FindBin(c0+.5,r0+.5);

	  ratio1[nstb-1]->SetBinContent(bin,ledratio);
	  int revN=rat1vsEta[nstb-1]->GetN();
	  rat1vsEta[nstb-1]->SetPoint(revN,Y,ledratio);
	  rat1[nstb-1]->Fill(ledratio);
	  
	  float led1b=c1->GetNominalLedADC();
	  float led2b=c2->GetNominalLedADC();
	  float base1tothis1=0;
	  if(led1)base1tothis1=led1b/led1;
	  float base2tothis2=0;
	  if(led2)base2tothis2=led2b/led2;
	  float gn1=Cal1->GetValue(2,nstb,r0,c0);
	  float gn2=Cal2->GetValue(2,nstb,r0,c0);
	  float rawgn2togn1=0;
	  if(gn1)
	    {
	      rawgn2togn1=gn2/gn1;
	      int revN=rat3vsEta[nstb-1]->GetN();
	      rat3vsEta[nstb-1]->SetPoint(revN,Y,rawgn2togn1);
	    }
	  
	  float gn1Wledcor=gn1;
	  if(led1)gn1Wledcor=gn1*((led1b/led1-1)*LedCorrectionFraction+1.);
	  float gn2Wledcor=gn1;
	  if(led2)gn2Wledcor=gn2*((led2b/led2-1)*LedCorrectionFraction+1.);
	  bin=ratio2[nstb-1]->FindBin(c0+.5,r0+.5);
	  if(rawgn2togn1)
	    {
	      ratio3[nstb-1]->SetBinContent(bin,rawgn2togn1);
	      rat3[nstb-1]->Fill(rawgn2togn1);
	    }
	  if(gn1Wledcor)
	    {
	      ratio2[nstb-1]->SetBinContent(bin,gn2Wledcor/gn1Wledcor);
	      rat2[nstb-1]->Fill(gn2Wledcor/gn1Wledcor);
	      printf("nstb=%d row0=%d col0=%d led1=%f led1b=%f led2=%f led2b=%f gn1=%f gn2=%f gn1Wledc=%f gn2Wledc=%f \n",nstb,r0,c0,led1,led1b,led2,led2b,gn1,gn2,gn1Wledcor,gn2Wledcor);
	    };
	};
    }
}

CalibStr* ProjectGCor::ProjectTo(Int_t RunNumber,Int_t OldRunNumber)
{
  /*
    Based on all available information, we project calibration from 
    OldRunNumber ( calibrated with Cal1 with implied base of OldPathBase
    to
    RunNumber (calibrated with Cal1out and with self reference base)
   
    Plot ratios of gains between RunNumber and OldRunNumber based on cal1 and cal2
    Plot ratios of led between RunNumber and OldRunNumber 
   */
  printf("call ProjectTo\n");
  RunDepCor* rdold=0;
  RunDepCor* rdoldbase=0;

 if(PAthBase1)
    {
      printf("setup OLDPathBase\n");
      Mgr->SetRdep(OldRunNumber,false);
      Mgr->SetBase(PAthBase1,false);
      rdold=Mgr->Rdep;
      rdoldbase=Mgr->Base;
      printf("rdold->array len:%d\n",rdold->array->GetEntries());
    };
  Mgr->SetRdep(RunNumber,false);
  Mgr->SetBase(PAthBase2,false);
  printf("Mgr setup to %d base: %d \n",RunNumber,PAthBase1);
  //Mgr is set up to deal with RunNumber and its base OldRunNumber
  Int_t nrows[4]={34,34,24,24};
  Int_t ncols[4]={17,17,12,12};
  for(int j=0;j<4;j++)
    {
      if(ratio1[j])delete ratio1[j];
      if(ratio2[j])delete ratio2[j];
      if(ratio3[j])delete ratio3[j];
      if(rat1[j])delete rat1[j];
      if(rat2[j])delete rat2[j];
      if(rat3[j])delete rat3[j];
      char titl[100];
      sprintf(titl,"ratio12_%d",j);
      ratio1[j]=new TH2F(titl,titl,ncols[j],0,ncols[j],nrows[j],0,nrows[j]);
      sprintf(titl,"rat1_%d",j);
      rat1[j]=new TH1F(titl,titl,200,.1,5.);
      sprintf(titl,"ratioproj_%d",j);
      ratio2[j]=new TH2F(titl,titl,ncols[j],0,ncols[j],nrows[j],0,nrows[j]);
      sprintf(titl,"rat2_p_%d",j);
      rat2[j]=new TH1F(titl,titl,200,.1,5.);

      sprintf(titl,"ratiogcor_%d",j);
      ratio3[j]=new TH2F(titl,titl,ncols[j],0,ncols[j],nrows[j],0,nrows[j]);
      sprintf(titl,"rat3_p_%d",j);
      rat3[j]=new TH1F(titl,titl,200,.1,5.);

    };
  printf("Mgr setup to %d base: %d  phase 2\n",RunNumber,PAthBase2);
  for(int nstb=1;nstb<5;nstb++)
    {
      for(int row0=0;row0<nrows[nstb-1];row0++)
	{
	  for(int col0=0;col0<ncols[nstb-1];col0++)
	    {
	      if(Mgr->Rdep->Legal(2,nstb,row0,col0))
		{
		  int nbin=ratio1[nstb-1]->FindBin(col0+.5,row0+.5);
		  ratio1[nstb-1]->SetBinContent(nbin,1.);
		  ratio2[nstb-1]->SetBinContent(nbin,1.);
		  ratio3[nstb-1]->SetBinContent(nbin,1.);
		  CellTDep* c;
		  CellTDep* cb;
		  printf("Mgr->Rdep->Cdep: nstb:%d row0%d col0%d \n",nstb,row0,col0);
		  printf("Mgr->Rdep->array len:%d\n",Mgr->Rdep->array->GetEntries());
		  c=Mgr->Rdep->Cdep(nstb,row0,col0);
		  if(!c)printf("fail\n");
		  if(c)
		    {
		      //for each cell in the RunNumber file we Get the CellTDep* c element
		      //we extract the run average led by pulling the fit from LED data for that cell/run
		      //
		      cb=0;
		      printf("cb nentries=%d\n",rdold->array->GetEntries());
		      printf("ok Rdep->Cdep\n");
		      if(rdold)cb=rdold->Cdep(nstb,row0,col0);
		      printf("rdold->array len:%d\n",rdold->array->GetEntries());
		      printf("ok rdold->Cdep\n");
		      TGraphErrors* gg;
		      if(gg=c->GetGraph())
			{
			  if(gg->GetFunction("pol0"))
			    {
			      if(gg->GetN()>10)
				{
				  Float_t nom=c->GetNominalLedADC();
				  Float_t thadc=c->GetthisLedADC();
				  Float_t oldval=Cal1->GetValue(2,nstb,row0,col0);
				  Float_t cal2val=oldval;
				  if(Cal2)cal2val=Cal2->GetValue(2,nstb,row0,col0);
				  Float_t rc12=0;
				  Float_t rp12=0;
				  Float_t rg12=0;
				  Float_t nom2=nom;
				  float_t thadc2=thadc;
				  printf("c: nstb:%d row0%d col0%d thadc=%f\n",nstb,row0,col0,thadc);
				  if(cb)
				    {
				      printf("cb: nstb:%d row0%d col0%d thadc2=%f\n",nstb,row0,col0,thadc2);
				      nom2=cb->GetNominalLedADC();
				      thadc2=cb->GetthisLedADC();
				    };
				  if(oldval!=0)
				    {
				      rc12=cal2val/oldval*nom2/thadc2*thadc/nom;
				      rg12=cal2val/oldval;
				    }
				  

				  //rc12 is the ratio of the correction from the two files 
				  // Float_t corr= Mgr->Rdep->GetCor(20,1,nstb,row0,col0,Cal1);
				  if(thadc>0)
				    {
				      //corr is the correction for current run based on current corrections  
				      //Cal1 and led scaling from Rdep->Base
				      Float_t corr=oldval*nom/thadc;

				      // this correction is the result of the old file value scaled by led ratio
				      // from OLDPathBase to OldRunNumber 

				      if(thadc!=0)rp12=nom/thadc;
				      //rp12 is essentially the ratio of this to nominal led averages
				      Cal1Out->SetValue(2,nstb,row0,col0,corr);
				      //CallOut is set to reflect the old file value multiplied by the led run ratio
				      //there is no accounting here for the led ratio needed to project Cal1 to OldRunNumber 
				      // that is-- no reference to OldRunNumber's Base with respect to the file calibration Cal1
				      nbin=ratio1[nstb-1]->FindBin(col0+.5,row0+.5);
				      ratio1[nstb-1]->SetBinContent(nbin,rc12);
				      ratio2[nstb-1]->SetBinContent(nbin,rp12);
				      ratio3[nstb-1]->SetBinContent(nbin,rg12);
				      rat1[nstb-1]->Fill(rc12);
				      rat2[nstb-1]->Fill(rp12);
				      rat3[nstb-1]->Fill(rg12);
				      if(kPrint)
					{
					  printf("nomled=%3.3f thisled=%3.3f Cal1=%3.3f Cal2=%3.3f projCal2=%3.3f  \n",nom,thadc,oldval,cal2val,corr);
					};
				    };
				};
			    }
			}
		    }
		}
	    }
	}
    };
 
  return Cal1Out;
};

