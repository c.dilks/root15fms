#ifndef BSFix_
#define BSFix_
#include "Yiqun.h"
#include "Rtypes.h"
#include "TF1.h"
class BSFix : public TObject 
{
 public:
  BSFix(Yiqun *y_=0, Double_t dayNum_ =60., Double_t fudgeFactor_ =1,Geom* pgeom=0);
  
  ~BSFix();
  Geom* p_geom;
  Yiqun* Y;

  TF1 *pTimeEtaFuncL, *pTimeEtaFuncS;
  TF1 *pSlopeFuncL, *pSlopeFuncS;

  Int_t CreateBSFuncs();
  Double_t GetETrue(Bool_t isLarge_ = true, Float_t eta_ = 3.1, Float_t days_ = 52, Float_t energy_ =10., Float_t fudgeFactor=1.);

 private:
  ClassDef(BSFix,1);
};
#endif

