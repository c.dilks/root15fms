{
  gStyle->SetOptStat(0);
  TFile* file=new TFile("adcTr.root");
  TTree* tradc=file->Get("Tr_adc");
  TCanvas* c2=new TCanvas("c2","c2");
  TCanvas* c1=new TCanvas("c1","c1");
  tradc->Draw("evtnum>>zn(1000,0,1000)","led==0");
  c1->SetLogz();
  c1->Print("evtdsp.ps(");
  TH2F* his[1000];
  TH2F* hist[1000];
  TH1F* tdc[1000];
  char nam[100];
  char cutbase[100];
  char cuta[100];
  char cutt[100];
  char cuttd[100];
  char vara[300];
  char vart[300];
  char vartd[300];
  printf("ok1\n");
  for(int j=1;j<20;j++)
    {
      c1->cd();
      printf("j=%d\n",j);
      sprintf(nam,"LargeCellTDCEv%d",j);
      tdc[j]=new TH1F(nam,nam,16,0,16);
      sprintf(vartd,"TDC>>%s",nam);
      sprintf(nam,"LargeCellTimesEv%d",j);
      his[j]=new TH2F(nam,nam,34,-17,17,34,-17,17);
      printf("ok2\n");
      sprintf(cutbase,"led==0&&nstbADC<3&&evtnum==%d&&ClInd<3",j);
      sprintf(cuta,"ADC*(%s)",cutbase);
      sprintf(cutt,"TDC*(%s)",cutbase);
      sprintf(cuttd,"%s",cutbase);
      printf("ok3\n");
      sprintf(vara,"-(rowADC-16.5):((nstbADC==2)-(nstbADC==1))*(colADC+.5)>>%s",nam);
      sprintf(nam,"T_large%d",j);
      hist[j]=new TH2F(nam,nam,34,-17,17,34,-17,17);
      sprintf(vart,"-(rowADC-16.5):((nstbADC==2)-(nstbADC==1))*(colADC+.5)>>%s",nam);
      his[j]->GetZaxis()->SetTitle("color:ADC text:TDC"); 
      cout<<nam<<"\n";
      cout<<cuta<<"\n";
      cout<<vara<<"\n";

      if(zn->GetBinContent(j)>5)
	{
	  tradc->Draw(vart,cutt,"text");
	  tradc->Draw(vara,cuta,"zcol");
	  hist[j]->Draw("sametext");
	  c1->Update();
	  c2->cd();
	  tradc->Draw(vartd,cuttd);
	  c2->Update();
	  c2->Print("evtdsp.ps");
	  c1->Print("evtdsp.ps");
	}
    }
  c1->Clear();
  c1->Print("evtdsp.ps)");
  gSystem->Exec("ps2pdf evtdsp.ps;rm evtdsp.ps;cp evtdsp.pdf ../../");
}
