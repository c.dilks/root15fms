{
  gROOT->Reset();
  gROOT->LoadMacro("CodeSmall/Legal.C");
  gROOT->LoadMacro("CodeSmall/LogWt.C");
  gStyle->SetPalette(1);
  gStyle->SetOptFit();
  TCanvas* c2=new TCanvas("c2","c2",600,600);
  TCanvas* c1=new TCanvas("c1","c1",600,600);

   TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("adcTr.root");
   if (!f) {
      f = new TFile("adcTr.root");
   }
   TTree *Tr_adc = (TTree*)gDirectory->Get("Tr_adc");

//Declaration of leaves types
   Int_t           ADC;
   Int_t           rowADC;
   Int_t           colADC;
   Int_t           nstbADC;
   Int_t           runnum;
   Int_t           evtnum;
   Int_t           segnum;
   Int_t           led;
   Int_t           Bunchid7bit;

   // Set branch addresses.
   Tr_adc->SetBranchAddress("br_ADC",&ADC);
   Tr_adc->SetBranchAddress("br_rowADC",&rowADC);
   Tr_adc->SetBranchAddress("br_colADC",&colADC);
   Tr_adc->SetBranchAddress("br_nstbADC",&nstbADC);
   Tr_adc->SetBranchAddress("br_runnum",&runnum);
   Tr_adc->SetBranchAddress("br_evtnum",&evtnum);
   Tr_adc->SetBranchAddress("br_segnum",&segnum);
   Tr_adc->SetBranchAddress("br_led",&led);
   Tr_adc->SetBranchAddress("br_Bunchid7bit",&Bunchid7bit);

//     This is the loop skeleton
//       To read only selected branches, Insert statements like:
// Tr_adc->SetBranchStatus("*",0);  // disable all branches
// TTreePlayer->SetBranchStatus("branchname",1);  // activate branchname
   TFile fo("adcCor.root","recreate");
   Long64_t nentries = Tr_adc->GetEntries();

   TH2F* LNLave=new TH2F("LNLave","LNLave",17,0,17,34,0,34);
   TH2F* LSLave=new TH2F("LSNLave","LSLave",17,0,17,34,0,34);
   TH2F* SNLave=new TH2F("SNLave","SNLave",12,0,12,24,0,24);
   TH2F* SSLave=new TH2F("SSLave","SSLave",12,0,12,24,0,24);
   TH2F* Lave[4];
   Lave[0]=LNLave;
   Lave[1]=LSLave;
   Lave[2]=SNLave;
   Lave[3]=SSLave;
   for(int j=0;j<4;j++)
     {
       Lave[j]->SetStats(0);
     }
   
   TH1D* adc[4][34][17];
   TH1D* adcL[4][34][17];
   TGraph* tdep[4][34][17];
   TGraphErrors* tdepr[4][34][17];
   TString name[4][34][17];
   TString Lname[4][34][17];
   TString Tname[4][34][17];
   TString Tnamer[4][34][17];
   Int_t nr[4]={34,34,24,24};
   for(int d=0;d<4;d++)
     {
       for(int row=0;row<nr[d];row++)
	 {
	   for(int col=0;col<nr[d]/2;col++)
	     {
	       char str[100];
	       sprintf(str,"adcr%d_c%d_%d",row,col,d);
	       cout<<str<<"\n";
	       adc[d][row][col]=new TH1D(str ,str,4096,0,4095);
	       name[d][row][col]=str;
	       sprintf(str,"Ladcr%d_c%d_%d",row,col,d);
	       adcL[d][row][col]=new TH1D(str ,str,4096,0,4095);
	       adcL[d][row][col]->SetLineColor(4);
	       Lname[d][row][col]=str;
	       sprintf(str,"Tadcr%d_c%d_%d",row,col,d);
	       tdep[d][row][col]=new TGraph();
	       tdep[d][row][col]->SetName(str);
	       tdep[d][row][col]->SetMarkerSize(.2);
	       tdep[d][row][col]->SetMarkerColor(3);	       
	       Tname[d][row][col]=str;
	       sprintf(str,"rTadcr%d_c%d_%d",row,col,d);
	       tdepr[d][row][col]=new TGraphErrors(0);
	       tdepr[d][row][col]->SetName(str);
	       tdepr[d][row][col]->SetMarkerSize(.2);	       
	       tdepr[d][row][col]->SetMarkerColor(2);	       
	       Tnamer[d][row][col]=str;
	     };
	 };
     };
   int cnt=0;
   Long64_t nbytes = 0;
   printf("loop through %d adc hits\n",nentries);
   if(nentries>100000000)nentries=100000000;
   nentries=10000000;
   for (Long64_t i=0; i<nentries;i++) {
      nbytes += Tr_adc->GetEntry(i);
      if(Legal(2,nstbADC,rowADC,colADC))
	{

	  if(cnt++%100000==0)printf("cnt=%d adc hits\n",cnt);
	  if(led==0)
	    {
	      adc[nstbADC-1][rowADC][colADC]->Fill(ADC);
	    }
	  else
	    {
	      adcL[nstbADC-1][rowADC][colADC]->Fill(ADC);
	      TGraph* pg=tdep[nstbADC-1][rowADC][colADC];
	      TGraphErrors* pgr=tdepr[nstbADC-1][rowADC][colADC];
	      int np=pg->GetN();
	      int n0=np-40;
	      if(n0<0)n0=0;
	      Float_t acum=0;
	      Double_t x,y;
	      acum=ADC;		      
	      y=acum;
	      if(y<=0)y=.1;
	      Double_t rr=(runnum-12075028);
	      x=rr+(segnum*10000+evtnum)/1500000.;
	      pg->SetPoint(np,x,y);
	    };
	}
   };
   c2->Clear();   
   c2->Print("Lave.ps(");
   for(int d=0;d<4;d++)
     {
       c2->Clear();
       c2->Divide(4,4);
       Int_t padcnt=0;

       for(int row=0;row<nr[d];row++)
	 {
	   for(int col=0;col<nr[d]/2;col++)
	     {
	       if(Legal(2,d+1,row,col))
		 {
		   padcnt++;
		   TGraph* pg=tdep[d][row][col];
		   TGraphErrors* pgr=tdepr[d][row][col];
		   int N0= pg->GetN();
		   int NN0=pgr->GetN();
		   if(N0>40)
		     {
		       for(int nn=20;nn<N0;nn=nn+40)
			 {
			   Double_t x0,y0;
			   pg->GetPoint(nn,x0,y0);
			   Double_t x10=x0;
			   Double_t y10=0;
			   Double_t sig10=0;
			   for(int np=nn-20;np<nn+20;np++)
			     {
			       pg->GetPoint(np,x0,y0);
			       y10+=y0;
			       sig10+=y0*y0;
			     };
			   y10=y10/40;
			   sig10=sqrt((sig10-40*y10*y10))/40;
			   //			   printf("x10=%f y10=%f err=%f \n",x10,y10,sig10);
			   NN0=pgr->GetN();
			   pgr->SetPoint(NN0,x10,y10);
			   pgr->SetPointError(NN0,0,sig10);
			 }
		     };

		   if(padcnt>16)
		     {
		       c2->Update();
		       c2->Print("Lave.ps");
		       padcnt=1;
		     };
		   c2->cd(padcnt);
		   c2->GetPad(padcnt)->SetLogy();
		   c2->GetPad(padcnt)->SetLogx();
		   adc[d][row][col]->Draw();
		   adcL[d][row][col]->Draw("same");
		   Float_t val=LogWt(adc[d][row][col]);
		   val=adc[d][row][col]->Integral(160,4096);
		   Lave[d]->Fill(col+.5,row+.5,val);
		   cout<<name[d][row][col]<<" => "<<val<<"\n";
		   padcnt++;
		   c2->cd(padcnt);
		   //		   tdep[d][row][col]->Draw("A*");
		   if(tdepr[d][row][col]->GetN()>1)
		     tdepr[d][row][col]->Draw("A*");
		   Float_t chi=0;
		   Float_t dof=0;
		   Float_t chipd=chi;
		   tdepr[d][row][col]->Write(Tnamer[d][row][col]);
		   if(tdepr[d][row][col]->GetN()>3)
		     {
		       if(tdepr[d][row][col]->Fit("pol0")==0)
			 {		
			   printf("fit 1 good\n");
			   chi=tdepr[d][row][col]->GetFunction("pol0")->GetChisquare();
			   dof=tdepr[d][row][col]->GetFunction("pol0")->GetNDF();
			   chipd=chi;
			 };
		     };
		   if(dof>10)
		     {
		       chipd=chi/dof;
		       
		       if(chipd>2)
			 {
			   printf("try pol3\n");
			   Int_t retval=tdepr[d][row][col]->Fit("pol3");
			   printf("Fit Retval=%d \n",retval);
			   if(retval==0)
			     {
			       chi=tdepr[d][row][col]->GetFunction("pol3")->GetChisquare();
			       dof=tdepr[d][row][col]->GetFunction("pol3")->GetNDF();
			       chipd=chi;
			       if(dof>2)chipd=chi/dof;
			     }
			   else
			     {
			       printf("failed pol3\n");
			     };
			 }
		     };
		   
		   adc[d][row][col]->Write(name[d][row][col]);
		   adcL[d][row][col]->Write(Lname[d][row][col]);
		   tdep[d][row][col]->Write(Tname[d][row][col]);
		   tdepr[d][row][col]->Write(Tnamer[d][row][col]);
		 };
	     };
	 };
       c2->Print("Lave.ps");

       c1->cd();
       c1->SetLogz();
       Lave[d]->Draw("zcol");
       c1->Update();
       c1->Print("Lave.ps");
       c2->cd();

     };
   c1->Clear();
   c1->Print("Lave.ps)");
   system("ps2pdf Lave.ps;rm Lave.ps");
}
