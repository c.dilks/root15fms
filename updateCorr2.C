void updateCorr2(TString base, TString dest, TString baseCor,TString destCor)
{
  /*
  TString base="16128001";
  TString dest="16117001";
  TString baseCor="FmsCorrD128b.txt";
  TString destCor="FmsCorrD128b.txt";
  */
  cout<<baseCor<<"\n";
  Int_t ibase;
  Int_t idest;
  sscanf(((base+" ")+dest).Data(),"%d %d",&ibase,&idest);
  Int_t Yr,Day,Rn;
  Yr=idest/1000000;
  Day=idest/1000-Yr*1000;
  TString DirName=(base+"_to_")+dest;
  cout<<"DirName="<<DirName<<"\n";
  TString com=TString("touch ")+DirName;
  gSystem->Exec(com);
  TString com=TString("rm -r ")+DirName;
  gSystem->Exec(com);
  com=TString("mkdir ")+DirName;
  gSystem->Exec(com);
  gStyle->SetPalette(1);
  gStyle->SetOptFit();
  gSystem->Load("${FMSSRC}/Fpdchan.so");
  TString fmstxt=gSystem->Getenv("FMSTXT");
  TString RDpath=gSystem->Getenv("RunDepPath");
  fmstxt=fmstxt+"/";
  TString TSgcor1=fmstxt+baseCor;
  TString TSgcor2=fmstxt+destCor;
  cout<<TSgcor1<<"\n";
  cout<<TSgcor2<<"\n";
  cout<<RDpath<<"\n";
  ProjectGCor* projGC=new ProjectGCor(TSgcor1,RDpath,TSgcor2);
  projGC->SetPrint();
  CalibStr* calout=projGC->ProjectTo(idest,ibase);
  //  calout->UpdateFile(20);
  //  calout->Compare(2,1,projGC->Cal2);
  TCanvas* c1=new TCanvas("c1","c1");
  c1->Print(DirName+"/UpdCor.ps(");
  c1->Divide(2,2);
  for(int j=0;j<4;j++)
    {
      c1->cd(1);
      TString titl= projGC->ratio1[j]->GetTitle();
      titl=titl+dest;
      projGC->ratio1[j]->SetTitle(titl);
      projGC->ratio1[j]->Draw("zcol");
      c1->cd(2);
      projGC->rat1[j]->Fit("gaus","","",.7,1.3);
      projGC->rat1[j]->Draw();
      c1->cd(3);
      titl=projGC->ratio2[j]->GetTitle();
      titl=titl+base;
      projGC->ratio2[j]->SetTitle(titl);
      projGC->ratio2[j]->Draw("zcol");
      c1->cd(4);
      projGC->rat2[j]->Fit("gaus","","",.7,1.3);
      projGC->rat2[j]->Draw();
      c1->Print(DirName+"/UpdCor.ps");
    }
  c1->Clear();
  c1->Print(DirName+"/UpdCor.ps)");
  char cmd[200];
  sprintf(cmd,"cd %s;ps2pdf UpdCor.ps;rm UpdCor.ps",DirName.Data());
  gSystem->Exec(cmd);
  calout->filename=(TString("./")+DirName+"/FmsCorrWork.txt");
  calout->UpdateFile(20);
  sprintf(cmd,"cd %s;mv FmsCorrWork.txt_V20 FmsCorrW%d.txt;ls",DirName.Data(),Day);
  gSystem->Exec(cmd);
  cout<<cmd<<"\n";
}
 
