{
  gROOT->Macro("start.C");
  Float_t avecos=.75;
  Float_t avePol=.6;
  Float_t pcos=1./avecos/avePol;
  TCanvas* c1=new TCanvas("c1","c1",600,600);
  TChain* TwoTr=new TChain("TwoTr");
  TwoTr->Add("../Output/Outputset0*.root");
  TH1D* h[100];
  TF1* fh[100];
  Int_t cnt=0;
  TFile* ffout=new TFile("SpinAnal/ffout.root");
  TFile* fout=new TFile("FitTest.root","recreate");

  c1->Print("Etas.ps(");
  int etacnt=0;
  for(float eta=2.85;eta<4.;eta=eta+.1)
    {
      c1->Clear();
      c1->Divide(2,3);
      c1->cd(1);
      char cfout[200];
      sprintf(cfout,"DirEdcut%dset1fff_nb10",etacnt);

      cout<<"dir="<<cfout<<"\n";
      etacnt++;
      TH1F* CrossRat=(TH1F*) ((TDirectory*) ffout->Get(cfout))->Get("CrossRat");
      if(CrossRat==0)
	{

	  printf("CrossRat not found in %s \n",cfout);
	  ffout->ls();
	  ffout->((TDirectory*) ffout->Get(cfout))->ls();
	  return;
	};

      CrossRat->Scale(pcos);
      char titl1[100];
      sprintf(titl1,"Cross AN (pcos=%f)",pcos);
      CrossRat->GetYaxis()->SetTitle(titl1);
      CrossRat->GetXaxis()->SetRangeUser(0.,60.);
      CrossRat->GetYaxis()->SetRangeUser(-.1,.15 );
      CrossRat->Draw();

      TLine line(0.,0.,150.,0.);
      line.Draw("same");
      int encnt=0;
      TLine mlo(.035,0,.035,10000);
      TLine mhi(.235,0,.235,10000);
  for(float en=35;en<70;en=en+10)
    {
      encnt++;

      c1->cd(1+encnt);
      char fnam[100];
      sprintf(fnam,"ff%d",cnt);
      TF1* ff=new TF1(fnam,"[0]*exp(-1*pow((x-[1])/[2],2))+[3]*exp(-1*pow((x-[4])/[5],2))+[6]*exp(-1*pow((x-[7])/[8],2)) +[9]*pow(x,[10])*exp(-1*x/[11])");
      fh[cnt]=ff;
      ff->SetParLimits(0,1,500000);
      ff->SetParLimits(1,.125,.26);
      ff->SetParLimits(2,.02,.1);
      
      ff->SetParLimits(3,1,200000);
      ff->SetParLimits(4,.48,.6);
      ff->SetParLimits(5,.05,.12);
      
      ff->SetParLimits(6,1,10);
      ff->SetParLimits(7,.9,1.0);
      ff->SetParLimits(8,.08,.18);

      ff->SetParLimits(9,1,100000);
      ff->SetParLimits(10,1.4,1.9);
      ff->SetParLimits(11,.2,.35);


      char hnam[100];

      sprintf(hnam,"h%d",cnt);
      char cnam[100];

      sprintf(cnam,"abs(E12-%f)<5 && abs(M12-1)<1 && N12==2&&abs(Eta-%f)<.05",en,eta);
      h[cnt]=new TH1D(hnam,cnam,50,0.,1);
      TwoTr->Project(hnam,"M12",cnam);
      //      h[cnt]->Fit(fnam);
      int binid=h[cnt]->FindBin(.55);
      Float_t etamax=h[cnt]->GetBinContent(binid);
      //      h[cnt]->SetMaximum(etamax*1.5);
      printf("%s\n",cnam);
      h[cnt]->Draw();
      mlo->Draw("same");
      mhi->Draw("same");
      h[cnt]->Write();
      cnt++;
    }
  c1->Update();
  c1->Print("Etas.ps");

    };
  c1->Clear();
  c1->Print("Etas.ps)");

}
