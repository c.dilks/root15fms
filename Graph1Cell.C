{
  gROOT->Macro("start.C");
 TString RowNum=gSystem->Getenv("Row");
 TString ColNum=gSystem->Getenv("Col");
 TString NSTBNum=gSystem->Getenv("NSTB");

  TCanvas* c1=new TCanvas("c1","c1",700,700);
  Geom* p_geom=new Geom(p_files);
  TString Tgc=FMSTXT+"/"+FMSCORR;
  CalibStr* gcor=new CalibStr(9010000,Tgc);
  TFile* GR=new TFile("Graph.root","update");
  TFile* RD=new TFile("fmstxt/RunDep15.root");
  TList* lst= RD->GetListOfKeys();
  lst->ls();
  system("sleep 10");
  TKey* key=0;
  Int_t nrows[4]={34,34,24,24};
  TIter next(lst);
  int cnt=0;
  TGraphErrors* gr_all=new TGraphErrors;
  int nstb=1;
  int row=1;
  int col=3;
  sscanf((const char*) RowNum,"%d",&row);
  sscanf((const char*) ColNum,"%d",&col);
  sscanf((const char*) NSTBNum,"%d",&nstb);
  Int_t firstrun=16070001;
  Int_t lastrun= 16090001;
  char allname[200];
  sprintf(allname,"r%d_c%d_%d",row,col,nstb-1);
  gr_all->SetName(allname);
  gr_all->SetTitle(allname);
  gr_all->SetMarkerSize(.1);
  while(key=(TKey*) next())
    {
      RunDepCor* rd=(RunDepCor*) key->ReadObj();
      int RunNumber=rd->RunNumber;
      
      if(rd->RunNumber<firstrun || rd->RunNumber>lastrun)
	{
	  if(rd)delete rd;
	  RunNumber=0;
	  cout<<".";
	};
      if(RunNumber==0)continue;

      printf("run=%d\n",rd->RunNumber);
      TGraphErrors* gr=0;
      CellTDep* ct=0;
      if(!rd->Legal(2,nstb,row,col))exit();
      ct=rd->Cdep(nstb,row,col);
      if(ct)gr=ct->GetGraph();

      //      c1->Print("CellTDep.ps(");
      cnt++;
      if(cnt>400)continue;
      if(gr)
	{
	  int gcnt=gr->GetN();
	  int acnt=gr_all->GetN();
	  Double_t x,y,errx,erry;
	  for(int j=0;j<gcnt;j++)
	    {
	      gr->GetPoint(j,x,y);
	      Int_t ix1=(rd->RunNumber-firstrun)/1000;
	      Int_t ix0=-(1000*ix1-(rd->RunNumber-firstrun));
	      if(ix0>100)ix0=100;

	      x=1.*ix1+1.*ix0/100.+1.*x/100./2000000.;
	      erry=gr->GetErrorY(j);
	      gr_all->SetPoint(acnt,x,y);
	      gr_all->SetPointError(acnt,0,erry);
	      acnt++;
	      //	      printf("point%d x=%f10.7 y=%f10.7 \n",acnt,x,y);
	    };
	  GR->cd();
	  char nam[300];
	  sprintf(nam,"gr_%s",(const char*) rd->GetName());
	  printf("cnt=%d %s \n",cnt,(const char*) rd->GetName());
	  //	  gr->SetName(nam);
	  //	  gr->SetTitle(nam);
	  //	  gr->Write();
	  //	  gr_all->Write();
	  //	  gr->Draw("A*");
	  //	  c1->Update();
	  //	  c1->Print("CellTDep.ps");
	};
      GR->Purge();
      if(rd)delete rd;
    };
  gr_all->Draw("A*");
  //  c1->Print("CellTDep.ps)");
  //  system("ps2pdf CellTDep.ps; rm CellTDep.ps");
  GR->cd();
  gr_all->Write();
}
