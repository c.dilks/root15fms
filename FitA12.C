{
  gROOT->Macro("start.C");
  gROOT->LoadMacro("ChiCPD.C");
  float udwght=.0075;
  Float_t pol=.6;
  Float_t pcos=1./pol/.75;
  TGraphErrors Zeros;
  //  char* hexset="ffffffff";
  //  char* hexset="ffffffffffffffff_1fffffffffff";
  char* hexset="ffffffffffffffff_7ffffff";
  Int_t fset=5;
  TString FSET=gSystem->Getenv("FSET");
  if( FSET != "")
    {
      sscanf((const char*) FSET,"%d",&fset);
      printf("fset reset to %d \n",fset);
    }
  TLine lin[10];
  Float_t range[2]={-.3,.3};
  
  TCanvas* c1=new TCanvas("c1","c1",600,600);
  TCanvas* c2=new TCanvas("c2","c2",600,600);
  TChain* TwoTr=new TChain("TwoTr");
  
  TwoTr->Add("../Output/Outputset05*.root");
  //  TFile* ffout=new TFile("SpinAnal/fout12b.root");
  //  TFile* ffout=new TFile("SpinAnal/fout12f.root");
  TFile* ffout=new TFile("$FOUT");
  TFile* fout=new TFile("FitTest.root","update");
  if(!fout)fout=new TFile("FitTest.root","create");
  gStyle->SetOptFit();
  Float_t Mlow[10]={-.015,0,0,.4,.3,.38,0,0,0,0};
  Float_t Mhi[10]={.285,0,.4,.7,.8,.58,0,0,0,0};
  TLine llo(Mlow[fset],0,Mlow[fset],1000000);
  TLine lhi(Mhi[fset],0,Mhi[fset],1000000);
  char hf[200];
  char hcnt[200];
  sprintf(hf,"f%d",fset);
  TString Hf=hf;
  TString Hname="";
  //    char* Dirname="DirCPtcut%dset%s_nb10_f0";
  
  char* Dirname="DirCPtcut%dset%s_nb10_f%d";
  Float_t Ebins[5]={30,40,50,60,70};
  Float_t Ptbins[6]={1.5,2.5,3.5,4.5,5.5,6.5};
  
  /*
    char* Dirname="DirCPt2cut%dset%s_nb10_f%d";
    Float_t Ebins[5]={27.5,32.5,37.5,42.5,47.5};
    Float_t Ptbins[6]=    {1.5,2.0,2.5,3.0,3.5,4.0};
  */
  
  /*
    char* Dirname="DirCPt3cut%dset%s_nb10_f0";
    Float_t Ebins[5]={47.5,52.5,57.5,62.5,67.5};
    Float_t Ptbins[6]=    {1.5,2.0,2.5,3.0,3.5,4.0};
  */
  
  TH1F* MEmpty=new TH1F("MEmpty","MEmpty",100,0,1);
  TF1* lfun=new TF1("lfun","[0]+[1]*x",-1,1.);
  TGraphErrors* grpt[6];
  TGraphErrors* gre[6];
  TF1* fits[100];
  for(int j=0;j<100;j++)fits[j]=0;
  for(int j=0;j<6;j++){gre[j]=new TGraphErrors();grpt[j]=new TGraphErrors();};
  char cfoutpt[200];
  int counter=0;
  int colors[6]={1,2,3,4,6,7};
  char stit[100];
  c1->Print("ANplots.ps(");
  for(int ecnt=0;ecnt<5;ecnt++)
    {
      Float_t En=Ebins[ecnt];
      c1->Clear();
      c1->Divide(3,2);
      c2->Clear();
      c2->Divide(3,2);
      for(int ptcnt=0;ptcnt<6;ptcnt++)
	{
	  Float_t pt=Ptbins[ptcnt];
	  //	  
	  sprintf(cfoutpt,Dirname,counter,hexset,fset);
	  sprintf(hcnt,"h%d",counter);
	  if(ffout->Get(cfoutpt)==0)
	    {
	      printf("Not Found: %s\n",cfoutpt);
	      return;
	    }
	  TH1F* Asm=((TDirectory*) ffout->Get(cfoutpt))->Get("Asm");
	  TGraphErrors* GAsm=new TGraphErrors();
	  int jcnt=0;
	  for(int j=1;j<11;j++)
	    {
	      if((Asm->GetBinContent(j)!=0.))
		{
		  GAsm->SetPoint(jcnt,
				 Asm->GetBinCenter(j),Asm->GetBinContent(j));
		  GAsm->SetPointError(jcnt,
				      0,Asm->GetBinError(j));
		  jcnt++;
		};
	    };

	  TH1F* M12plt1=((TDirectory*) ffout->Get(cfoutpt))->Get("M12plt");
	  int nfp=Zeros.GetN();
	  if(Asm->GetFunction("pol1"))
	    {
	      Zeros.SetPoint(nfp,
			     Asm->GetFunction("pol1")->GetParameter(1),
			     Asm->GetFunction("pol1")->GetParameter(0));
	      
	      Zeros.SetPointError(nfp,
				  Asm->GetFunction("pol1")->GetParError(1),
				  Asm->GetFunction("pol1")->GetParError(0));
	    };

	  cout<<Asm->GetTitle()<<"\n";
	  TF1* pl1=new TF1("pl1","[0]+[1]*x",-1.,1.);
	  pl1->SetParameter(0,udwght);
	  pl1->FixParameter(0,udwght);
	  printf("here is a fit *******************************\n");
	  c1->cd(ptcnt+1);
	  if(GAsm->GetN()>0)GAsm->Fit("pl1","b","",-1.,1.);
	  GAsm->GetXaxis()->SetTitle("Cos(Phi)");
	  GAsm->Draw("A*");

	  fout->cd();
	  Hname=(Hf+hcnt)+Asm->GetName();
	  Asm->Write(Hname);
	  TString GAname="GA";
	  GAname=GAname+Hname;
	  GAsm->Write(GAname);

	  //	  fout->ls();
	  c1->Update();
	  TList* llis=Asm->GetListOfFunctions();
	  cout<<llis->GetEntries()<<" functions found\n";
	  TPaveStats* stats=llis->FindObject("stats");
	  
	  if(stats)
	    {
	      cout<< " GetY1NDC()="<<stats->GetY1NDC()<<"\n";
	      cout<< " GetY2NDC()="<<stats->GetY2NDC()<<"\n";
	      stats->SetY2NDC(.5);
	      stats->SetY1NDC(.15);
	      cout<< " after GetY1NDC()="<<stats->GetY1NDC()<<"\n";
	      cout<< " after GetY2NDC()="<<stats->GetY2NDC()<<"\n";
	    }
	  else
	    {
	      printf("stats not found\n");
	    };

	  TF1* fpol1=GAsm->GetFunction("pl1");
	  if(fpol1)
	    {
	      Float_t asm= fpol1->GetParameter(1)/pol;
	      Float_t errasm= fpol1->GetParError(1)/pol;
	      int gcnt=grpt[ecnt]->GetN();
	      grpt[ecnt]->SetPoint(gcnt,pt+ecnt*.02,asm);
	      
	      grpt[ecnt]->SetPointError(gcnt,0,errasm);
	      gcnt=gre[ptcnt]->GetN();
	      gre[ptcnt]->SetPoint(gcnt,En+ptcnt*.02,asm);
	      
	      gre[ptcnt]->SetPointError(gcnt,0,errasm);
	      
	      
	      cout<<"En="<<En<<" pt="<<pt<<" asm="<<asm<<"+-"<<errasm<<"\n";
	    }
	  else
	    {
	      printf("cant find fit function\n");
	    }
	  c1->Update();
	  c2->cd(ptcnt+1);
	  if(!M12plt1)
	    {
	      printf("M12plt1 not found\n");
	      M12plt1=MEmpty;
	    }
	  else
	    {
	      printf("entries=%d\n",M12plt1->GetEntries());
	      if(fset==3)
		{
		  DoMassFit(M12plt1);
		  fits[counter]=M12plt1->GetFunction("ffit");
		  //		  fits[counter]->SetParameter(3,0);
		}
	      printf("back from DoMassFit\n");
	    }
	  M12plt1->GetXaxis()->SetRangeUser(0,1.);
	  M12plt1->GetXaxis()->SetTitle("Mass (GeV)");
	  M12plt1->Draw();
	  //	  if{fits[counter])fits[counter]->Draw("same");
	  Hname=(Hf+hcnt)+M12plt1->GetName();
	  char Mname[100];
	  sprintf(Mname,"M12_E%d_PT%d",ecnt,ptcnt);
	  Hname=Hf+Mname;
	  M12plt1->Write(Hname);
	  if(fset==3)
	    {
	      sprintf(Mname,"M12_ffit_E%d_PT%d",ecnt,ptcnt);
	      Hname=Hf+Mname;
	      if(fits[counter]) fits[counter]->Write(Mname);
	    }
	  llo.Draw("same");
	  lhi.Draw("same");
	  c2->Update();
	  counter++;
	} 
      c1->Print("ANplots.ps");
      c2->Print("ANplots.ps");
      grpt[ecnt]->SetMarkerColor(colors[ecnt]);
      grpt[ecnt]->GetXaxis()->SetTitle("pt GeV/c");
      grpt[ecnt]->GetYaxis()->SetTitle("An (@Pol=.6)");
      grpt[ecnt]->GetYaxis()->SetRangeUser(range[0],range[1]);
      grpt[ecnt]->GetYaxis()->SetTitleOffset(1.25);
      sprintf(stit,"Energy=%f GeV",En);
      grpt[ecnt]->SetTitle(stit);
      c1->Clear();
      c2->Clear();
      c1->cd();
      
      grpt[ecnt]->Draw("A*");
      sprintf(stit,"En%d",ecnt);
      
      Hname=Hf+stit;
      cout<<Hname <<"\n";
      grpt[ecnt]->Write(Hname);
      
      lin[ecnt]=TLine(1.25,0.,7.25,0.);
      lin[ecnt].Draw();
      
      
      c1->Print("ANplots.ps");
      c1->Update();
      c1->Clear();
    }
  c1->Clear();
  grpt[4]->Draw("A*");
  lin[0]->Draw();
  grpt[3]->Draw("*");
  grpt[2]->Draw("*");
  grpt[1]->Draw("*");
  grpt[0]->Draw("*");
  c1->Print("ANplots.ps");
  c1->Clear();
  c1->Divide(2,3);
  for(int j=0;j<6;j++)
    {
      int ptcnt=j;
      Float_t pt=Ptbins[j];
      gre[ptcnt]->SetMarkerColor(colors[ptcnt]);
      gre[ptcnt]->GetXaxis()->SetTitle("Energy GeV");
      gre[ptcnt]->GetYaxis()->SetTitle("An (@Pol=.6)");
      gre[ptcnt]->GetYaxis()->SetRangeUser(range[0],range[1]);
      gre[ptcnt]->GetYaxis()->SetTitleOffset(1.25);
      sprintf(stit,"Pt=%f GeV/c",pt);
      gre[ptcnt]->SetTitle(stit);
      sprintf(stit,"Pt%d",ptcnt);
      Hname=Hf+stit;
      cout<<Hname <<"\n";
      gre[ptcnt]->Write(Hname);
      c1->cd(j+1);
      gre[j]->Draw("A*");
      lin[j]=TLine(25.,0.,75.,0.);
      lin[j].Draw();
      c1->Update();
    };
  
  c1->Print("ANplots.ps");
  c1->Clear();
  Zeros.Fit("pol0");
  Zeros.Draw("A*");
  Zeros.Write("Zeros");
  c1->Print("ANplots.ps)");
  system("ps2pdf ANplots.ps");
}
