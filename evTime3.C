{
  gStyle->SetOptStat(0);
  TFile* file=new TFile("adcTr.root");
  TFile* ofile=new TFile("oev3.root","recreate");
  TTree* tradc=file->Get("Tr_adc");
  TCanvas* c2=new TCanvas("c2","c2");
  TCanvas* c1=new TCanvas("c1","c1");
  tradc->Draw("evtnum>>zn(1000,0,1000)","led==0");

  c1->Print("evtdsp3.ps(");
  char nam[100];
  char cutbase[100];
  char vara[300];
  char varb[300];
  printf("ok1\n");
  c1->cd();

  sprintf(nam,"LargeCellTDCEv");
  //  sprintf(cutbase,"led==0&&nstbADC<3&&ClInd<2&&ADC>10&&TDC>0");
  sprintf(cutbase,"led==0&&nstbADC<3&&ClInd<2&&ADC>60&&TDC>0");
  sprintf(vara,"TDC:-(rowADC-16.5):((nstbADC==2)-(nstbADC==1))*(colADC+.5)>>%s",nam);

  sprintf(nam,"LargeCellTDCEv");
  TH3F* hist=new TH3F(nam,nam,34,-17,17,34,-17,17,16,0,16);
  sprintf(nam,"LargeCellTDCE4");
  sprintf(varb,"TDC:-(rowADC-16.5):((nstbADC==2)-(nstbADC==1))*(colADC+.5)>>%s",nam);
  printf("varb=%s\n",varb);

  TH3F* hist2=new TH3F(nam,nam,2,-17,17,2,-17,17,16,0,16);

  c1->Clear();
  tradc->Draw(vara,cutbase,"box");
  hist->Write();
  c1->Print("evtdsp3.ps");
  tradc->Draw(varb,cutbase,"box");
  hist2->Write();
  hist->Project3DProfile("yx")->Draw("zcol");
  c1->Print("evtdsp3.ps");
  c1->Clear();
  c1->Divide(2,2);
  c1->cd(3);
  gStyle->SetOptFit();
  hist2->ProjectionZ("pNB",1,1,1,1)->Fit("gaus");  
  c1->cd(1);
  hist2->ProjectionZ("pNT",1,1,2,2)->Fit("gaus");
  c1->cd(2);
  hist2->ProjectionZ("pST",2,2,2,2)->Fit("gaus");
  c1->cd(4);
  hist2->ProjectionZ("pSB",2,2,1,1)->Fit("gaus");

  pNT->SetTitle("pNT");
  pST->SetTitle("pST");
  pNB->SetTitle("pNB");
  pSB->SetTitle("pSB");

  pNT->GetXaxis()->SetTitle("TDC");
  pST->GetXaxis()->SetTitle("TDC");
  pNB->GetXaxis()->SetTitle("TDC");
  pSB->GetXaxis()->SetTitle("TDC");

  c1->Print("evtdsp3.ps");
  c1->Clear();
  sprintf(nam,"SmallCellTDCEv");
  sprintf(cutbase,"led==0&&nstbADC>2&&ClInd<2&&ADC>10&&TDC>0");
  sprintf(vara,"TDC:-(rowADC-11.5):((nstbADC==4)-(nstbADC==3))*(colADC+.5)>>%s",nam);
  
  TH3F* hists=new TH3F(nam,nam,24,-12,12,24,-12,12,16,0,16);
  c1->Clear();
  tradc->Draw(vara,cutbase,"box");
  c1->Print("evtdsp3.ps");
  hists->Project3DProfile("yx")->Draw("zcol");
  hists->Write();
  c1->Print("evtdsp3.ps)");
  gSystem->Exec("ps2pdf evtdsp3.ps;rm evtdsp3.ps;cp evtdsp3.pdf ../../");
  ofile->Close();
}
