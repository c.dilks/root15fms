class SETFPD 
{
public:
  SETFPD(CalibStr*,TString="");
  bool quiet;
  CalibStr* fpdcorr;
  CalibStr* maxadc;
  void SetRange(int Iew,int nstb,int row0,int col0,float low, float high);
  void SetValue(int Iew,int nstb,int row0,int col0,float val);
  void PreSet();
  void checkCases();
  void CheckAll();
  bool sset[4][34][34];
  void aGTb(int Iewa,int nstba,int row0a,int col0a,int Iewb,int nstbb,int row0b,int col0b);// set a to  value above b
  void aLTb(int Iewa,int nstba,int row0a,int col0a,int Iewb,int nstbb,int row0b,int col0b);// set a to  value below b
  
};
SETFPD::SETFPD(CalibStr* Fpdcorr,TString arg)
{

  quiet=false;
  if(arg=="Q")quiet=true;
  if(quiet)printf("SETFPD set to quiet\n"); 
  TString skipSETFPD=gSystem->Getenv("skipSETFPD");
  if(!quiet)printf("skipSETFPD=%s \n",skipSETFPD.Data());
  if(skipSETFPD != "") return;
  if(!quiet)printf("skipSETFPD=%s \n",skipSETFPD.Data());
  gSystem->Load("${FMSSRC}/Fpdchan.so");	
  fpdcorr=Fpdcorr;
  //  maxadc=new CalibStr(90100100,"ADCmax.txt");
  //  PreSet();
  checkCases();
  checkAll();
  cout<<"SETFPD done a\n";
}
void SETFPD::PreSet()
{
  
  Int_t Nrows[4]={34,34,24,24};
  
  for(int ns=1;ns<5;ns++)
    {
      for(int row=0;row<Nrows[ns-1];row++)
	{
	  for(int col=0;col<Nrows[ns-1]/2;col++)
	    {
	      sset[ns][row][col]=false;
	    }
	}
    }
};
void SETFPD::checkAll()
{

  gROOT->LoadMacro("${FMSROOT}/CodeSmall/Legal.C");
  Int_t Nrows[4]={34,34,24,24};
  
  for(int ns=1;ns<5;ns++)
    {
      for(int row=0;row<Nrows[ns-1];row++)
	{
	  for(int col=0;col<Nrows[ns-1]/2;col++)
	    {
	      if(Legal(2,ns,row,col)&&(!sset[ns-1][row][col])&&MASKFMS(2,ns,row,col))
		{
		  bool changed=false;
		  bool test=false;
		  bool sometest=false;
		  Float_t z=fpdcorr->GetValue(2,ns,row,col);

		  // if(z==0) fpdcorr->SetValue(2,ns,row,col,1.);
		  //		  if(z==5) fpdcorr->SetValue(2,ns,row,col,0.);
		  if(z<.2) fpdcorr->SetValue(2,ns,row,col,.2);
		  if(z>4.2&&ns<3) fpdcorr->SetValue(2,ns,row,col,4.2);
		  if(z>4&&ns>2) fpdcorr->SetValue(2,ns,row,col,4);

		  if(z!=fpdcorr->GetValue(2,ns,row,col))
		    {
		      if(!quiet)printf("PreSet change  r0%d c0%d n=%d at limit %f\n",
			     row ,col,ns,fpdcorr->GetValue(2,ns,row,col));
		    }
		}
	      else
		{
		  //		  fpdcorr->SetValue(2,ns,row,col,.0);
		};
	    };
	};
    };
}

void SETFPD::checkCases()
{
      
      
      SetRange(2,1,6,15,.4,1.);
      
      SetRange(2,1,8,12,.8,1.2);
      SetRange(2,1,7,12,.65,1.5);
      SetRange(2,1,7,0,.5,1.5);  
      
      SetRange(2,1,4,11,.2,.6); 
      SetRange(2,1,4,8,.8,1.3); 
      SetRange(2,1,4,9,.3,1.); 
      SetRange(2,1,4,13,1.5,4.); 
      SetRange(2,1,4,14,.8,1.2); 
      SetRange(2,1,4,12,.4,1.);
      SetRange(2,1,5,14,.8,1.2); 
      SetRange(2,1,12,0,.2,1.);
      
      
      SetRange(2,1,26,6,.9,1.1);
      aLTb(2,1,26,16, 2,1,25,16);
      aLTb(2,1,16,9, 2,1,16,10);
      
      
      SetRange(2,1,28,6,.2,.3);
      SetRange(2,1,30,12,.1,.15);
      
      SetValue(2,1,27,14,1.);

      
      SetRange(2,1,29,12,.3,.3);
      SetRange(2,1,29,11,.3,.6);
      SetRange(2,1,30,11,.15,.25);
      
      
      
      SetRange(2,1,31,0,.8,1.2);
      SetRange(2,1,31,10,.8,1.2);

      SetRange(2,1,32,5,.8,1.2);
      SetRange(2,1,32,3,.1,1.2);
      SetRange(2,1,31,4,.8,1.2);
      SetRange(2,1,31,3,.8,1.2);
      SetRange(2,1,31,5,.5,1.8);
      SetRange(2,1,32,4,1.3,1.5);
      
      
      SetRange(2,2,2,0,.5,.8);  
      SetRange(2,2,2,9,.5,1.5);  
      SetRange(2,2,2,10,.5,1.5);  
      SetRange(2,2,2,11,1.5,2.);  
      SetRange(2,2,4,9,.5,1.1);  
      SetRange(2,2,5,2,.5,1.);  
      
      SetRange(2,2,7,0,.5,1.3);  
      SetRange(2,2,5,13,.5,1.);
      
      
      SetRange(2,2,1,10,.25,1.5);       
      SetRange(2,2,3,7,.8,1.5);
      SetRange(2,2,3,11,1.2,1.8);
      SetRange(2,2,3,12,.25,2.2);

      SetRange(2,2,4,11,1.2,2.2);
      SetRange(2,2,4,12,1.2,1.8);

      
      SetRange(2,2,5,14,.5,1.5 );
      SetRange(2,2,6,13,1.,1.5 );
      SetRange(2,2,6,14,.5,1.5 );
      SetRange(2,2,6,15,1.5,3. );

      SetRange(2,2,8,0,1.,1.1 );
      SetRange(2,2,8,6,.1,1.4);
      SetRange(2,2,8,7,4.,5.5);
      SetRange(2,2,8,13,.5,1.35);
            

      SetRange(2,2,9,15,.3,1.2);
      SetRange(2,2,12,15,.4,1.1)
;
      SetRange(2,2,19,5,.8,1.6);

      SetRange(2,2,25,15,.3,.8);
      SetRange(2,2,27,15,.3,.8);
      SetRange(2,2,25,0,.3,1.5);
      SetRange(2,2,25,2,.3,1.);
      SetRange(2,2,25,4,.1,1.5);
      SetRange(2,2,25,5,.3,1.1);
      SetRange(2,2,25,6,.3,1.1);

      SetRange(2,2,29,13,.7,1.15);
      SetRange(2,2,30,12,.8,1.2);
      
      SetRange(2,3,10,7,.01,.02);
      SetRange(2,3,12,6,.01,.02);
      SetRange(2,3,12,8,.01,.02);
      SetRange(2,3,16,8,.8,2.);
      SetRange(2,3,13,5,.8,1.6);  
      SetRange(2,3,21,6,.8,1.6); 
      SetRange(2,3,21,5,.8,1.6);  
      SetRange(2,3,19,10,.8,1.6);
      SetRange(2,3,19,11,.4,.6);
      SetRange(2,3,20,9,1.,1.4);
      SetRange(2,3,12,6,.8,1.6);
      SetRange(2,3,5,8,.8,1.6);
      SetRange(2,3,5,9,.8,1.6);
      SetRange(2,3,5,10,.8,1.6);
      SetRange(2,3,5,11,.8,1.6); 
      SetRange(2,3,4,11,.8,1.6);
      SetRange(2,3,4,0,.6,.9);
      
      SetRange(2,3,17,3,.8,1.6);
      SetRange(2,3,20,0,.8, 1.4);
      SetRange(2,3,13,7,.01,.02);
      
      
      
      SetRange(2,4,0,8,.8,1.2);
      SetRange(2,4,17,4,.5,.8);
      SetRange(2,4,0,9,.8,1.2);
      SetRange(2,4,23,11,.5,.8); 
      SetRange(2,4,23,7,.8,1.2);
      SetRange(2,4,6,11,.8,1.2);
      SetRange(2,4,11,10,.8,1.4);
      SetRange(2,4,6,0,.8,1.2);
      SetRange(2,4,7,11,.8,1.2);
      SetRange(2,4,3,9,.8,1.2);
      SetRange(2,4,2,9,.8,1.2);
      
      SetRange(2,4,0,3,.8.,1.2);
      SetRange(2,4,0,4,2.,3.);
      SetRange(2,4,1,4,1.,1.5);
      
      SetRange(2,4,17,1,.2,.95);
      aGTb(2,4,17,2,2,4,17,1);
      aGTb(2,4,18,1,2,4,17,1);
      SetRange(2,4,22,2,.01,.02);
      aGTb(2,4,19,10, 2,4,19,9);
      SetRange(2,4,21,9,1.1,1.8);

};
void SETFPD::SetRange(int Iew,int nstb,int row0,int col0,float low, float high)
{
  if(MASKFMS(Iew,nstb,row0,col0))
    {
      float old;
      old=fpdcorr->GetValue(Iew,nstb,row0,col0);
      
      if(old<low)old=low;
      if(old>high)old=high;
      fpdcorr->SetValue(Iew,nstb,row0,col0,old);
      sset[nstb-1][row0][col0]=true;
      if(!quiet&&(old<=low || old>=high))printf("SetRange r0=%d c0=%d n=%d at limit %f\n",row0,col0,nstb,old);
    }
}

void SETFPD::SetValue(int Iew,int nstb,int row0,int col0,float val)
{
  if(MASKFMS(Iew,nstb,row0,col0))
    {
      float old;
      old=fpdcorr->GetValue(Iew,nstb,row0,col0);
      fpdcorr->SetValue(Iew,nstb,row0,col0,val);
      sset[nstb-1][row0][col0]=true;
      if(!quiet)printf("SetValue r0=%d=c0 %d n=%d change %f\n",row0,col0,nstb,val);
    };
};

void SETFPD::aGTb(int Iewa,int nstba,int row0a,int col0a,int Iewb,int nstbb,int row0b,int col0b)
{
 float  vala=fpdcorr->GetValue(Iewa,nstba,row0a,col0a);
 float valb=fpdcorr->GetValue(Iewb,nstbb,row0b,col0b);
 if(vala<=valb&&MASKFMS(Iewa,nstba,row0a,col0a))
    {
      fpdcorr->SetValue(Iewa,nstba,row0a,col0a,valb);
      if(!quiet)printf("aGtb r0=%d c0=%d n=%d change %f\n",row0a,col0a,nstba,valb);
    }
}

void SETFPD::aLTb(int Iewa,int nstba,int row0a,int col0a,int Iewb,int nstbb,int row0b,int col0b)
{
 float vala=fpdcorr->GetValue(Iewa,nstba,row0a,col0a);
 float valb=fpdcorr->GetValue(Iewb,nstbb,row0b,col0b);
 if(vala>=valb&&MASKFMS(Iewa,nstba,row0a,col0a))
   {
     
     fpdcorr->SetValue(Iewa,nstba,row0a,col0a,valb);
     if(!quiet)printf("aLtb r0%d c0%d n=%d change %f\n",row0a,col0a,nstba,valb);
   }
}
