#!/bin/bash

rm fill.txt

# obtain list of fill numbers
cat index.html | cut -f5 -d " " > fillList.dat

for line in `cat fillList.dat`
do

    # obtain list of run numbers for fill
    fillNum=$line
    fillNumPrint=`echo $fillNum | cut -c2-6`
    echo "obtaining run numbers for fill "$fillNumPrint
    cd $fillNum
    grep '"10%" scope="column">12' index.html | sed -e 's/<[^>]*>//' | cut -c2-10 >> ../runListDegenerate.dat
    cd ..
    echo "">runList.dat
    
    # remove duplicate run numbers
    for line in `cat runListDegenerate.dat`
    do
	runNumDegenerate=$line
	search=`grep $runNumDegenerate runList.dat`
	if [ "$search" == "" ]; then
	    echo $runNumDegenerate >> runList.dat
	fi
    done
    rm runListDegenerate.dat

    # tabulate fill numbers with run numbers
    for line in `cat runList.dat`
    do
	runNum=$line
	echo $runNum" "$fillNumPrint >> fill.txt
    done
    rm runList.dat

done
rm fillList.dat
cat fill.txt