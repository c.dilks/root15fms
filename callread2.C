void callread2(char* path="psu/7-61/OF*.root",Int_t setn=61,char* Opath=".")
{
  TString str=gSystem->ExpandPathName("$SETFMSENV");
  if(str!="SETFMSENV"){printf("source SetFMSEnv first");exit();};
  gROOT->Reset();
  TString FMSTXT =gSystem->ExpandPathName("${FMSTXT}");
  FMSTXT=FMSTXT+"/";
  TString FMSGAIN =gSystem->ExpandPathName("${FMSGAIN}");
  TString FMSCORR =gSystem->ExpandPathName("${FMSCORR}");
  TString QTMAP = gSystem->ExpandPathName("${QTMAP}");
  TString QTMAP2PP = gSystem->ExpandPathName("${QTMAP2PP}");
  gSystem->Load("${FMSSRC}/Fpdchan.so");
  TString FMSTXT=gSystem->ExpandPathName("${FMSTXT}");
  cout<<"FMSTXT=" <<FMSTXT <<"\n";
  
  FilesSet*  p_files=new FilesSet(  (char*) FMSTXT,
				    "fpdped.txt",
				    (char*) FMSGAIN, 
				    (char*) FMSCORR,
				    "fill.txt",
				    "Fake",
				    "spinpat",
				    "geom.txt",
				    (char*) QTMAP,
				    (char*) QTMAP2PP);

  p_files->Print();
  gStyle->SetPalette(1);
  cout<<"path="<<path<<"\n";
  cout<<"Opath="<<Opath<<"\n";
  TString tmplist=Opath;
  tmplist=tmplist+"/tmplist.txt";
  TString command="rm -f ";
  command=command+tmplist+";ls -1 ";
  command=command+path+">"+tmplist;
  cout<<"command="<<command<<"\n";
  gSystem->Exec(command);
  TString OutRoot=Opath;
  OutRoot=OutRoot+"/Output.root";
  cout<<"OutRoot="<<OutRoot<<"\n";
  cout<<"tmplist="<<tmplist<<"\n";
  
  AnalTools* at=new AnalTools();
  at->OutFileName=OutRoot;
  
  
  //   FitTower::Setwe_ErrFactors(.01,.03,1.,1.);
  
  
  
  // FitTower::Setwe_ab(.8, 0.2, 0,.25, .8, 10.); 
  
  
  
  
  /*
    float a1[3]={0.806868, 0.309599, -0.116467};
    float b1[3]={0.383446, 0.693601, 9.064924};
    float a1[3]={0.801664, 0.312982, -0.114646};
    float b1[3]={0.474185, 0.484726, 9.989510};
    float a1[3]={0.551175, 1.015920, -0.567095};
    float b1[3]={0.499115, 0.747827, 1.635490};
    float a1[3]={0.551175, 1.015920, -0.567095};
    float b1[3]={0.499115, 0.747827, 1.635490};
    float a1[3]={1.200000, 1.196061, -1.396061};
    float b1[3]={0.241612, 0.241101, 0.155891};
    float a1[3]={0.813939, 0.881728, -0.639433};
    float b1[3]={0.330695, 0.317772, 0.323351};
  */
  
  
  
  at->Energy_study=50.;
  
  /*
    float a1[3]={1.010096, 0.120803, -0.130899};
    float b1[3]={0.404687, 2.373520, 6.428797};
    float a1[3]={1.372750, 1.614862, -1.987612};
    float b1[3]={1.493502, 0.943420, 2.000617};
    float a1[3]={1.087439, 0.207367, -0.294806};// top frac .90
    float b1[3]={0.524966, 1.231896, 4.689016};
    
    
    
    float a1[3]={1.003244, 0.148641, -0.151885};// top frac .84
    float b1[3]={0.401399, 2.373722, 5.683016};
    float a1[3]={1.003244, 0.148641, -0.151885};// top frac .84
    float b1[3]={0.401399, 2.373722, 5.683016};
    
    float a1[3]={0.857603, 0.296195, -0.153798};// top frac .80
    float b1[3]={0.298671, 2.482535, 8.070142};
    float a1[3]={0.904023, 0.112427, -0.016449};// top frac .80
    float b1[3]={0.305030, 2.270512, 8.806297};
    float a1[3]={0.873565, 0.163525, -0.037090};// top frac .80
    float b1[3]={0.253702, 2.155430, 2.003001};
    float a1[3]={0.914948, 0.152195, -0.067142};//top frac .85
    float b1[3]={0.273086, 1.665521, 8.385673};
    float a1[3]={0.632935, 0.537843, -0.170778};//top frac .89
    float b1[3]={0.234150, 0.874560, 8.685755};
    float a1[3]={1.046284, 0.497169, -0.543454};//top frac 89
    float b1[3]={0.652588, 1.237093, 3.052981};
    float a1[3]={1.105107, 0.251524, -0.356632};//top frac 89
    float b1[3]={0.667807, 0.869657, 4.436999};
    float a1[3]={1.039921, 0.101893, -0.141814};//top frac 83
    float b1[3]={0.490614, 1.122733, 3.145812};
    float a1[3]={1.021422, 0.115445, -0.136868};//top frac 83
    float b1[3]={0.457124, 1.044964, 2.290717};
    
  */
  
  
  
  //float a1[3]={1.070804, 0.167773, -0.238578};//top frac 85
  //float b1[3]={0.535845, 0.850233, 2.382637};
  //float a1[3]={0.612889, 1.785089, -1.397978};
  //float b1[3]={1.499384, 2.464962, 8.842214};
  //float a1[3]={0.608814, 1.758238, -1.367052};
  //float b1[3]={1.485492, 2.483684, 9.998245};
  
  //float a1[3]={0.506516, 1.735097, -1.241613}; //large
  //float b1[3]={1.262838, 2.499761, 9.984770};
  
  //float a1[3]={1.077053, 0.173863, -0.250917};
  //float b1[3]={0.536860, 1.698887, 2.686510};
  //float a1[3]={1.186060, 0.563336, -0.749396};
  //float b1[3]={0.768417, 2.452418, 3.058504};
  //float a1[3]={1.006325, 0.485765, -0.492090};
  //float b1[3]={1.150393, 1.394233, 5.781444};
  //float a1[3]={1.330355, 0.505494, -0.835849};
  //float b1[3]={1.368814, 1.231919, 3.330714};
  //float a1[3]={1.132819, 0.300968, -0.433787};
  //float b1[3]={0.830099, 1.569559, 4.688696};
  //float a1[3]={1.058843, 0.140742, -0.199585};
  //float b1[3]={0.566347, 2.383962, 2.460393};
  //float a1[3]={.8,.3,-.1};
  //float b1[3]={.8,.2,7.6};
  // float a1[3]={1.070804, 0.167773, -0.238578};//top frac 85
  // float b1[3]={0.535845, 0.850233, 2.382637};
  //float a1[3]={1.034987, 0.188712, -0.223699};
  //float b1[3]={0.444212, 1.364135, 2.003337};
  float a1[3]={1.223680, 0.371153, -0.594834};
  float b1[3]={0.845571, 0.923658, 2.002513};
  
  FitTower::Setwe_ab(a1[0],a1[1],a1[2],b1[0],b1[1],b1[2]);
  FitTower::Setwe_ErrFactors(.01,.03,1.,1.,at->Energy_study);
  FitTower::SetForceMass(-1.);
  printf("about to call readq2\n");
  at->NumberEventsToProcess=1000000;
  at->NumberEventsToProcess=35000000;
  at->readq2((const char*) tmplist,setn,p_files);
}
