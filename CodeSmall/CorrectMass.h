ifndef CorrectMass_
define CorrectMass_
class CorrectMass
{
public:
  CorrectMass(float oldmass, float e,float z,float hm0=.145,float hm1=-88.27,float hm2=-10890.);
  float hiEMass[3];
  float FixedMass();
  float FixedforM(float m=0.135);
  float OldMass;
  float e1;
  float e2;
  float ang;
  float gcorang;
};
endif
