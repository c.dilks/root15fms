#include "elem.h"

elem::elem(Float_t x1,Float_t x2,Float_t y1,Float_t y2,Int_t nstb,Int_t ro0,Int_t co0,CalibStr* gain,CalibStr* gaincor,TObjArray* SheetArray)
{
	Nstb=nstb;
	row0=ro0;
	col0=co0;
	xlo=x1;
	xhi=x2;
	ylo=y1;
	yhi=y2;
	pxlo=pxhi=pylo=pyhi=0;
	Gain=gain;
	GainCor=gaincor;
	sheetArray=SheetArray;
	str=TString("<area shape =\"rect\" coords =\"0,0,10,10\" onmouseover=\"WriteText(\'This is what we write.\')\")/>");
}
void elem::UpdateStr(Qt* QT)
{
  Int_t id=1+col0+row0*QT->COL_NUM[Nstb-1];
  Int_t ppan=QT->pr[id-1][Nstb-1];
  Int_t prow=QT->rr[id-1][Nstb-1];
  Int_t pcol=QT->cr[id-1][Nstb-1];
  Int_t ns=0;
  char fname[200];
  char f2name[200];
  char f3name[200];
  char f4name[200];
  Float_t gn=Gain->GetValue(2,Nstb,row0,col0);
  Float_t gc=GainCor->GetValue(2,Nstb,row0,col0);
  char text[200];
  sprintf(text,"Shr%d_c%d_%d",row0,col0,Nstb-1);
  Sheet* sheet=sheetArray->FindObject(text);
  Int_t shift =1;
  Int_t HVset=0;
  Int_t cntt=row0+col0*100+Nstb*10000;
  if(sheet)
    {
      shift=sheet->GetHistory(0)->OldShift;
      
      HVset=sheet->GetHistory(0)->OldHV;
    }
  sprintf(fname,"gifs/ar%dc%dd%d.gif",row0,col0,Nstb-1);
  sprintf(f2name,"gifs/adcr%dc%dd%d.gif",row0,col0,Nstb-1);
  sprintf(f3name,"gifs/hh1r%dc%dd%d.gif",row0,col0,Nstb-1);
  sprintf(f4name,"gifs/hh2r%dc%dd%d.gif",row0,col0,Nstb-1);
  if(Nstb==2 || Nstb==4)ns=1;
  Int_t crate=QT->rcrate[pcol-1][prow-1][ppan-1][ns];
  Int_t slot=QT->rslot[pcol-1][prow-1][ppan-1][ns];
  Int_t card=QT->rdcd[pcol-1][prow-1][ppan-1][ns];
  Int_t chan=QT->rdch[pcol-1][prow-1][ppan-1][ns];
  cout<<"enter Update\n";
  char sa[300];
  sprintf(sa,"sh%d",cntt);
  char ss[400];
  sprintf(ss,"%d,%d,%d,%d",pxlo,pylo,pxhi,pyhi);
  str="<area id=\"";
  str=str+sa;
  str=str+"\" shape =\"rect\" coords =\" ";
  str=str+ss;
  str=str+TString("\" onclick=\"WriteTextCrSl3(\'");
  str=str+sa+"\',\'";
  sprintf(ss,"pp%d_%d",card+1,chan+1);
  str=str+ss;
  sprintf(ss,"\',%d,%d,\'",crate,slot);
  str=str+ss;
  sprintf(ss,"%d%d_r%d_c%d p%d(r%dc%d)",card,chan,row0,col0,ppan,prow,pcol);
  str=str+ss;
  str=str+"\',\'";
  str=str+fname;
  str=str+"\',\'";
  str=str+f2name;
  str=str+TString("\')\" onmouseover=\"WriteTextPlot1(\'desc\',\'");
  sprintf(ss,"Nstb=%d Row0=%d Col0=%d ID=%d <br> Panel=%d PRow=%d PCol=%d <br> QT: crate=%d slot=%d card=%d chan=%d (Gain=%f)x( Gaincor=%f)=%f GeV/cnt <br> HV=%d BitShift=%d ",Nstb,row0,col0,id,ppan,prow,pcol,crate,slot,card,chan,gn,gc,gn*gc,HVset,shift);
  str=str+ss;

  str=str+"\',\'";
  str=str+fname;
  str=str+"\',\'";
  str=str+f3name;
  str=str+"\',\'";
  str=str+f4name;
  str=str+"\')\"/>\n";
  cout<<str<<"\n";
  
};
