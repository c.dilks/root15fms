#ifndef elem_
#define elem_
#include "elem.h"

#include "TObject.h"
#include "TString.h"

class elem  :public TObject
{
public:
  elem(Float_t x1,Float_t x2,Float_t y1,Float_t y2,Int_t nstb,Int_t ro0,Int_t co0,CalibStr* gain,CalibStr* Gaincor,TObjArray* SheetArray);
Int_t Nstb;
Int_t row0;
Int_t col0;
Float_t xlo;
Float_t xhi;
Float_t ylo;
Float_t yhi;
Int_t pxlo;
Int_t pxhi;
Int_t pylo;
Int_t pyhi;
 TObjArray* sheetArray;
 CalibStr* Gain;
 CalibStr* GainCor;
TString str;
~elem(){};
void Print(){
cout<<"xlo="<<xlo<<" xhi="<<xhi<<" pxlo="<<pxlo<<" pxhi="<<pxhi;
cout<<" ylo="<<ylo<<" yhi="<<yhi<<" pylo="<<pylo<<" pyhi="<<pyhi<<"\n";
}
void UpdateStr(Qt* QT);
};

#endif
