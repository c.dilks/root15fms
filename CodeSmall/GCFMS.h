#ifndef GCFMS_
#define GCFMS_
#include <TFile.h>
#include <TString.h>
#include <TGraph.h>
#include "../FpdRoot/Cell.h"
#include "../FpdRoot/CalibStr.h"


class GCFMS : public TObject
{
public:
  GCFMS(Cell* c=0);
  void GraphVersions(int nstblo,int nstbhi);
  void AppendChange(int EW, int nstb, int row0, int col0,char* comment="");
  Float_t Cor;
  int ver;
  Int_t NROWS[4];
  TFile* Fpd;
  CalibStr* FpdCorr;
  CalibStr* FpdGain;
  TString GainSource;
  TString CorrSource;
  TGraph* fpgr[34][17][4];
  TGraph* cvar[34][17][4];
  private:
  ClassDef(GCFMS,1);
};

#endif
