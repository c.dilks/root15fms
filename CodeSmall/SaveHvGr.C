{
  gROOT->Macro("start.C");
  FILE* fp;
  fp=fopen("SheetFiles/newomg3.csv","write");
  TFile* sheetset=new TFile("SheetSet.root");
  TObjArray* Sheets=sheetset->Get("MySheet");
  TIter next(Sheets);
  Sheet* sh=0;
  Int_t cnt=0;

  TFile* HvGr=new TFile("SheetFiles/HvGr.root","recreate");
  while(sh=(Sheet*) next())
    {
      sh->Print();
      if(sh->gr)
	{
	  sh->gr->Write();
	  TString grname=sh->gr->GetName();
	  
	  TF1* fun=(TF1*) sh->gr->GetFunction("fun");
	  if(fun)
	    {
	      TString fstr=grname.Replace(0,2,"");
	      fstr="fun"+fstr;
	      fun->SetName(fstr);
	      fun->Write();
	    };
	};
      if(cnt==0)fprintf(fp,"%s\n",(const char*) sh->MakecsvHead());
      fprintf(fp,"%s",(const char*) sh->MakecsvLine());
      cout<<cnt++<<"\n";

    };
  fclose(fp);
}

