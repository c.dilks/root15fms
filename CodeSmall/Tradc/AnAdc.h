#ifndef AnAdc_
#define AnAdc_


#include <stdio.h>
#include <TFile.h>
#include <TH2F.h>
#include <TH1F.h>
#include <TH2D.h>
#include <TH1D.h>
#include <TCanvas.h>
#include <TGraphErrors.h>
#include <TF1.h>
#include <TString.h>
#include <TRandom.h>

#include "../GCFMS.h"
//class AnAdc : public TObject
class AnAdc 
{
 public:
  AnAdc(TString adcpath="adcTr.root",TString tmppath="adctmp.root",Float_t Erun=200);
  AnAdc(Int_t jump){};
  int CLIndSoft;
  TFile* f;
  TFile* ftmp;
  TTree* Tr_adc;
  TF1* f1time;
  TH1F* HQuadFactor[8];
  void Set1Fun();
  Float_t EnergyOfRun;
  Long64_t maxnentries;
  void Begin();
  void ExpPowSetup(TString smallarge="small");
  TF1* ExpPow;
  TF1* Pwr;
  TString FMSROOT;
  CalibStr* Gain;
  CalibStr* Gcor;
  GCFMS* gFMS;
  Int_t ReGainCorOpt;
  TCanvas* c2;
  TCanvas* c1;
  TCanvas* c3;
  //histograms
  void  Makehab(); //histogram for Hot.gif
  void docross(); //call before update gains
  bool didcross;// true if docross() has been called
  TH2F* ha;
  TH2F* hb;
  TH2F* hc;
  TH2F* hd;
  
  //Declaration of leaves types
  void SetupTradc();
  Int_t           ADC;
  Int_t           rowADC;
  Int_t           colADC;
  Int_t           nstbADC;
  Int_t           runnum;
  Int_t           evtnum;
  Int_t           segnum;
  Int_t           led;
  Int_t           Bunchid7bit;
  Float_t         CellE;
  Int_t           ClusterCount;
  Float_t         ClusterE;
  Int_t           ClusterOrder;
  Float_t         minang;
  Float_t         minE;
  Float_t         minM;
  Float_t         HiCluE;
  Float_t         NextHiCluE;
  Float_t         dToHiTower;
  Float_t         PseudoR;
  Float_t         EDet;
  Float_t         ADCDet;
  Int_t           TrigBits;
  Int_t           L2sum[2];
  Int_t           ClInd;
  Float_t           AngAway;
  
  //Histograms and other arrays
  bool histsetupcalled;
  void histsetup();
  bool firstfillcalled;
  void firstfill();
  void MoreHists(int ishift,Int_t row0,Int_t col0,Int_t nstb0);
  void MoreFill();
  void PostFill();
  void Finish();
  Int_t Opt2(Int_t row0,Int_t col0,Int_t nstb0);
  Float_t LLhis2prime(Float_t val,Int_t row0,Int_t col0,Int_t nstb0);
  Int_t FinishStatus[34][24][4];
  Int_t  shiftfromADC(TH1F* h_adc,int nstb0,int row0,int col0);
  Int_t rows[4];
  
  /*
    "r%dc%dd%d"  his[][][] nontrig cell cluster energy distribution with 
    variable # of bins (from ishift)
    "adcr%dc%dd%d", created his3[][][]  raw adc dist
    "aar%dc%dd%d", created his1[][][] hi tower shifted adc->trig clust energy dist 
    "ar%dc%dd%d", created his2[][][]  shifted adc->non-trig cell energy dist
    "hh1r%dc%dd%d", creted hh1[][][] high cell energy fraction
    "hh2r%dc%dd%d", creted hh2[][][] next cell energy fraction for lrtb 
    "hledr%dc%dd%d", creted hled[][][] led time sequence
  */
  
  TH1F* his[34][24][4];
  TH1F* his1[34][24][4];
  TH1F* his2[34][24][4];
  TH1F* his3[34][24][4];
  TH1F* hadc[34][24][4];
  TH2F* hh1[34][24][4];
  TH2F* hh2[34][24][4];
  TH2F* hled[34][24][4];
  TH2F* Sig3[4];
  
  Float_t rapidity[34][24][4];
  Int_t Shifted[34][24][4];
  CalibStr* HiAdc;
  Float_t frac1[34][24][4];// fraction of energy in secondary towers
  Float_t fof3g[34][24][4];//f(3GeV) using fit to his2
  Int_t Nov200[34][24][4];// Number events in his1 with E above E_200
  Float_t trg2sig[34][24][4];// E of upper extent of his1 distribution
  Float_t trgOverAve[34][24][4];// Average energy of events with E>E_200
  Float_t f3fac[34][24][4];
  Float_t eslope[34][24][4];// exp slope fron his2 fit
  Float_t highestBinVal(TH1F* h,Int_t zerolevel=1);
  
  TH2F* HCross3s;
  TH2F* HCross3l;
  TGraphErrors* Cross3l;
  TGraphErrors* Cross3s;
  TF1* f3s; //small cell rapidity fit
  TF1* f3l;// large cell rapidity fit 
  TH1D* Crs;
  TH1D* Crl;
  TRandom* pRand;
  Float_t EneEval;// energy at which csec will be calculated
  Float_t MinGainChange;
  // private:
  //  ClassDef(AnAdc,1);

};

#endif

