{

TString str=gSystem->ExpandPathName("$SETFMSENV");
  if(str!="SETFMSENV"){printf("source SetFMSEnv first");exit();};
  gROOT->Reset();
  gStyle->SetPalette(1);
  TString FMSTXT =gSystem->ExpandPathName("${FMSTXT}");
  FMSTXT=FMSTXT+"/";
  TString FMSGAIN =gSystem->ExpandPathName("${FMSGAIN}");
  TString FMSCORR =gSystem->ExpandPathName("${FMSCORR}");
  TString QTMAP = gSystem->ExpandPathName("${QTMAP}");
  TString QTMAP2PP = gSystem->ExpandPathName("${QTMAP2PP}");
  gSystem->Load("${FMSSRC}/Fpdchan.so");
  TString FMSTXT=gSystem->ExpandPathName("${FMSTXT}");
  cout<<"FMSTXT=" <<FMSTXT <<"\n";
  
  FilesSet*  p_files=new FilesSet(  (char*) FMSTXT,
				    "fpdped.txt",
				    (char*) FMSGAIN, 
				    (char*) FMSCORR,
				    "fill.txt",
				    "Fake",
				    "spinpat",
				    "geom.txt",
				    (char*) QTMAP,
				    (char*) QTMAP2PP);

  TString FMSroot =gSystem->ExpandPathName("${FMSROOT}");
  gROOT->LoadMacro(FMSroot+"/CodeSmall/SETFPD.C");
  gROOT->LoadMacro(FMSroot+"/CodeSmall/GCFMS.C");
  gROOT->LoadMacro(FMSroot+"/CodeSmall/Tradc/AnAdc.C");
  gROOT->LoadMacro(FMSroot+"/CodeSmall/Legal.C");

}; 
