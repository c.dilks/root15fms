{
  gROOT->Reset();
  gSystem->Load("${FMSSRC}/Fpdchan.so");
  gROOT->LoadMacro("Legal.C");
  TString set="small";
  Int_t nstb0=1;
  if(set == TString("small"))nstb0=3;
  TString str=gSystem->ExpandPathName("$SETFMSENV");
  if(str!="SETFMSENV"){printf("source SetFMSEnv first");exit();};
  printf("nstb0=%d\n",nstb0);
  TFile* Fpd=new TFile("FpdFile.root");
  TCanvas* c1=new TCanvas("c1","c1",600,600);
  Bool_t ReplaceFpd=false;
  if(Fpd)ReplaceFpd=true;
  int ver=1;  
  CalibStr* FpdCorr=0;
  CalibStr* FpdCnew=0;
  TString sver="1";
  
  while(FpdCnew=(CalibStr*) Fpd->Get(TString("FpdCorrV")+sver))
    {		
      FpdCorr=FpdCnew;
      FpdCorr->filename="FpdCorr.txt";
      FpdCorr->OutName="FpdCorr.txt";
      ver++;
      sver=ver;
    };
  FitTower::Setwe_ab(0.8,.3,-.1,.8,.2,7.6);//Nominal Shower Shape

  Cell* cell[34][17][4];
  //  printf("nstb0=%d\n",nstb0);
  exit;

    for(int nstb=nstb0;nstb<nstb0+2;nstb++)
    {
      for(int row=1;row<35;row++)
	{for(int col=1;col<18;col++)
	    {
	      cell[row-1][col-1][nstb-1]=0;
	    }
	};
    };
  
  TFile* fl=new TFile("Output.root","Update");
  TObjArray* mycells=(TObjArray*)  fl->FindKey("Mycells")->ReadObj();
  TIter next(mycells);
  Cell* c;
  TTree* tr=0;
  tpCellRec* rc=0;
  
  TH1F* edist=new TH1F("edist","edist",100,.0,100.);
  while(c=(Cell*) next())
    {
      edist->Reset();
      printf("row=%d col=%d nstb=%d\n",c->Row1,c->Col1,c->Instb);
      cell[c->Row1-1][c->Col1-1][c->Instb-1]=c;
      printf("CellD entries=%d\n",c->CellD->GetEntries());
    }
  printf("stage 2\n");
  int pass=1;
  Int_t Nrows[4]={34,34,24,24};
  for(int nstb=nstb0;nstb<nstb0+2;nstb++)
    {
      for(Int_t ro=1;ro<Nrows[nstb0-1]+1;ro++)
	{
	  printf("ro=%d\n",ro);
	  for(int col=1;col<Nrows[nstb0-1]/2+1;col++)
	    {
	      printf("ro= %d col=%d nstb %d \n",ro,col,nstb);
	      c=cell[ro-1][col-1][nstb-1];
	      if(!c)
		{
		  printf("c not defined\n");
		};
	      
	      if(!c)continue;
	      printf("c->Col1=%d \n",c->Col1);
	      if(Legal(2,c->Instb,c->Row1-1,c->Col1-1))
		{
		  printf("is legal\n");
		  c->InitBranches();
		  
		  if(ReplaceFpd)c->fpdcorr=FpdCorr;
		  TString fname("data2/");
		  fname=fname+"_"+c->GetName()+".root";
		  printf(" %s\n",(const char*)fname);
		  TFile flout(fname,"recreate");
		  if(tr)delete tr;
		  tr=new TTree(*c->CellD->CloneTree());
		  if(c->Instb<nstb0 ||c->Instb>nstb0+1)continue;
		  int nev=tr->GetEntries();
		  printf("CellD entries before=%d\n",c->CellD->GetEntries());
		  c->CellD=tr;
		  printf("CellD entries=%d\n",c->CellD->GetEntries());
		  for(int iev=0;iev<nev;iev++)
		    {
		      tr->GetEntry(iev);
		      //		      Float_t efl=c->FillFMSEnergy(c->Instb)->Sum();
		      //		      edist->Fill(efl);
		    };
		  //	      edist->Draw();
		  if(c->Rdep)delete c->Rdep;
		  c->Write();
		  flout.Close();
		};
	    };
	};
      printf("Extract %s done\n",(char*) set);
    };

};
