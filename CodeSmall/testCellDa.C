{
  TCanvas* c1=new TCanvas("c1","c1");
  gROOT->Macro("start.C");  
  TString name="Cellr3_c4_1";
  TString filename="data2/_";
  filename=filename+name+".root";
  TFile* file=new TFile("Output.root","read");
  TObjArray* my=file->Get("Mycells");
  Cell* c=(Cell*) my->FindObject(name);

  c->CellD->Print();
  c->InitBranches();
  c->CellD->Draw("nSavedHits");
  for(int j=2;j<10;j++)
    {
      c->CellD->GetEntry(j);
      c->FillFMSADC(2)->Draw("box");
      c1->Update();
      int ns= c->celldat->nSavedHits;
      cout<<"ns="<<ns<<"\n";

      for(int n=0;n<ns;n++)
	{
	  cout<< c->celldat->SavedHits[n]<<",  ";
	};

      cout<<"\n";

      system("sleep 2");
    };
}
