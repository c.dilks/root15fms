void RunOne(char* name,  int Begin=0,
	    int set=0,int MaxRec=20000,Float_t gaincut=0.)
{
  TString str=gSystem->ExpandPathName("$SETFMSENV");
  if(str!="SETFMSENV"){printf("source SetFMSEnv first");exit();};

  
  gROOT->Reset();
  gSystem->Load("${FMSSRC}/Fpdchan.so");
  bool NoChange=false;
  gROOT->LoadMacro("Legal.C");
  gROOT->LoadMacro("LocalAve.C");
  gROOT->LoadMacro("./SetFPD.C");
  gROOT->LoadMacro("LogWt.C");
  gROOT->LoadMacro("gcorbyTrig.C");
  gStyle->SetPalette(1);
  //  FitTower::Setwe_ab(0.8,.3,-.1,.8,.2,7.6);//Nominal Global Shower Shape
  FitTower::Setwe_ErrFactors(.015,.26,1.95,2.71);
  //float a1[3]={0.396404, 1.200000, -0.596403};
  //float b1[3]={0.091390, 1.076924, 2.572030};
  
  //  float a1[3]={0.873565, 0.163525, -0.037090};// top frac .80
  //float b1[3]={0.253702, 2.155430, 2.003001};
  //  float a1[3]={1.070804, 0.167773, -0.238578};//top frac 85
  //float b1[3]={0.535845, 0.850233, 2.382637};

  float a2[3]={1.223680, 0.371153, -0.594834};//large
  float b2[3]={0.845571, 0.923658, 2.002513};
  float a1[3]={0.8, 0.3, -0.1};// top frac .80
  float b1[3]={0.8, .2, 7.6};
  AnalTools* at=new AnalTools();
  
   TString fna("data2/_");
  fna=fna+name+".root";
  // *************
  Int_t Nv,Iro,Ico,Ide;
  Nv=  sscanf(name,"Cellr%d_c%d_%d",&Iro,&Ico,&Ide);
  cout<<fna<<" ?? \n";
  if(Nv != 3)return;
  if(!Legal(2,Ide+1,Iro,Ico))return;
  printf("Is Legal\n");
  TFile fl(fna,"UPDATE");
  
  TList* tl= fl.GetListOfKeys();
  TIter next(tl);
  TKey* to;
  TGraph* p_Gr0=0;
  TTree* gTr=0;
  p_Gr0=(TGraph*) fl.Get("Gr0");
  if(p_Gr0)
    {
      if(p_Gr0->GetN()==0)p_Gr0=0;
    };
  if(!p_Gr0)
    {
      p_Gr0=new TGraph();
    }
  else if ( fabs(p_Gr0->GetY()[p_Gr0->GetN()-1]-1.)<gaincut)
    {
      printf("Exit because gain already near one = %f \n",
	     p_Gr0->GetY()[p_Gr0->GetN()-1]);
      exit();
    }

  gTr=(TTree*) fl.Get("gTr");

  if(gTr)
    {
      if(gTr->GetEntries()<1)gTr=0;
    };
  gTr=0;
  Float_t gEn70;
  Float_t gDEn70;
  Float_t gMass;
  Float_t gDMass;
  Float_t gEta;
  Float_t gFn70;
  Float_t gRatio;
  Float_t gDRatio;
  Int_t gSet=set;
  Float_t gFM;
  Float_t gFS;
  Float_t gDFM;
  Float_t gDFS;
  Int_t Quality;
  Float_t Cor;
  Int_t nstb;
  Int_t row;
  Int_t col;
  Float_t Int65;
  if(!gTr)
    {
      fl.cd();
      gTr=new TTree("gTr","gTr");
      gTr->Branch("gEn70",&gEn70,"gEn70/F");
      gTr->Branch("gDEn70",&gDEn70,"gDEn70/F");
      gTr->Branch("gMass",&gMass,"gMass/F");
      gTr->Branch("gDMass",&gDMass,"gDMass/F");
      gTr->Branch("gEta",&gEta,"gEta/F");
      gTr->Branch("gFn70",&gFn70,"gFn70/F");
      gTr->Branch("gRatio",&gRatio,"gRatio/F");
      gTr->Branch("gDRatio",&gDRatio,"gDRatio/F");
      gTr->Branch("gSet",&gSet,"gSet/I");
      gTr->Branch("gnstb",&nstb,"nstb/I");
      gTr->Branch("grow",&row,"row/I");
      gTr->Branch("gcol",&col,"col/I");
      gTr->Branch("gQ",&Quality,"Q/I");
      gTr->Branch("gCor",&Cor,"gCor/F");
      
      gTr->Branch("gFM",&gFM,"gFM/F");
      gTr->Branch("gFS",&gFS,"gFS/F");
      gTr->Branch("gDFM",&gDFM,"gDFM/F");
      gTr->Branch("gDFS",&gDFS,"gDFS/F");
      gTr->Branch("gInt65",&Int65,"Int65/F");
      
    }
  else
    {
      gTr->SetBranchAddress("gEn70",&gEn70);
      gTr->SetBranchAddress("gDEn70",&gDEn70);
      gTr->SetBranchAddress("gMass",&gMass);
      gTr->SetBranchAddress("gDMass",&gDMass);
      gTr->SetBranchAddress("gEta",&gEta);
      gTr->SetBranchAddress("gFn70",&gFn70);
      gTr->SetBranchAddress("gRatio",&gRatio);
      gTr->SetBranchAddress("gDRatio",&gDRatio);
      gTr->SetBranchAddress("gSet",&gSet);
      gTr->SetBranchAddress("gnstb",&nstb);
      gTr->SetBranchAddress("grow",&row);
      gTr->SetBranchAddress("gcol",&col);
      gTr->SetBranchAddress("gQ",&Quality);
      gTr->SetBranchAddress("gCor",&Cor);
      gTr->SetBranchAddress("gFM",&gFM);
      gTr->SetBranchAddress("gFS",&gFS);
      gTr->SetBranchAddress("gDFM",&gDFM);
      gTr->SetBranchAddress("gDFS",&gDFS);      
      gTr->SetBranchAddress("gInt65",&Int65);
    };
  
  

  printf(" %s \n",name);
  TKey* key=fl.FindKey(name);
  if(!key)return;
  printf("reading key\n");
  Cell* c=(Cell*) key->ReadObj();
  TTree* trn= fl.Get("trn");
  c->CellD=trn;
  TKey* keyRank=fl.FindKey("Rank");
  CalibStr* Rank=0;
  if(keyRank)Rank=(CalibStr*) keyRank->ReadObj();
  
  nstb=c->Instb;
  //use separate small and large shapes
  if(nstb>2)
    {
      FitTower::Setwe_ab(a1[0],a1[1],a1[2],b1[0],b1[1],b1[2]);
    }
  else
    {
      FitTower::Setwe_ab(a2[0],a2[1],a2[2],b2[0],b2[1],b2[2]);
    };
  row=c->Row1;
  col=c->Col1;
   
  TCanvas* c1=new TCanvas("c1","c1",600,600);

  // ***********Fpd
  TFile* Fpd=new TFile("FpdFile.root","Read");
  CalibStr*  FpdGain=Fpd->Get("FpdGain");
  if(FpdGain==0)
    {
      CalibStr* FpdGain=c->fpdgain;
      if(FpdGain==0)CalibStr* FpdGain=new CalibStr(90100100,"${FMSTXT}/${FMSGAIN}");
    }


  Float_t Cor;
  int ver=1;  
  CalibStr* FpdCorr=0;
  CalibStr* FpdCnew=0;
  char strver[100];
  sprintf(strver,"%d",ver);
  while(FpdCnew=(CalibStr*) Fpd->Get(TString("FpdCorrV")+strver))
    {		
      FpdCorr=FpdCnew;
      FpdCorr->filename="FpdCorr.txt";
      FpdCorr->OutName="FpdCorr.txt";
      ver++;
      Float_t g=FpdCorr->GetValue(2,nstb,row-1,col-1);
      sprintf(strver,"%d",ver);
    };  
  Fpd->Close();
  fl.cd();
  FILE* fp=fopen("ChangeFpd.txt","r");
  char line[200];
  if(fp)
    {
      while(!feof(fp))
	{
	  if(fgets(line,100,fp))
	    {
	      Int_t ew,nst,ro,co;
	      Float_t val;
	      sscanf(line,"SetValue(%d,%d,%d,%d,%f)",&ew,&nst,&ro,&co,&val);
	      FpdCorr->SetValue(ew,nst,ro,co,val);
	    };
	};
      fclose(fp);
    }
  else
    {
      cout<<"ChangeFpd.txt not found\n";
    };
  SetFPD(FpdCorr);
  c->fpdcorr=FpdCorr;
  c->gcorr=c->gcorrNew;
  if(!c)printf("c not found\n");
  Bool_t lowcount=false;
  if(c->p_adc->Integral(10,1000)<10)lowcount=true;
  if(lowcount)
    {
      printf("too few hits\n");
    };
  c->InitBranches();
  rc=new tpCellRec();

  printf("Pre ReRec gcorr=%f gcorrNew=%f  fpdcorr in c=%f\n",
	 c->gcorr,c->gcorrNew, c->fpdcorr->GetValue(2,nstb,row-1,col-1));
  Quality=Rank->GetValue(2,nstb,row-1,col-1);
  // TTreeFormula* fo=new TTreeFormula("fo","Epair>6.&&sqrt(pow(X2-X1,2)+pow(Y2-Y1,2))<5.",c->CellD);
  char cutform[200];
  Float_t emin=6.;
  // s1  Float_t emin=1.;
  //  if(c->gcorr>0)emin=emin/c->gcorr;
  sprintf(cutform,"Epair>%f ",emin);
  sprintf(cutform,"Epair>%f &&sqrt(pow(X2-X1,2)+pow(Y2-Y1,2))<2.4",emin);

  TTreeFormula* fo=new TTreeFormula("fo",cutform,c->CellD);
  //  Rank->tm(2,3)->Draw("text");
  c1->Update();
  c->SetEmin(.01);
  //  c->RdepBaseOverride=12080001;//set to non-zero non-defined Base Run
  //c->RdepBaseOverride=12095006;//set to non-zero non-defined Base Run
  //  TTreeFormula* fo=new TTreeFormula("fo","1==1r",c->CellD);
  c->RdepBaseOverride=13058029;//set to non-zero non-defined Base Run
  //  fl.Print();
  atr=c->ReRecon(rc,0,MaxRec,Rank,fo,false);//true sets fiducial cuts on ReRecon

  c1->Clear();
  c1->cd();
  atr->Print();
  c1->Update();
  TH2F* mvse=new TH2F("mvse","mvse",60,0,60.,30,.0,.6);
  char cut[300];
  Float_t* wid=c->p_Geom->FpdTowWid(c->Iew,c->Instb);
  Float_t xlocal=c->xLocal/wid[0];
  Float_t ylocal=c->yLocal/wid[1];
  //  sprintf(cut,"mr>.02 && mr<.3 && er>5 && er<100 &&e1>er-e1&&abs(x1-%f)<.5 &&abs(y1-%f)<.5",xlocal,ylocal);
  sprintf(cut,"mr>.02 && mr<.5 && er-e1>2  && e1>2 && er<100 &&abs(x1-%f)<.5 &&abs(y1-%f)<.5",xlocal,ylocal);
  cout<<cut<<"\n";
  atr->Project("mvse","mr:er",cut);
  Int_t Nmvse=mvse->GetEntries();
  Int_t mvseNbins=8;
  if(nstb<3)mvseNbins=10; //large cells
  TGraphErrors* gr=c->FitMvsE(mvse,mvseNbins);
  mvse->Write();
  
  gr->Write("gr");
  printf("gr->GetN()=%d\n",gr->GetN());
  c1->Divide(2,3);
  c1->cd(1);
  mvse->Draw("box");
  c1->Update();
  //  system("sleep 5");
  Float_t fllo;
  Float_t flhi;
  if(nstb<3)
    {
      TF1* fl0=new TF1("fl0","[0]+(x-25.)*[1]",8,65);
      fllo=18;
      flhi=36;
    }
  else
    {
      TF1* fl0=new TF1("fl0","[0]+(x-40.)*[1]",8,65);
      fllo=32;
      flhi=50;
    };
  fl0->SetParameter(1,.0004);
  fl0->SetParLimits(1,.00001,.001);
  fl0->SetParameter(0,.135);
  fl0->SetParLimits(0,.03,.3);
  //  gr->Fit("fl0","","",20,60);
  //  fl0->Print();
  Float_t FitM=0.;
  Float_t dFitM=0.;
  if(!lowcount)
    {
      printf("pass lowcount\n");
      if((int) (gr->Fit("fl0","","",fllo,flhi))==0)
	{
	  
	  //  fl0->FixParameter(1,.0004);
	  gFM=gr->GetFunction("fl0")->GetParameter(0);
	  gFS=gr->GetFunction("fl0")->GetParameter(1);
	  gDFM=gr->GetFunction("fl0")->GetParError(0);
	  gDFS=gr->GetFunction("fl0")->GetParError(1);
	  //      fl0->Print();
	  gr->Fit("fl0","","",fllo,flhi);
	  
	  Float_t FitM=fl0->GetParameter(0);
	  Float_t dFitM=fl0->GetParError(0);
	  gr->Write("gr");
	}
      else
	{
	  printf("Fit Mass Error\n");
	};
    };
  gr->Draw("*");
  c1->cd(2);
  TH1F* Ene=new TH1F("Ene","Energy",100,0,100.);
  atr->Project("Ene","er","mr>0 && mr<.25 && er<100 &&e1>er-e1");
  TF1 ffg("ffg","[3]*exp(-.2*(x-20))+[0]*exp(-pow((x-[1])/[2],2)/2.)");
  ffg.SetParameter(3,10);
  ffg.SetParameter(0,10);
  ffg.SetParameter(1,40.);
  ffg.SetParameter(2,10.);
  
  ffg.SetParLimits(1,30,80);
  ffg.SetParLimits(2,8,20.);
  Ene->Fit("ffg","L","",30,80);
  Float_t peak=Ene->GetFunction("ffg")->GetParameter(1);
  if(peak<30) peak=30.;
  if(peak>70) peak=70;
  TF1 fe("fe","[0]*exp([1] * (x-65))",peak+10,peak+30);
  fe.SetParameter(0,.1);
  fe.FixParameter(1,-.2);
  fe.SetParLimits(0,.005,1000.);
  Int65=Ene->Integral(65,100);
  Float_t I50_55=Ene->Integral(51,55);
  Float_t I56_60=Ene->Integral(56,60);
  Float_t I61_65=Ene->Integral(61,65);
  if(peak<50 && .75*I50_55<I56_60)peak=55;
  if(peak<55.1 && .75*I56_60<I61_65)peak=60;
  printf("I50_55=%f I56_60=%f I61_65=%f peak=%f \n",I50_55,I56_60,I61_65,peak);
  Ene->Fit("fe","L","",peak+5,peak+30);
  //  gROOT->GetFile()->Print();
  Ene->Write();
  Ene->Draw();
  Float_t en70=Ene->GetFunction("fe")->GetParameter(0);
  printf("en70=%f\n", en70);
  Float_t den70=Ene->GetFunction("fe")->GetParError(0);
  Float_t Eta=c->Eta;
  c1->GetPad(2)->SetLogy();
  c1->cd(3);
  atr->Draw("mr","mr>0 && mr<.75 && er>10 && er<100 &&e1>er-e1");
  c1->cd(4);
  c->p_adc->Draw();
  c1->GetPad(4)->SetLogy();
  c1->cd(5);
  atr->Draw("y2:x2","mr>0 && mr<.25 && er>10 && er<100 &&e1>er-e1","box");
  c1->cd(6);
  atr->Draw("y1:x1","mr>0 && mr<.25 && er>10 && er<100 &&e1>er-e1","box");
  printf("done.....");
  c1->SetTitle(c->GetName());
  fl.cd();
  Bool_t savegTr=true;
  
  if(FitM>0 )
    {
      gEn70=en70;
      gDEn70=den70;
      gMass=FitM;
      gDMass=dFitM;
      savegTr=true;
    }
  else 
    { printf("FitM or en70 bad!!! \n");};
  savegTr=true;
  gEta=c->Eta;
  gEn70=0;
  Int65=0;
  gFn70=0;
  if(c->p_adc)
    { 
      
      Int65=c->p_adc->Integral(160,4096);
      gFn70=LogWt(c->p_adc);
      gEn70=gcorbyTrig(c->p_adc);
      printf("Int65=%d gFn70=%f gEn70=%f \n",Int65,gFn70,gEn70);
    };
  c1->Update();
  system("sleep 3");
  c1->Write("plots");
  printf("wrote c1 to plots\n");
  Float_t mnew=0;
 
  if(FitM>0)
    {
      mnew=gr->GetFunction("fl0")->GetParameter(0);
    };
  Float_t factor=1.;
  printf("mnew=%f \n",mnew);
  if(!lowcount && mnew>.05 && mnew<.3)factor=pow(0.135/mnew,1.5);
  c->gcorr=c->fpdcorr->GetValue(2,nstb,row-1,col-1);
  printf("Resolve fdpcorr to %f \n",c->gcorr);

  c->gcorrNew=(c->gcorr)*factor;
  Float_t gcnew=c->gcorrNew;
  printf("gcnew=%f \n",gcnew);
  Float_t cellY=c->Eta;
  printf("cellY=%f\n",cellY);
  Float_t gf70=gcnew*gFn70;
  TF1 etaFn;
  if(nstb<3)
    {
      etaFn=TF1("etaFn","100+(x-2.7)*100/.4",2.4,4.3) ;
    }
  else
    {
      etaFn=TF1("etaFn","250-(x-3.2)*10",2.4,4.3) ;
    };

  Float_t fval=etaFn.Eval(cellY);
  printf("gf70=%f  vs fval=%f \n",gf70,fval);
  if( false&& ( gf70>8.*fval || gf70<(.2*fval) && (gFn70>4)))
    {
      c->gcorrNew=fval/gFn70;
      if((c->gcorrNew)>0)factor=(c->gcorrNew)/(c->gcorr);
      printf("new gcorr set by Fn70\n");
    };

  Float_t FullGain=c->gcorrNew*FpdGain->GetValue(2,nstb,row-1,col-1);
  Float_t over10=4096;
  Float_t intover10;

  int intovermax=30;
  if(c->p_adc)
    { 
      while(over10>100 && intover10<intovermax)
	{
	  intover10=c->p_adc->Integral(over10,4096);
	  over10-=1;
	} 
      over10=over10*FullGain;
      printf("%d adc Evts with E>%f\n",intovermax,over10);
      printf("nstb=%d\n",nstb);

      TFile* aveShape=new TFile("aveShape.root");
      TH2F* SC[4];
      SC[0]=(TH2F*) aveShape->Get("SCA");
      SC[1]=(TH2F*) aveShape->Get("SCB");
      SC[2]=(TH2F*) aveShape->Get("SCC");
      SC[3]=(TH2F*) aveShape->Get("SCD");
      SC[3]->Print();
      Float_t nominalE10=SC[nstb-1]->GetBinContent(col,row);
      if(nstb<3&&nominalE10>50)nominalE10=50;
      printf("over10=%f : nominalE10=%f \n",over10,nominalE10);
      aveShape->Close();
      Float_t overhi=nominalE10*1.5;
      Float_t underlo=nominalE10/5;
      Float_t localgcorr=LocalAve(c->fpdcorr,nstb,row,col,2);
      printf("localgcorr=%f\n",localgcorr);
      if(c->gcorrNew<localgcorr)
	underlo=nominalE10/5;
      if(c->gcorrNew>localgcorr)
	overhi=nominalE10*1.85;
      if(overhi>100)overhi=100;
      
      overhi=100;//temp remove upper limit
      underlo=4;//temp..
      printf("new value of underlo=%f \n",underlo);
      if(over10>overhi )
	{
	  c->gcorrNew=(overhi/over10)*c->gcorrNew;
	  if(c->gcorr>0)factor=c->gcorrNew/c->gcorr;
	  if((c->gcorr)>0)factor=(c->gcorrNew)/(c->gcorr);
	  printf("overhi/over10 =%f / %f ; new gcorr gain reduced to %f\n",
		 overhi,over10,c->gcorrNew);
	}
      if(over10<underlo)
	{
	  c->gcorrNew=(underlo/over10)*c->gcorrNew;
	  if(c->gcorr>0)factor=c->gcorrNew/c->gcorr;
	  if((c->gcorr)>0)factor=(c->gcorrNew)/(c->gcorr);
	  
	  printf("underlo/over10 =%f / %f ; new gcorr increased to E10=%f\n",
		 underlo,over10,c->gcorrNew);
	  
	}
    }; 
  fl.cd();
  if((gf70<(fval*.5) && c->gcorrNew>30 ))
    {
      factor=30;
      c->gcorrNew=30;
      if(c->gcorr>0)factor=c->gcorrNew/c->gcorr;
    };
  if(gFn70<3)
    {
      factor=1;
      c->gcorrNew=1;
      if(c->gcorr>0)factor=c->gcorrNew/c->gcorr;
    };
  printf("factor=%f\n",factor);
  if(fabs(factor-1)>.35 )
    {
      if(factor>1.35)
	{
	  factor=1.35;
	}
      else if(factor<.4)
	{
	  factor=.4;
	};
      if(c->gcorr>0)=c->gcorrNew=c->gcorr*factor;
      printf("10percent  change limit \n");
    }
  Int_t pass=p_Gr0->GetN();
  p_Gr0->SetTitle(name);
  p_Gr0->SetPoint(pass,pass,factor);
  
  p_Gr0->Write("Gr0");
  Int_t nstb=c->Instb;
  Int_t row=c->Row1;
  Int_t col=c->Col1;
  c->fpdcorr->SetValue(2,nstb,row-1,col-1,c->gcorrNew);
  TString mvsestr=mvse->GetTitle();
  char zch[200];
  sprintf(zch,"%s_gc=%f->%f",(char*) mvsestr,c->gcorr,c->gcorrNew);
  mvse->SetTitle(zch);
  mvse->Write();
  SetFPD(c->fpdcorr);// Hard set values override correction
  c->gcorrNew= c->fpdcorr->GetValue(2,nstb,row-1,col-1);
  char setvalchar[100];
  sprintf(setvalchar,"SetValue(2,%d,%d,%d,%f)\n",nstb,row-1,col-1,c->gcorrNew);
  FILE* fp=fopen("ChangeFpd.txt","a");
  if(fp && !NoChange)
    {
      fputs(setvalchar,fp);
      fclose(fp);
      printf("wrote: %s \n",setvalchar);
    }
  else
    {
      printf("failed to write: %s \n",setvalchar);

    };

  Cor=c->fpdcorr->GetValue(2,nstb,row-1,col-1);
  printf("Cor=%f \n",Cor);

  c1->Clear();
  fl.cd();

  if(savegTr)gTr->Fill();

  cout<<"c name="<<c->GetName()<<"\n";
  if(c->Rdep)delete c->Rdep;
  c->Rdep=0;
  cout<<"about to Write c\n";
  c->Write();
  cout<<"about to RecTrc\n";
  atr->Write("RecTr");
  cout<<"about to gTr\n";
  gTr->Write("gTr");
  c1->Update();
  c1->cd(2);
  c1->SetLogz();
  c1->Update();

  //************
  fl.Purge();
  fl.Close();
  TFile fl(fna,"read");

  return;
};

