void Extract(TString set="large")
{
  gROOT->LoadMacro("Legal.C");
  Int_t nstb0=1;
  if(set == TString("small"))nstb0=3;
  TString str=gSystem->ExpandPathName("$SETFMSENV");
  if(str!="SETFMSENV"){printf("source SetFMSEnv first");exit();};
  
  gROOT->Reset();
  gSystem->Load("${FMSSRC}/Fpdchan.so");

  TFile* Fpd=new TFile("FpdFile.root");
  TCanvas* c1=new TCanvas("c1","c1",600,600);
  Bool_t ReplaceFpd=false;
  if(Fpd)ReplaceFpd=true;
  int ver=1;  
  CalibStr* FpdCorr=0;
  CalibStr* FpdCnew=0;
  TString sver="1";
  while(FpdCnew=(CalibStr*) Fpd->Get(TString("FpdCorrV")+sver))
    {		
      FpdCorr=FpdCnew;
      FpdCorr->filename="FpdCorr.txt";
      FpdCorr->OutName="FpdCorr.txt";
      ver++;
      sver=ver;
    };
  if(!FpdCorr)exit;
  FitTower::Setwe_ab(0.8,.3,-.1,.8,.2,7.6);//Nominal Shower Shape
  Cell* cell[34][17][4];
  for(int nstb=nstb0;nstb<nstb0+2;nstb++)
    {
      for(int row=1;row<35;row++)
	{for(int col=1;col<18;col++)
	    {
	      cell[row-1][col-1][nstb-1]=0;
	    }
	};
    };
  TFile* fl=new TFile("Output.root","Update");
  TObjArray* mycells=(TObjArray*)  fl->FindKey("Mycells")->ReadObj();
  TIter next(mycells);
  Cell* c;
  TTree* atr=0;
  tpCellRec* rc=0;
  
  TH1F* edist=new TH1F("edist","edist",100,.0,100.);
  while(c=(Cell*) next())
    {
      edist->Reset();
      //      printf("row=%d col=%d nstb=%d\n",c->Row1,c->Col1,c->Instb);
      cell[c->Row1-1][c->Col1-1][c->Instb-1]=c;
    }
  printf("stage 2\n");
  int pass=1;
  //  Int_t Nrows[4]={34,34,24,24};
  Int_t Nrows[4]={34,34,24,24};
  TTree* tR[34][34][4];
  for(int nstb=nstb0;nstb<nstb0+2;nstb++)
    {
      for(Int_t ro=1;ro<Nrows[nstb-1]+1;ro++)
	{
	  printf("ro=%d\n",ro);
	  for(int col=1;col<Nrows[nstb-1]/2+1;col++)
	    {
	      printf("ro= %d col=%d nstb %d \n",ro,col,nstb);
	      c=cell[ro-1][col-1][nstb-1];
	      if(!c)
		{
		  printf("c not defined\n");
		};
	      
	      if(!c)continue;
	      printf("c->Col1=%d \n",c->Col1);
	      if(Legal(2,c->Instb,c->Row1-1,c->Col1-1))
		{
		  printf("is legal\n");

		  //		  c->InitBranches();
		  int ne=c->CellD->GetEntries();
		  if(ReplaceFpd)c->fpdcorr=FpdCorr;
		  TString fname("data2/");
		  fname=fname+"_"+c->GetName()+".root";
		  TString trname="tr_";
		  trname=trname+c->GetName();
		  printf(" %s\n",(const char*)fname);
		  TFile flout(fname,"recreate");
		  TTree* tr=tR[ro-1][col-1][nstb0-1];
		  tr=new TTree(trname,trname);
		  tr=c->CellD->CopyTree("E1>0");
		  int ne2=tr->GetEntries();
		  tr->Write("tr");
		  if(c->Instb<nstb0 ||c->Instb>nstb0+1)continue;
		  int nev=tr->GetEntries();
		  if(c->CellD)delete c->CellD;
		  c->CellD=tr->CopyTree("E1>0");
		  if(c->Rdep)delete c->Rdep;
		  printf("CellD entries=%d \n",c->CellD->GetEntries());
		  c->Write();
		  int ne3=c->CellD->GetEntries();
		  printf("*****ne=%d ne2=%d ne3=%d\n",ne,ne2,ne3);
		  flout.Close();
		};
	    };
	};
      printf("Extract %s done\n",(char*) set);
    };
  
};
