{
  gROOT->LoadMacro("Legal.C");
  TString str=gSystem->ExpandPathName("$SETFMSENV");
  if(str!="SETFMSENV"){printf("source SetFMSEnv first");exit();};
   
  gROOT->Reset();
  gSystem->Load("${FMSSRC}/Fpdchan.so");
  TString FMSTXT =gSystem->ExpandPathName("${FMSTXT}");
  FMSTXT=FMSTXT+"/";
  TString FMSGAIN =gSystem->ExpandPathName("${FMSGAIN}");
  TString FMSCORR =gSystem->ExpandPathName("${FMSCORR}");
  TString QTMAP = gSystem->ExpandPathName("${QTMAP}");
  TString QTMAP2PP = gSystem->ExpandPathName("${QTMAP2PP}");
  TString FMSTXT=gSystem->ExpandPathName("${FMSTXT}");
  cout<<"FMSTXT=" <<FMSTXT <<"\n";

  FilesSet*  p_files=new FilesSet(  (char*) FMSTXT,
                                    "fpdped.txt",
                                    (char*) FMSGAIN, 
                                    (char*) FMSCORR,
                                    "fill.txt",
                                    "Fake",
                                    "spinpat",
                                    "geom.txt",
                                    (char*) QTMAP,
                                    (char*) QTMAP2PP);
  
  
  gROOT->LoadMacro("Legal.C");
  //  gROOT->LoadMacro("./SetFPD.C");
  gStyle->SetPalette(1);
  //***********Set Up FPDCORR
  TCanvas* c1=new TCanvas("c1","c1",600,500);
   TFile* Fpd=new TFile("FpdFile.root","Read");
   int ver=1;  
  CalibStr* FpdCorr=0;
  CalibStr* FpdCnew=0;
  char strver[100];
  sprintf(strver,"%d",ver);
  while(FpdCnew=(CalibStr*) Fpd->Get(TString("FpdCorrV")+strver))
    {		
      FpdCorr=FpdCnew;
      FpdCorr->filename="FpdCorr.txt";
      FpdCorr->OutName="FpdCorr.txt";
      ver++;
      sprintf(strver,"%d",ver);
    };  
  Fpd->Close();
  FILE* fp=fopen("ChangeFpd.txt","r");
  char line[200];
  if(fp)
    {
      while(!feof(fp))
	{
	  if(fgets(line,100,fp))
	    {
	      Int_t ew,nst,ro,co;
	      Float_t val;
	      sscanf(line,"SetValue(%d,%d,%d,%d,%f)",&ew,&nst,&ro,&co,&val);
	      if(Legal(ew,nst,ro,co))
		FpdCorr->SetValue(ew,nst,ro,co,val);
	    };
	};
      fclose(fp);
    }
  else
    {
      cout<<"ChangeFpd.txt not found\n";
    };
  cout<<"Now SetFPD\n";
  //  SetFPD(FpdCorr);

  //***************  Loop over cells 

  Int_t rr[4];
  rr[0]=rr[1]=34;
  rr[2]=rr[3]=24;
  TFile* f=0;
  bool first=true;
  TH2F NTrigY("NTrigY","Number above 300 vs Y",10000,0,10000,15,2.5,4.0);
  NTrigY.GetXaxis()->SetTitle("Number ADC>300");
  NTrigY.GetYaxis()->SetTitle("Tower Y");
  
  TH2F gcorrY("gcorry","Suggested Gcorr vs Y",200,0,10,15,2.5,4.0);
  gcorrY.GetXaxis()->SetTitle("Gain Correction Required (relative to 50 MeV");
  gcorrY.GetYaxis()->SetTitle("Tower Y");
  c1->Divide(2,1);
  int cnt=0;
  for(int det = 0;det<4;det++)
    {
      printf("det=%d\n",det);
      for(int ro=0;ro<rr[det];ro++)
	{
	  printf("row=%d\n",ro);
	  for(int co=0;co<rr[det]/2;co++)
	    {
	      if(Legal(2,det+1,ro,co))
		{
		  char fname[200];
		  sprintf(fname,"data2/_Cellr%d_c%d_%d.root",ro,co,det);
		  cout<<"try: "<<fname<<"\n";
		  if(f)delete f;
		  f=new TFile(fname,"read");
		  if(f)
		    {
		      TList* keys;
		      keys=f->GetListOfKeys();
		      keys->Print();
		      Float_t deltagcorr=0;
		      TIter next(keys);
		      TKey* key;
		      while(key=(TKey*) next())
			{
			  Sheet* Shr=0;
			  Cell* c=0;
			  TString name=key->GetName();
			  cout<<"keyname="<<key->GetName()<<"\n";
			  int cycle=key->GetCycle();
			  char st[10];
			  sprintf(st,";%d",cycle);
			  name=name+st;
			  if(name.Index("Shr")==0)
			    {
			      cout<<"finding sheet\n";
			      Shr=(Sheet*) key;
			    }
			  if(name.Index("Cellr")==0)
			    {
			      cout<<"finding cell\n";

			      c=(Cell*) key->ReadObj();
			    }
			  if(c)
			    {
			      Float_t gcorr=FpdCorr->GetValue(c->Iew,c->Instb,
						      c->Row1-1,c->Col1-1);
			      c->Setpfiles(p_files);
			      cout<<"do somthing\n";
			      c1->GetPad(1)->SetLogx(0);
			      c1->GetPad(2)->SetLogx(1);
			      c1->GetPad(1)->SetLogy(0);
			      c1->GetPad(2)->SetLogy(0);
			      c1->cd(1);
			      cout<<"Draw\n";
			      //gcorr-1 = Log[75/N300]*5/15;			      
			      if(c->p_adc)
				{
				  if(first)c->p_adc->Draw();
				  if(!first)c->p_adc->Draw("");
				}
			      
			      Int_t nabove300=c->p_adc->Integral(300,4096);
			      Int_t nabove100=c->p_adc->Integral(100,4096);
			      printf("nabove300=%d nabove100=%d \n",nabove300, nabove100);
			      Float_t Etrigger=15;
			      deltagcorr=(5./Etrigger)*log(50./(nabove300+2.));
			      Float_t ggcorr=1+deltagcorr;
			      gcorr=ggcorr;
			      if(gcorr>2.)gcorr=2.;
			      if(gcorr<.4)gcorr=.4;
			      if(nabove100<2)gcorr=2.;
			      
			      c->gcorrNew=gcorr;
			      char setvalchar[100];
			      sprintf(setvalchar,"SetValue(2,%d,%d,%d,%f)\n",
				      det+1,ro,co,c->gcorrNew);
			      FpdCorr->SetValue(ew,det+1,ro,co,c->gcorrNew);			      
			      FILE* fp=fopen("ChangeFpd.txt","a");
			      if(fp)
				{
				  fputs(setvalchar,fp);
				  fclose(fp);
				  printf("wrote: %s \n",setvalchar);
				}
			      else
				{
				  printf("failed to write: %s \n",setvalchar);
				};
			      
			      gcorrY.Fill(c->gcorrNew,c->Eta);
			      NTrigY.Fill(nabove300+.5,c->Eta);
			      gcorrY.Draw("box");
			      c1->cd(2);
			      NTrigY->Draw("box");
			      if((cnt++)%10==0)c1->Update();
			      first=false;
			      cout<<c->GetName()<<"\n";
			    };
			  
			};
		      cout<<"closing file\n";
		      f->Close();
		    };
		};
	    };
	};
    };
  FpdCorr->UpdateFile(4);
}
