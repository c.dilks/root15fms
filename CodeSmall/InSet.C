{
  /*
    -open 6 HV txt files  and  4 qt text files
    -open tmp.root and read TObjArray of Sheets
    -Backup old SheetHistory
    -read 4 txt0 shift files and reset shift in current history
    -read 4 large cell hv txt0 files add update current history
    -read 2 small cell hv txt0 files andd update current history
   */
  gROOT->LoadMacro("Legal.C");
  TString str=gSystem->ExpandPathName("$SETFMSENV");
  if(str!="SETFMSENV"){printf("source SetFMSEnv first");exit();};
  
  gROOT->Reset();
  gSystem->Load("${FMSSRC}/Fpdchan.so");
  Bool_t UseEnvironGaincor=true;

  TString FMSCORR=gSystem->ExpandPathName("${FMSTXT}/${FMSCORR}");
  TString FMSGAIN=gSystem->ExpandPathName("${FMSTXT}/${FMSGAIN}");
 
  CalibStr* FpdG=new CalibStr(90100100,FMSGAIN);
  CalibStr* FpdC=new CalibStr(90100100,FMSCORR);

  FILE* fp[6];
  FILE* qtfp[4];
  printf("ok\n");

  fp[0]=fopen("SheetFiles/txt0/HV1.set","r");
  fp[1]=fopen("SheetFiles/txt0/HV2.set","r");
  fp[2]=fopen("SheetFiles/txt0/HV3.set","r");
  fp[3]=fopen("SheetFiles/txt0/HV4.set","r");
  fp[4]=fopen("SheetFiles/txt0/HV5.set","r");
  fp[5]=fopen("SheetFiles/txt0/HV6.set","r");
  qtfp[0]=fopen("SheetFiles/txt0/QT1.dat","r");
  qtfp[1]=fopen("SheetFiles/txt0/QT2.dat","r");
  qtfp[2]=fopen("SheetFiles/txt0/QT3.dat","r");
  qtfp[3]=fopen("SheetFiles/txt0/QT4.dat","r");
  printf("ok Sho\n");
  Sheet* sho;

  TFile* froot=new TFile("SheetFiles/SheetSet.root","update");
  froot->cd();
  froot->ls();
  TObjArray* to=(TObjArray*) froot->Get("MySheet");
  //  if(!to)to=(TObjArray*) froot->Get("newSheets");
  printf("to created\n");
  if(!to) printf("to not found\n");
  to->IsOwner();
  printf("IsOwner od \n");


  TIter nxt(to);
  while(sho=(Sheet*) nxt())sho->BackupHistory();

  printf("BackupHistory created \n");  
  froot->cd();
  TString line;
  
  Int_t cnt=0;
  Int_t port[6]={7005,7006,7007,7008,2,1};
  TIter nxt0(to);
  int cnt=0;
  Sheet* sh0=0;

  while(sho=(Sheet*) nxt0())
    {
      int iew=2;
      int nstb=sho->nstb1;
      int row=sho->myrow0;
      int col=sho->mycol0;

      if(sho->GetHistory())
	{
	  SheetHistory* hs=sho->GetHistory();
	  hs->OldShift=0;
	  hs->OldHV=0;
	  if(UseEnvironGaincor)
	    {
	      hs->OldGcorr=FpdC->GetValue(iew,nstb,row,col);
	      hs->Gain=FpdG->GetValue(iew,nstb,row,col);
	    };

	  if(sho->nstb1==4 && sho->myrow0==3 && sho->mycol0==4)
	    {
	      cout<<"Presets: "<<sho->GetName()<<"\n"<<sho->StringHistory();	      
	    };
	};
    };
  for(int infile=0;infile<4;infile++)
    {
      printf("read infile %d \n",infile);
      while(qtfp[infile]!=0 && !feof(qtfp[infile]))
      {
	line.Gets(qtfp[infile]);
	cout<<line<<"\n";
	int qtcrate,qtslot,qtchan,minusone,shift;
	qtcrate=infile+1;
	if(line.First("#")!=0)
	  {
	    Sheet* sha=0;
	    char* str=(const char*) line;
	    int nvar=  sscanf(str," %d %d %d %d ",&qtslot,&qtchan,&minusone,&shift);
	    cout<<"nvar="<<nvar<<"\n";
	    if(nvar==4)
	      {
		printf("Sheet::FindSheetqt(to,%d,%d,%d)\n",qtcrate,qtslot+1,qtchan);
		sha=Sheet::FindSheetqt(to,qtcrate,qtslot+1,qtchan);
		if(sha)
		  {
		    cout<<sha->nstb1<< "r=" << sha->myrow0 <<"\n";
		    if(sha->nstb1==4 && sha->myrow0==3 && sha->mycol0==4)
		      {
			cout<<str<<"\n";
			printf("qtcrate=%d slot=%d chan=%d shift=%d \n",qtcrate,qtslot,qtchan,shift);

			printf("line cnt=%d \n",cnt++);

		      };
		    printf("sh defined\n");
		    SheetHistory* his=sha->GetHistory();
		    his->OldShift=shift;

		    if(sha->nstb1==4 && sha->myrow0==3 && sha->mycol0==4)
		      {
			cout<<"oldshift="<<his->OldShift<<" new shift="<<shift<<"\n";
			cout<<"before: "<<sha->StringHistory()<<"\n";
			cout<<"after: "<<sha->StringHistory()<<"\n";
		      }
		  }
		else
		  {
		    printf("Sheet not found\n");

		  };

	      };
	  };
      };
    };
  printf("QT's done\n");
  for(int infile=0;infile<6;infile++)
    {
      while(line.Gets(fp[infile]))
	{ 

	  if(infile<4)
	    {
	      if(line.First("#")!=0)
		{
		  Int_t row,col,hv;
		  char* str=(const char*) line;
		  int pos=0;
		  if((pos=(line.Index("ite")))>0)
		    {
		      pos=pos+4;
		      sscanf(&(str[pos]),"(%d,%d) %d ",&row,&col,&hv);
		      Sheet* shb=Sheet::FindSheetid(to,port[infile],0,row,col);
		      if(shb)
			{
			  cout << str <<"\n";
			  SheetHistory* his=shb->GetHistory();
			  printf("%d %d %d  OldHV=%f\n",shb->nstb1,row,col,his->OldHV);
			  printf("%d %d %d  NewHV=%f\n",shb->nstb1,row,col,his->NewHV);
			  his->OldHV=hv;
			  if(shb->nstb1==4 && shb->myrow0==3 && shb->mycol0==4)
			    {
			      shb->Print();
			      cout<<shb->StringHistory()<<"\n";
			    };
			};
		    };
		};
	    }
	  else
	    {
	      if(line.First("!")==0)
		{
		  Int_t id1,id2,id3,id4,HV;
		  char* str=(const char*) line;
		  int pos=0;
		  if((pos=(line.Index("setctrl")))>0)
		    {
		      pos=pos+7;
		      sscanf(&(str[pos]),"%d %d ",&id2,&id3);
		      printf("setctrl: dev=%d chip=%d chan=%d \n",port[infile],id2,id3);
		    };
		  if((pos=(line.Index("rdac")))>0)
		    {
		      pos=pos+4;
		      sscanf(&(str[pos]),"%x %x",&id4,&HV);
		      printf("id= %d %d %d %d (rdac)\n",port[infile],id2,id3,id4);
		      Sheet* shc=Sheet::FindSheetid(to,port[infile],id2,id3,id4);
		      if(shc==0)
			{
			  printf("no sheet for %s \n",str);
			  system("sleep 2");
			}
		      else
			{
			  SheetHistory* his2=shc->GetHistory();
			  printf("id= %d %d %d %d ",port[infile],id2,id3,id4);
			  cout<<shc<<"\n";
			  his2->OldHV=1.*HV;
			};
		    };

		};
	    };
	};
    };
  printf("step 0\n");
  to->Write("newSheets",TObjArray::kSingleKey);
  TIter next2(to);
  FILE* nfp;
  nfp=  fopen("SheetFiles/newcsv.csv","write");
  int counter=0;
  while(sho=(Sheet*) next2())
    {
      SheetHistory* his=sho->GetHistory();
      sho->HVsetValue=his->OldHV;
      Int_t newHV=sho->HvScale(his->OldGcorr,his->OldShift);
      his->NewHV=newHV;
      his->NewGcorr=1.;
      his->NewShift=his->OldShift;
      
      if(counter++==0)
	{
	  
	  fprintf(nfp,"%s \n",(const char*) sho->MakecsvHead());
	};
      cout<<sho->GetName()<<"\n"<<sho->StringHistory();
      fprintf(nfp,"%s",(const char*) sho->MakecsvLine());
    }
  printf("step 1 \n");
  froot->Purge();
  froot->Close();

}
