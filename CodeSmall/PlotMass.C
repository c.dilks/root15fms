{
  TString str=gSystem->ExpandPathName("$SETFMSENV");
  if(str!="SETFMSENV"){printf("source SetFMSEnv first");exit();};
  gROOT->Reset();
  TString FMSTXT =gSystem->ExpandPathName("${FMSTXT}");
  FMSTXT=FMSTXT+"/";
  TString FMSGAIN =gSystem->ExpandPathName("${FMSGAIN}");
  TString FMSCORR =gSystem->ExpandPathName("${FMSCORR}");
  TString QTMAP = gSystem->ExpandPathName("${QTMAP}");
  TString QTMAP2PP = gSystem->ExpandPathName("${QTMAP2PP}");
  gSystem->Load("${FMSSRC}/Fpdchan.so");
  TString FMSTXT=gSystem->ExpandPathName("${FMSTXT}");
  cout<<"FMSTXT=" <<FMSTXT <<"\n";
  
  FilesSet*  p_files=new FilesSet(  (char*) FMSTXT,
                                    "fpdped.txt",
                                    (char*) FMSGAIN, 
                                    (char*) FMSCORR,
                                    "fill.txt",
                                    "Fake",
                                    "spinpat",
                                    "geom.txt",
                                    (char*) QTMAP,
                                    (char*) QTMAP2PP);
  
  TCanvas* c1=new TCanvas("c1","c1",600,600);
  c1->SetLogz(0);
  gStyle->SetPalette(1);
  TChain gTr("gTr","gTr");
  gTr->Add("data2/*.root");
  TH2F* hh[4];
  hh[0]=new TH2F("NLMass","North Large Mass",17,0,17,34,0,34);
  hh[1]=new TH2F("SLMass","South Large Mass",17,0,17,34,0,34);
  hh[2]=new TH2F("NSMass","North Small Mass",12,0,12,24,0,24);
  hh[3]=new TH2F("SSMass","South Small Mass",12,0,12,24,0,24);

  gTr->Project("NLMass","row-.5:col-.5","(nstb==1)*gMass","zcol");
  gTr->Project("SLMass","row-.5:col-.5","(nstb==2)*gMass","zcol");
  gTr->Project("NSMass","row-.5:col-.5","(nstb==3)*gMass","zcol");
  gTr->Project("SSMass","row-.5:col-.5","(nstb==4)*gMass","zcol");
  for(int j=0;j<4;j++)
    {
      hh[j]->SetStats(0);
      hh[j]->SetMinimum(.05);
      hh[j]->SetMaximum(.2);
    };
  hh[0]->Draw("zcol");
  c1->Print("MassAve.ps(");
  hh[1]->Draw("zcol");
  c1->Print("MassAve.ps");
  hh[2]->Draw("zcol");
  c1->Print("MassAve.ps");
  hh[3]->Draw("zcol");
  c1->Print("MassAve.ps)");
  system("ps2pdf MassAve.ps;rm MassAve.ps");
}
