Float_t LogWt(TH1D* src)
{
  int NBins=src->GetXaxis()->GetNbins();
  TH1D* LW=new TH1D("LogWtsrc","LogWtsrc",NBins,0,NBins);
  for(int j=1;j<NBins;j++)
  {
    Float_t val=src->GetBinContent(j);
    Float_t lval=0;
    if(val>.5)lval=log(1.*val);
    LW->SetBinContent(j,lval);
  };
    Float_t retval=LW->GetMean();
    delete LW;
    return retval;
}
