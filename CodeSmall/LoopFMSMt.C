{
  /*
    This is configured to take Gain from Environment
    It takes FPDCORR from environment if UseEnvironmentFPDCORR is true;
   */
  gROOT->Reset();
  Bool_t UseEnvironmentFPDCORR=false;
  gROOT->LoadMacro("Legal.C");
  TString str=gSystem->ExpandPathName("$SETFMSENV");
  if(str!="SETFMSENV"){printf("source SetFMSEnv first");exit();};
   
  gSystem->Load("${FMSSRC}/Fpdchan.so");
  TString FMSTXT =gSystem->ExpandPathName("${FMSTXT}");
  FMSTXT=FMSTXT+"/";
  TString FMSGAIN =gSystem->ExpandPathName("${FMSGAIN}");
  TString FMSCORR =gSystem->ExpandPathName("${FMSCORR}");
  TString QTMAP = gSystem->ExpandPathName("${QTMAP}");
  TString QTMAP2PP = gSystem->ExpandPathName("${QTMAP2PP}");
  TString FMSTXT=gSystem->ExpandPathName("${FMSTXT}");
  cout<<"FMSTXT=" <<FMSTXT <<"\n";

  FilesSet*  p_files=new FilesSet(  (char*) FMSTXT,
                                    "fpdped.txt",
                                    (char*) FMSGAIN, 
                                    (char*) FMSCORR,
                                    "fill.txt",
                                    "Fake",
                                    "spinpat",
                                    "geom.txt",
                                    (char*) QTMAP,
                                    (char*) QTMAP2PP);
  
  
  gROOT->LoadMacro("Legal.C");
  //  gROOT->LoadMacro("./SetFPD.C");
  gStyle->SetPalette(1);
  //***********Set Up FPDCORR
  Int_t RunNum=9010010;
  //get Gain and Gcor from Environmant

  CalibStr* Gcor=new CalibStr(RunNum,(const char*) (FMSTXT+"/"+FMSCORR));
  CalibStr Gain(RunNum,(const char*) (FMSTXT+"/"+FMSGAIN));
  CalibStr* FpdCorr=Gcor;
  FpdCorr->tm(2,3)->Print();
  Gain.tm(2,3)->Print();
  if(UseEnvironmentFPDCORR)
    {
      cout<<"Using Environment for FPDCORR ->"<<FMSCORR<<"\n";
    }
  //get FpdCorr from FpdFile.root && ChangeFpd.txt

  TCanvas* c1=new TCanvas("c1","c1",600,500);
  if(!UseEnvironmentFPDCORR)
    {
      cout<<"Using FpdFile.root\n";
      TFile* Fpd=new TFile("FpdFile.root","Read");
      int ver=1;  
      FpdCorr=0;
      CalibStr* FpdCnew=0;
      char strver[100];
      sprintf(strver,"%d",ver);
      while(FpdCnew=(CalibStr*) Fpd->Get(TString("FpdCorrV")+strver))
	{		
	  FpdCorr=FpdCnew;
	  FpdCorr->filename="FpdCorr.txt";
	  FpdCorr->OutName="FpdCorr.txt";
	  ver++;
	  sprintf(strver,"%d",ver);
	};  
      Fpd->Close();
      FILE* fp=fopen("ChangeFpd.txt","r");
      char line[200];
      if(fp)
	{
	  while(!feof(fp))
	    {
	      if(fgets(line,100,fp))
		{
		  Int_t ew,nst,ro,co;
		  Float_t val;
		  sscanf(line,"SetValue(%d,%d,%d,%d,%f)",&ew,&nst,&ro,&co,&val);
		  if(Legal(ew,nst,ro,co))
		    FpdCorr->SetValue(ew,nst,ro,co,val);
		};
	    };
	  fclose(fp);
	}
      else
	{
	  cout<<"ChangeFpd.txt not found\n";
	};
      cout<<"Now SetFPD\n";
      //  SetFPD(FpdCorr);
    };
  if(UseEnvironmentFPDCORR)FpdCorr=Gcor;
  
  //***************  Loop over cells 
  cout<<"2,3,9,3="<<FpdCorr->GetValue(2,1,9,3)<<"\n";
  FpdCorr->SetValue(2,1,9,3,0.);
  Int_t rr[4];
  rr[0]=rr[1]=34;
  rr[2]=rr[3]=24;
  TFile* f=0;
  bool first=true;
  int cnt=0;
  Float_t GeVperCnt[4]={.055,.055,.08,.08};
  Float_t nominal_angles[4]={.1 ,.1,.05,.05};
  for(int det = 0;det<4;det++)
    {
      printf("det=%d\n",det);
      for(int ro=0;ro<rr[det];ro++)
	{
	  for(int co=0;co<rr[det]/2;co++)
	    {
	      if(Legal(2,det+1,ro,co))
		{
		  char fname[200];
		  sprintf(fname,"data2/_Cellr%d_c%d_%d.root",ro,co,det);
		  //		  cout<<"try: "<<fname<<"\n";
		  if(f)delete f;
		  f=new TFile(fname,"read");
		  if(f)
		    {
		      TList* keys;
		      keys=f->GetListOfKeys();
		      //     keys->Print();
		      Float_t deltagcorr=0;
		      TIter next(keys);
		      TKey* key;
		      cout<<f->GetName()<<"\n";
		      while(key=(TKey*) next())
			{
			  Sheet* Shr=0;
			  Cell* c=0;
			  TString name=key->GetName();
			  // cout<<"keyname="<<key->GetName()<<"\n";
			  int cycle=key->GetCycle();
			  char st[10];
			  sprintf(st,";%d",cycle);
			  name=name+st;
			  if(name.Index("Shr")==0)
			    {
			      // cout<<"finding sheet\n";
			      Shr=(Sheet*) key;
			    }
			  if(name.Index("Cellr")==0)
			    {
			      //cout<<"finding cell\n";
			      c=(Cell*) key->ReadObj();	
			    }
			  if(c)
			    {
			      if(c->Rdep)delete c->Rdep;
			      // c->Setpfiles(p_files);
			      if( (c->Instb!=det+1) 
				  || (c->Row1!=ro+1)
				  !! (c->Col1!=co+1))
				{
				  cout<<"***************************"<<
				    "Cell class seems messed up\n";
				  exit();
				};
			    };
			  
			};
		      //		      cout<<"closing file\n";
		      f->Close();
		    };
		};
	    };
	};
    };
  FpdCorr->UpdateFile(4);
  c1->Clear();
  gStyle->SetPalette(1);
  delete c1;
  TCanvas* c1=new TCanvas("c1","c1",800,500); 
  c1->Clear();   
  c1->SetLogz();
  c1->cd();
  TH2F* f1=new TH2F(*( FpdCorr->tm(2,1)));
  f1->SetStats(0);
  f1->SetTitle("NorthLarge gcorr");
  f1->SetMarkerSize(.8);
  f1->SetMaximum(5.);
  f1->Draw("zcoltext");
  c1->Print("PrintFMSCOR.ps(");
  TH2F* f2=new TH2F(*( FpdCorr->tm(2,2)));
  f2->SetStats(0);
  f2->SetTitle("SouthLarge gcorr");
  f2->SetMarkerSize(.8);
  f2->SetMaximum(5.);
  f2->Draw("zcoltext");
  c1->Print("PrintFMSCOR.ps");
  TH2F* f3=new TH2F(*( FpdCorr->tm(2,3)));
  f3->SetStats(0);
  f3->SetTitle("NorthSmall gcorr");
  f3->SetMarkerSize(.8);
  f3->SetMaximum(5.);
  f3->Draw("zcoltext");
  c1->Print("PrintFMSCOR.ps");
  TH2F* f4=new TH2F(*( FpdCorr->tm(2,4)));
  f4->SetStats(0);
  f4->SetTitle("SouthSmall gcorr");
  f4->SetMarkerSize(.8);
  f4->SetMaximum(5.);
  f4->Draw("zcoltext)");
  c1->Print("PrintFMSCOR.ps");
  c1->Clear();
  c1->SetLogz();
  c1->cd();
  TH2F* g1=new TH2F(*(Gain.tm(2,1)));
  g1->SetStats(0);
  g1->SetTitle("NorthLarge gains");
  g1->SetMarkerSize(.8);
  g1->SetMaximum(5.);
  g1->Draw("zcoltext");
  c1->Update();
  c1->Print("PrintFMSCOR.ps");
  TH2F* g2=new TH2F(*( Gain.tm(2,2)));
  g2->SetStats(0);
  g2->SetTitle("SouthLarge gain");
  g2->SetMarkerSize(.8);
  g2->SetMaximum(5.);
  g2->Draw("zcoltext");
  c1->Update();
  c1->Print("PrintFMSCOR.ps");
  TH2F* g3=new TH2F(*( Gain.tm(2,3)));
  g3->SetStats(0);
  g3->SetTitle("NorthSmall gain");
  g3->SetMarkerSize(.8);
  g3->SetMaximum(5.);
  g3->Draw("zcoltext");
  c1->Update();
  c1->Print("PrintFMSCOR.ps");
  TH2F* g4=new TH2F(*( Gain.tm(2,4)));
  g4->SetStats(0);
  g4->SetTitle("SouthSmall gain");
  g4->SetMarkerSize(.8);
  g4->SetMaximum(5.);
  g4->Draw("zcoltext)");
  c1->Update();
  c1->Print("PrintFMSCOR.ps)");

  system("ps2pdf PrintFMSCOR.ps;rm PrintFMSCOR.ps");
  FpdCorr->filename="FpdCorr.txt";
  Gain.filename="Gains.txt";    
  FpdCorr->UpdateFile(5);
  Gain.UpdateFile(5);
}
