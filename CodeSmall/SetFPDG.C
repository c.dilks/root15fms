void SetFPDG()
{ 
  TString str=gSystem->ExpandPathName("$SETFMSENV");
  if(str!="SETFMSENV"){printf("source SetFMSEnv first");exit();};
  gROOT->Reset();
  gSystem->Load("${FMSSRC}/Fpdchan.so");
  bool NoChange=false;
  gROOT->LoadMacro("Legal.C");
  gROOT->LoadMacro("LocalAve.C");
  gROOT->LoadMacro("LogWt.C");
  gROOT->LoadMacro("gcorbyTrig.C");
  gROOT->LoadMacro("${FMSROOT}/CodeSmall/MASKFMS.C");
  gROOT->LoadMacro("SETFPD.C");
  gROOT->LoadMacro("GCFMS.C");
  gROOT->LoadMacro("gTre.C");
  GCFMS* gFMS=new GCFMS(0);
  CalibStr* FpdGain=gFMS->FpdGain;
  CalibStr* fpdcorr=gFMS->FpdCorr;
  
  gStyle->SetPalette(1);
  FILE* fp=fopen("CarlGains.txt","r");
  if(fp) printf("file not found\n");
  float val[34][34];
  char instr[200];
  for(int rr=0;rr<34;rr++)
    {
      for(int cc=0;cc<34;cc++)
	{
	  fscanf(fp,"%f\n",&val[rr][cc]);
	  int mcol=16-cc;
	  int nnst=1;
	  float theval=val[rr][cc];
	  float oldval=0;
	  if(cc<17)
	    {
	      //	      printf("nstb=1 row=%d col=%d val=%f ",rr,mcol,val[rr][cc]);
	    }
	  else
	    {
	      nnst=2;
	      mcol=cc-17;
	      //	      printf("nstb=2 row=%d col=%d val=%f ",rr,mcol,val[rr][cc]);	
	    }
	  if(Legal(2,nnst,rr,mcol))
	    {
	      printf("nstb=%d row=%d col=%d val=%f ",nnst,rr,mcol,val[rr][cc]);
	      printf(" old gain=%f ",oldval=FpdGain->GetValue(2,nnst,rr,mcol));
	      if(oldval!=0)
		{
		  printf("ratio=%f",theval/oldval);
		  if(theval!=0)
		    {
		      printf(" invratio=%f",oldval/theval);
		      float corval=fpdcorr->GetValue(2,nnst,rr,mcol);
		      fpdcorr->SetValue(2,nnst,rr,mcol,corval*oldval/theval);
		      FpdGain->SetValue(2,nnst,rr,mcol,theval);
		    }
		}
	      printf("\n");
	    };
	};
    };
  cout<< FpdGain->OutName<<"\n";
  FpdGain->UpdateFile(17);
}
 
