{  
  gROOT->Macro("start.C");
  AnalTools at;
  TCanvas* c2=new TCanvas("c2","c2",800,600);
  TCanvas* c1=new TCanvas("c1","c1",800,600);
  TFile f("Output95_4.root");
  TTree* TwoTr=f.Get("TwoTr");
  Int_t nbins=10 ;
  TH1F* E12uN=new TH1F("E12uN","uN",nbins,0,250.);
  TH1F* E12dN=new TH1F("E12dN","dN",nbins,0,250.);
  TH1F* E12uS=new TH1F("E12uS","uN",nbins,0,250.);
  TH1F* E12dS=new TH1F("E12dS","dN",nbins,0,250.);
  TString cuts1="abs(Eta-3.65)<.3 && abs(M12-.55)<.2&&N12==2 && Ntracks>2";
  TString cuts2="N12==1&&abs(Eta-3.6)<.3&&Ntracks>1&&cos(Phi-Phiaway)<.5";
  TString cuts3="N12==2&&abs(Eta-3.6 )<.3&&Ntracks> 2&&Z<.8&&abs(M12-.55)<.2 ";

  TString cuts=cuts2;

  TString North="&&cos(Phi)<-.5";
  TString South="&&cos(Phi)>.5";
  TString Bdown="&&abs(spin-.5)<.6";
  TString Bup="&&abs(spin-1.5)<.6";
  TString Ydown="&&spin<4&&(spin%2==0)";
  TString Yup="&&spin<4&&(spin%2==1)";
  TString dn=Bdown;
  TString up=Bup;

  TwoTr->Project("E12uN","E12",cuts+North+dn   );
  E12uN->SetLineColor(1);
  TwoTr->Project("E12dN","E12",cuts+North+up );
  E12dN->SetLineColor(2);
  E12uN->Draw("e");
  E12dN->Draw("esame");
  E12uN->Sumw2();  TString cuts2="N12==1&&abs(Eta-3.6)<.3 &&Ntracks>1&&cos(Phi-Phiaway)<.2";

  E12dN->Sumw2();
  TH1F* RN=new TH1F((*E12uN)/(*E12dN));
  TwoTr->Project("E12uS","E12",cuts+South+dn );
  E12uS->SetLineColor(3);
  TwoTr->Project("E12dS","E12",cuts+South+up );
  E12dS->SetLineColor(4);
  E12uS->Draw("esame");
  E12dS->Draw("esame");
  E12uS->Sumw2();
  E12dS->Sumw2();
  c2->cd();
  TH1F* RS=new TH1F((*E12uS)/(*E12dS));
  RN->Draw("e");
  RS->SetLineColor(4);
  RS->Draw("esame");
  TH1F* crossR=new TH1F(at.GetCross(E12uN,E12dN,E12uS,E12dS,"CrossRat"));
  TCanvas* c3=new TCanvas("c3","c3",800,600);
  crossR->Draw();
 
}
