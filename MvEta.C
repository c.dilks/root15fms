{
  TH1F* h[5];
  TCanvas* c2=new TCanvas("c2","c2",600,600);
  c2->Divide(1,5);
  char cut[200];
  char nam[200];
  Float_t eta[5]={4.0,3.6,3.2,2.8,2.4};
  for(int j=0;j<5;j++)
    {
      sprintf(cut,"abs(M12-.5)<.49 && abs(E12-35)<20 && Z<.7 && abs(Eta-%2.1f)<.2",eta[j]);
      cout<<cut<<"\n";
      sprintf(nam,"TwoPhotM%d",j);
      cout<<nam<<"\n";
      h[j]=new TH1F(nam,cut,200,0,1.);
      TwoTr->Project(nam,"M12",cut);
      h[j]->Print();
      c2->cd(j+1);
      h[j]->GetYaxis()->SetLabelSize(.08);
      h[j]->GetXaxis()->SetLabelSize(.08); 
      //      h[j]->GetXaxis()->SetTitleSize(.1);
      h[j]->GetXaxis()->SetTitle("Pair Mass GeV");      
      h[j]->Draw();
    }; 
}
